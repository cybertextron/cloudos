Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/hda3              5242904    759692   4483212  15% /
tmpfs                   127876         8    127868   1% /dev/shm
/dev/hda1               127351     33047     87729  28% /boot
/dev/hda9             10485816     33508  10452308   1% /home
/dev/hda8              5242904    932468   4310436  18% /srv
/dev/hda7              3145816     32964   3112852   2% /tmp
/dev/hda5              5160416    474336   4423928  10% /usr
/dev/hda6              3145816    412132   2733684  14% /var