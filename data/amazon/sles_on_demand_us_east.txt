	vCPU	ECU	Memory (GiB)	Instance Storage (GB)	SUSE Linux Enterprise Server Usage
General Purpose - Current Generation
t2.nano	1	Variable	0.5	EBS Only	$0.0165 per Hour
t2.micro	1	Variable	1	EBS Only	$0.023 per Hour
t2.small	1	Variable	2	EBS Only	$0.056 per Hour
t2.medium	2	Variable	4	EBS Only	$0.152 per Hour
t2.large	2	Variable	8	EBS Only	$0.204 per Hour
m4.large	2	6.5	8	EBS Only	$0.226 per Hour
m4.xlarge	4	13	16	EBS Only	$0.352 per Hour
m4.2xlarge	8	26	32	EBS Only	$0.604 per Hour
m4.4xlarge	16	53.5	64	EBS Only	$1.108 per Hour
m4.10xlarge	40	124.5	160	EBS Only	$2.62 per Hour
m3.medium	1	3	3.75	1 x 4 SSD	$0.167 per Hour
m3.large	2	6.5	7.5	1 x 32 SSD	$0.233 per Hour
m3.xlarge	4	13	15	2 x 40 SSD	$0.366 per Hour
m3.2xlarge	8	26	30	2 x 80 SSD	$0.632 per Hour
Compute Optimized - Current Generation
c4.large	2	8	3.75	EBS Only	$0.21 per Hour
c4.xlarge	4	16	7.5	EBS Only	$0.32 per Hour
c4.2xlarge	8	31	15	EBS Only	$0.541 per Hour
c4.4xlarge	16	62	30	EBS Only	$0.982 per Hour
c4.8xlarge	36	132	60	EBS Only	$1.863 per Hour
c3.large	2	7	3.75	2 x 16 SSD	$0.205 per Hour
c3.xlarge	4	14	7.5	2 x 40 SSD	$0.31 per Hour
c3.2xlarge	8	28	15	2 x 80 SSD	$0.52 per Hour
c3.4xlarge	16	55	30	2 x 160 SSD	$0.94 per Hour
c3.8xlarge	32	108	60	2 x 320 SSD	$1.78 per Hour
GPU Instances - Current Generation
g2.2xlarge	8	26	15	60 SSD	$0.75 per Hour
g2.8xlarge	32	104	60	2 x 120 SSD	$2.7 per Hour
Memory Optimized - Current Generation
r3.large	2	6.5	15	1 x 32 SSD	$0.275 per Hour
r3.xlarge	4	13	30.5	1 x 80 SSD	$0.45 per Hour
r3.2xlarge	8	26	61	1 x 160 SSD	$0.8 per Hour
r3.4xlarge	16	52	122	1 x 320 SSD	$1.5 per Hour
r3.8xlarge	32	104	244	2 x 320 SSD	$2.9 per Hour
Storage Optimized - Current Generation
i2.xlarge	4	14	30.5	1 x 800 SSD	$0.953 per Hour
i2.2xlarge	8	27	61	2 x 800 SSD	$1.805 per Hour
i2.4xlarge	16	53	122	4 x 800 SSD	$3.51 per Hour
i2.8xlarge	32	104	244	8 x 800 SSD	$6.92 per Hour
d2.xlarge	4	14	30.5	3 x 2000 HDD	$0.79 per Hour
d2.2xlarge	8	28	61	6 x 2000 HDD	$1.48 per Hour
d2.4xlarge	16	56	122	12 x 2000 HDD	$2.86 per Hour
d2.8xlarge	36	116	244	24 x 2000 HDD	$5.62 per Hour