	vCPU	ECU	Memory (GiB)	Instance Storage (GB)	Windows with SQL Web Usage
General Purpose - Current Generation
t2.micro	1	Variable	1	EBS Only	$0.068 per Hour
t2.small	1	Variable	2	EBS Only	$0.136 per Hour
t2.medium	2	Variable	4	EBS Only	$0.272 per Hour
t2.large	2	Variable	8	EBS Only	$0.434 per Hour
m4.large	2	6.5	8	EBS Only	$0.261 per Hour
m4.xlarge	4	13	16	EBS Only	$0.439 per Hour
m4.2xlarge	8	26	32	EBS Only	$0.9 per Hour
m4.4xlarge	16	53.5	64	EBS Only	$1.844 per Hour
m4.10xlarge	40	124.5	160	EBS Only	$4.582 per Hour
m3.medium	1	3	3.75	1 x 4 SSD	$0.184 per Hour
m3.large	2	6.5	7.5	1 x 32 SSD	$0.367 per Hour
m3.xlarge	4	13	15	2 x 40 SSD	$0.734 per Hour
m3.2xlarge	8	26	30	2 x 80 SSD	$1.468 per Hour
Compute Optimized - Current Generation
c4.large	2	8	3.75	EBS Only	$0.418 per Hour
c4.xlarge	4	16	7.5	EBS Only	$0.787 per Hour
c4.2xlarge	8	31	15	EBS Only	$1.638 per Hour
c4.4xlarge	16	62	30	EBS Only	$2.233 per Hour
c4.8xlarge	36	132	60	EBS Only	$4.273 per Hour
c3.large	2	7	3.75	2 x 16 SSD	$0.271 per Hour
c3.xlarge	4	14	7.5	2 x 40 SSD	$0.542 per Hour
c3.2xlarge	8	28	15	2 x 80 SSD	$1.083 per Hour
c3.4xlarge	16	55	30	2 x 160 SSD	$2.166 per Hour
c3.8xlarge	32	108	60	2 x 320 SSD	$4.332 per Hour
GPU Instances - Current Generation
g2.2xlarge	8	26	15	60 SSD	$0.957 per Hour
Memory Optimized - Current Generation
r3.large	2	6.5	15	1 x 32 SSD	$0.395 per Hour
r3.xlarge	4	13	30.5	1 x 80 SSD	$0.755 per Hour
r3.2xlarge	8	26	61	1 x 160 SSD	$1.555 per Hour
r3.4xlarge	16	52	122	1 x 320 SSD	$2.365 per Hour
r3.8xlarge	32	104	244	2 x 320 SSD	$3.995 per Hour
Storage Optimized - Current Generation
i2.xlarge	4	14	30.5	1 x 800 SSD	$0.993 per Hour
i2.2xlarge	8	27	61	2 x 800 SSD	$1.986 per Hour
i2.4xlarge	16	53	122	4 x 800 SSD	$3.971 per Hour
i2.8xlarge	32	104	244	8 x 800 SSD	$7.942 per Hour
