t2.nano

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$3.285	$0.0045	31%	$0.0065 per Hour
Partial Upfront	$25	$1.095	$0.0044	32%
All Upfront	$38	$0	$0.0043	34%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$54	$0.73	$0.0031	52%	$0.0065 per Hour
All Upfront	$76	$0	$0.0029	55%
t2.micro

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$6.57	$0.009	31%	$0.013 per Hour
Partial Upfront	$51	$2.19	$0.0088	32%
All Upfront	$75	$0	$0.0086	34%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$109	$1.46	$0.0061	53%	$0.013 per Hour
All Upfront	$151	$0	$0.0057	56%
t2.small

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$13.14	$0.018	31%	$0.026 per Hour
Partial Upfront	$102	$4.38	$0.0176	32%
All Upfront	$151	$0	$0.0172	34%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$218	$2.92	$0.0123	53%	$0.026 per Hour
All Upfront	$303	$0	$0.0115	56%
t2.medium

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$26.28	$0.036	31%	$0.052 per Hour
Partial Upfront	$204	$8.76	$0.0353	32%
All Upfront	$302	$0	$0.0345	34%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$436	$5.84	$0.0246	53%	$0.052 per Hour
All Upfront	$607	$0	$0.0231	56%
t2.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$52.56	$0.072	31%	$0.104 per Hour
Partial Upfront	$408	$17.52	$0.0706	32%
All Upfront	$604	$0	$0.0689	34%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$872	$11.68	$0.0492	53%	$0.104 per Hour
All Upfront	$1214	$0	$0.0462	56%
m4.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$63.51	$0.087	31%	$0.126 per Hour
Partial Upfront	$324	$27.01	$0.074	41%
All Upfront	$635	$0	$0.0725	42%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$657	$18.25	$0.05	60%	$0.126 per Hour
All Upfront	$1235	$0	$0.047	63%
m4.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$126.29	$0.173	31%	$0.252 per Hour
Partial Upfront	$648	$54.02	$0.148	41%
All Upfront	$1271	$0	$0.1451	42%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$1314	$36.5	$0.1	60%	$0.252 per Hour
All Upfront	$2470	$0	$0.094	63%
m4.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$252.58	$0.346	31%	$0.504 per Hour
Partial Upfront	$1296	$108.04	$0.296	41%
All Upfront	$2541	$0	$0.2901	42%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2628	$73	$0.2	60%	$0.504 per Hour
All Upfront	$4941	$0	$0.1881	63%
m4.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$505.89	$0.693	31%	$1.008 per Hour
Partial Upfront	$2593	$216.08	$0.5921	41%
All Upfront	$5082	$0	$0.5802	42%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$5256	$146	$0.4	60%	$1.008 per Hour
All Upfront	$9881	$0	$0.376	63%
m4.10xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$1264.36	$1.732	31%	$2.52 per Hour
Partial Upfront	$6482	$540.2	$1.48	41%
All Upfront	$12706	$0	$1.4505	42%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$13140	$365	$1	60%	$2.52 per Hour
All Upfront	$24703	$0	$0.94	63%
m3.medium

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$35.04	$0.048	28%	$0.067 per Hour
Partial Upfront	$211	$12.41	$0.0411	39%
All Upfront	$353	$0	$0.0403	40%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$337	$10.95	$0.0278	59%	$0.067 per Hour
All Upfront	$687	$0	$0.0261	61%
m3.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$69.35	$0.095	29%	$0.133 per Hour
Partial Upfront	$421	$25.55	$0.0831	38%
All Upfront	$713	$0	$0.0814	39%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$673	$21.9	$0.0556	58%	$0.133 per Hour
All Upfront	$1373	$0	$0.0522	61%
m3.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$138.7	$0.19	29%	$0.266 per Hour
Partial Upfront	$842	$51.1	$0.1662	38%
All Upfront	$1428	$0	$0.1631	39%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$1345	$43.8	$0.1112	58%	$0.266 per Hour
All Upfront	$2746	$0	$0.1045	61%
m3.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$277.4	$0.38	29%	$0.532 per Hour
Partial Upfront	$1683	$101.47	$0.3312	38%
All Upfront	$2840	$0	$0.3243	39%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2691	$87.6	$0.2224	58%	$0.532 per Hour
All Upfront	$5493	$0	$0.209	61%
c4.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$59.86	$0.082	25%	$0.11 per Hour
Partial Upfront	$311	$25.55	$0.0706	36%
All Upfront	$606	$0	$0.0692	37%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$597	$16.79	$0.0457	58%	$0.11 per Hour
All Upfront	$1129	$0	$0.043	61%
c4.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$118.99	$0.163	26%	$0.22 per Hour
Partial Upfront	$621	$51.1	$0.1409	36%
All Upfront	$1212	$0	$0.1384	37%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$1194	$33.58	$0.0914	58%	$0.22 per Hour
All Upfront	$2258	$0	$0.0859	61%
c4.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$238.71	$0.327	26%	$0.441 per Hour
Partial Upfront	$1243	$102.93	$0.2829	36%
All Upfront	$2424	$0	$0.2768	37%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2388	$67.16	$0.1829	59%	$0.441 per Hour
All Upfront	$4516	$0	$0.1718	61%
c4.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$477.42	$0.654	26%	$0.882 per Hour
Partial Upfront	$2485	$205.13	$0.5647	36%
All Upfront	$4849	$0	$0.5536	37%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$4776	$134.32	$0.3657	59%	$0.882 per Hour
All Upfront	$9032	$0	$0.3437	61%
c4.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$954.11	$1.307	26%	$1.763 per Hour
Partial Upfront	$4970	$410.26	$1.1294	36%
All Upfront	$9698	$0	$1.1071	37%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$9552	$268.64	$0.7315	59%	$1.763 per Hour
All Upfront	$18064	$0	$0.6874	61%
c3.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$53.29	$0.073	31%	$0.105 per Hour
Partial Upfront	$326	$18.98	$0.0632	40%
All Upfront	$542	$0	$0.0619	41%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$508	$16.06	$0.0413	61%	$0.105 per Hour
All Upfront	$1020	$0	$0.0388	63%
c3.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$106.58	$0.146	31%	$0.21 per Hour
Partial Upfront	$652	$38.69	$0.1274	39%
All Upfront	$1093	$0	$0.1248	41%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$1016	$32.85	$0.0837	60%	$0.21 per Hour
All Upfront	$2066	$0	$0.0786	63%
c3.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$213.16	$0.292	31%	$0.42 per Hour
Partial Upfront	$1304	$75.92	$0.2529	40%
All Upfront	$2170	$0	$0.2477	41%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2031	$65.7	$0.1673	60%	$0.42 per Hour
All Upfront	$4132	$0	$0.1572	63%
c3.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$426.32	$0.584	31%	$0.84 per Hour
Partial Upfront	$2608	$152.57	$0.5067	40%
All Upfront	$4350	$0	$0.4966	41%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$4063	$131.4	$0.3346	60%	$0.84 per Hour
All Upfront	$8265	$0	$0.3145	63%
c3.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$852.64	$1.168	31%	$1.68 per Hour
Partial Upfront	$5216	$304.41	$1.0124	40%
All Upfront	$8691	$0	$0.9921	41%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$8126	$262.07	$0.6682	60%	$1.68 per Hour
All Upfront	$16506	$0	$0.6281	63%
g2.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$346.02	$0.474	27%	$0.65 per Hour
Partial Upfront	$2306	$103.66	$0.4052	38%
All Upfront	$3478	$0	$0.397	39%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$6307	$43.8	$0.3	54%	$0.65 per Hour
All Upfront	$7410	$0	$0.282	57%
g2.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$1384.08	$1.896	27%	$2.6 per Hour
Partial Upfront	$9224	$414.64	$1.621	38%
All Upfront	$13912	$0	$1.5881	39%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$25228	$175.2	$1.2	54%	$2.6 per Hour
All Upfront	$29640	$0	$1.1279	57%
r3.large

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$80.3	$0.11	37%	$0.175 per Hour
Partial Upfront	$541	$24.09	$0.0948	46%
All Upfront	$813	$0	$0.0928	47%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$1033	$18.98	$0.0653	63%	$0.175 per Hour
All Upfront	$1613	$0	$0.0614	65%
r3.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$160.6	$0.22	37%	$0.35 per Hour
Partial Upfront	$1082	$48.18	$0.1895	46%
All Upfront	$1626	$0	$0.1856	47%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2066	$37.96	$0.1306	63%	$0.35 per Hour
All Upfront	$3226	$0	$0.1228	65%
r3.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$321.2	$0.44	37%	$0.7 per Hour
Partial Upfront	$2164	$96.36	$0.379	46%
All Upfront	$3253	$0	$0.3713	47%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$4132	$75.92	$0.2612	63%	$0.7 per Hour
All Upfront	$6453	$0	$0.2455	65%
r3.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$642.4	$0.88	37%	$1.4 per Hour
Partial Upfront	$4328	$192.72	$0.7581	46%
All Upfront	$6507	$0	$0.7428	47%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$8264	$151.84	$0.5225	63%	$1.4 per Hour
All Upfront	$12906	$0	$0.4911	65%
r3.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$1284.8	$1.76	37%	$2.8 per Hour
Partial Upfront	$8656	$385.44	$1.5161	46%
All Upfront	$13015	$0	$1.4857	47%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$16528	$303.68	$1.0449	63%	$2.8 per Hour
All Upfront	$25812	$0	$0.9822	65%
i2.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$309.52	$0.424	50%	$0.853 per Hour
Partial Upfront	$1820	$113.15	$0.3628	58%
All Upfront	$3114	$0	$0.3555	58%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2740	$88.33	$0.2253	74%	$0.853 per Hour
All Upfront	$5564	$0	$0.2117	75%
i2.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$619.04	$0.848	50%	$1.705 per Hour
Partial Upfront	$3640	$227.03	$0.7265	57%
All Upfront	$6237	$0	$0.712	58%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$5480	$175.93	$0.4495	74%	$1.705 per Hour
All Upfront	$11104	$0	$0.4225	75%
i2.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$1238.08	$1.696	50%	$3.41 per Hour
Partial Upfront	$7280	$453.33	$1.4521	57%
All Upfront	$12465	$0	$1.4229	58%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$10960	$351.86	$0.899	74%	$3.41 per Hour
All Upfront	$22209	$0	$0.8451	75%
i2.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$2476.16	$3.392	50%	$6.82 per Hour
Partial Upfront	$14560	$906.66	$2.9041	57%
All Upfront	$24931	$0	$2.846	58%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$21920	$703.72	$1.7981	74%	$6.82 per Hour
All Upfront	$44418	$0	$1.6902	75%
d2.xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$293.46	$0.402	42%	$0.69 per Hour
Partial Upfront	$1506	$125.56	$0.3439	50%
All Upfront	$2952	$0	$0.337	51%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$2767	$76.65	$0.2103	70%	$0.69 per Hour
All Upfront	$5195	$0	$0.1977	71%
d2.2xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$586.92	$0.804	42%	$1.38 per Hour
Partial Upfront	$3012	$251.12	$0.6878	50%
All Upfront	$5904	$0	$0.674	51%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$5534	$153.3	$0.4206	70%	$1.38 per Hour
All Upfront	$10390	$0	$0.3954	71%
d2.4xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$1173.84	$1.608	42%	$2.76 per Hour
Partial Upfront	$6024	$502.24	$1.3757	50%
All Upfront	$11808	$0	$1.3479	51%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$11068	$306.6	$0.8412	70%	$2.76 per Hour
All Upfront	$20780	$0	$0.7907	71%
d2.8xlarge

1-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
No Upfront	$0	$2347.68	$3.216	42%	$5.52 per Hour
Partial Upfront	$12048	$1004.48	$2.7513	50%
All Upfront	$23616	$0	$2.6959	51%
3-YEAR TERM	
Payment Option	Upfront	Monthly*	Effective Hourly**	Savings over On-Demand	On-Demand Hourly
Partial Upfront	$22136	$613.2	$1.6823	70%	$5.52 per Hour
All Upfront	$41560	$0	$1.5814	71%

Reserved Instance Volume Discounts
If you purchase a large amount of Reserved Instances in an AWS region, you will automatically receive discounts on your upfront fees and hourly fees for future purchases of Reserved Instances in that AWS region. Reserved Instance Tiers are determined based on the List Value (non-discounted total price) for the active Reserved Instances you have per AWS region. A complete list of the Reserved Instance Tiers is shown below:

Reserved Instance Volume Discounts
Total Reserved Instances


Upfront Discount


Hourly Discount

Less than $500,000

0%

0%

$500,000 to $4,000,000

5%

5%

$4,000,000 to $10,000,000

10%

10%

More than $10,000,000

Contact Us

Contact Us