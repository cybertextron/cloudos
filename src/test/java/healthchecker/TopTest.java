package healthchecker;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class TopTest {

	private Top top;

	@Before
	public void setUp() throws Exception {
		String[] tokens = { "11595", "top", "7.6", "00:00.54", "1/1", "0", "21", "2428K+", "0B", "0B", "11595", "1582",
				"running", "*0[1]", "0.00000", };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.top = new Top(instance.getId(), tokens);
	}

	@Test
	public void test() {
		String json = this.top.toString();
		assertNotNull(json);
		assertNotEquals(json.length(), 0);
	}

}
