package healthchecker;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class IostatTest {

	private Iostat iostat;

	@Before
	public void setUp() throws Exception {
		String[] elements = { "61.39", "19", "1.13", "17", "7", "76", "4.19", "3.14", "2.82", };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.iostat = new Iostat(instance.getId(), elements);
	}

	@Test
	public void testToString() {
		String json = this.iostat.toString();
		assertNotNull(json);
		assertNotEquals(json.length(), 0);
	}

}
