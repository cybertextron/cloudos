package healthchecker;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class DiskFileTest {

	private DiskFile diskfile;

	@Before
	public void setUp() throws Exception {
		String[] elements = { "/dev/disk0s2", "1951845952", "773697912", "1177636040", "40%", "96776237", "147204505",
				"40%", "/", };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.diskfile = new DiskFile(instance.getId(), elements);
	}

	@Test
	public void testToString() {
		assertNotNull(this.diskfile.toString());
	}

}
