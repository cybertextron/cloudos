package healthchecker;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class ProcessServiceTest {

	private ProcessService processService;

	@Before
	public void setUp() throws Exception {
		String[] elements = { "philippesouzamoraesribeiro", "2253", "0.0", "0.1", "2496996", "9116", "??", "S",
				"3:42PM", "0:00.08", "/System/Library/" };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.processService = new ProcessService(instance.getId(), elements);
	}

	@Test
	public void testToString() {
		String json = this.processService.toString();
		assertNotNull(json);
		assertNotEquals(json.length(), 0);
	}

}
