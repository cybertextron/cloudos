package healthchecker;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class NetstatTest {

	private Netstat netstat;

	@Before
	public void setUp() throws Exception {
		String[] elements = { "tcp4", "0", "0", "10.232.74.243.55783", "10.0.0.65.afs3-fileser", "SYN_SENT", };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.netstat = new Netstat(instance.getId(), elements);
	}

	@Test
	public void testJson() {
		String json = this.netstat.toString();
		assertNotNull(json);
		assertNotEquals(json.length(), 0);
	}

}
