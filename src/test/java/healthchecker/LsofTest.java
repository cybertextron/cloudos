package healthchecker;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class LsofTest {

	private Lsof lsof;

	@Before
	public void setUp() throws Exception {
		String[] elements = { "Google", "248", "philipperibeiro", "73u", "REG", "1,4", "14163968", "16315254",
				"/Users/philipperibeiro", };
		Instance instance = new Instance("11.22.33.44", "ami-abcdefgh");
		this.lsof = new Lsof(instance.getId(), elements);
	}

	@Test
	public void testToString() {
		String json = this.lsof.toString();
		assertNotNull(json);
		assertNotEquals(json.length(), 0);
	}

}
