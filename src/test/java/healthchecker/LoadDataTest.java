package healthchecker;

/**
*
* Unit test for the LoadData class.
* 
* @Copyright CyberTextron Inc. 2016
* @license: private
* @author Philippe Ribeiro
* @date: 3/26/2016
*
*/
import org.junit.Before;
import org.junit.Test;

import healthchecker.LoadData;


public class LoadDataTest {

	private LoadData data;
	
	@Before
    public void setUp() {
		this.data = new LoadData();
	}

	@Test
	public void test() {
		if (this.data != null) {
			this.data.loadData();
		}
	}

}
