package healthchecker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.client.MongoDatabase;

import healthchecker.MongoDb;

public class MongoDbTest {

	private static MongoDatabase database;

	@Before
	public void setUp() throws Exception {
		database = MongoDb.getDatabase("amazon");
		assertNotNull(database);
		assertEquals(database.getName(), "amazon");
	}

	@Test
	public void testGetDocuments() {
		System.out.println("---------------------------------------");
		System.out.println("Getting all documents");
		System.out.println("---------------------------------------");
		assertNotNull(database);
		List<Document> documents = MongoDb.getDocuments(database, "linux_on_demand_instance");
		assertNotNull(documents);
		assertNotEquals(documents.size(), 0);
		for (Document document : documents) {
			System.out.println(document.toString());
		}
	}

	@Test
	public void testGetDocumentsMap() {
		System.out.println("---------------------------------------");
		System.out.println("Getting all documents with single filter");
		System.out.println("---------------------------------------");
		// Test with only one entry

		List<Document> documents = MongoDb.getDocuments(database, "linux_on_demand_instance", "size", "c3.large");
		assertNotNull(documents);
		assertNotEquals(documents.size(), 0);
		for (Document document : documents) {
			System.out.println(document.toString());
		}
		Map<String, String> map = new HashMap<>();
		map.put("size", "c3.large");
		map.put("price", "0.128");
		System.out.println("---------------------------------------");
		System.out.println("Getting all documents with multiple filter");
		System.out.println("---------------------------------------");
		documents = MongoDb.getDocuments(database, "linux_on_demand_instance", map);
		assertNotNull(documents);
		assertNotEquals(documents.size(), 0);
		for (Document document : documents) {
			System.out.println(document.toString());
		}

	}

	@After
	public void tearDown() throws Exception {
		assertNotNull(database);
	}

}
