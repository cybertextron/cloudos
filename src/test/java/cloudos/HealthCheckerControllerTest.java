package cloudos;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.google.gson.Gson;

import healthchecker.DiskFile;
import healthchecker.DiskFileRepository;
/* Import local libraries */
import healthchecker.Instance;
import healthchecker.InstanceRepository;
import healthchecker.Lsof;
import healthchecker.LsofRepository;
import healthchecker.Netstat;
import healthchecker.NetstatRepository;
import healthchecker.ProcessService;
import healthchecker.ProcessServiceRepository;
import healthchecker.Top;
import healthchecker.TopRepository;
import healthchecker.VmstatRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@ComponentScan("healthchecker")
public class HealthCheckerControllerTest {

	private MockMvc mvc;

	private final static Logger logger = Logger.getLogger(HealthCheckerControllerTest.class);

	@Autowired
	private InstanceRepository instanceRepository;

	@Autowired
	private LsofRepository lsofRepository;
	
	@Autowired
	private ProcessServiceRepository psRepository;
	
	@Autowired
	private VmstatRepository vmstatRepository;
	
	@Autowired
	private TopRepository topRepository;

	@Autowired
	private NetstatRepository netstatRepository;
	
	@Autowired
	private DiskFileRepository dfRepository;
	
	private HttpMessageConverter<Object> mappingJackson2HttpMessageConverter;

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		@SuppressWarnings("unchecked")
		HttpMessageConverter<Object> httpMessageConverter = (HttpMessageConverter<Object>) Arrays.asList(converters)
				.stream().filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();
		this.mappingJackson2HttpMessageConverter = httpMessageConverter;

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setUp() throws Exception {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");
		this.mvc = MockMvcBuilders
				.standaloneSetup(new HealthCheckerController(this.instanceRepository, this.lsofRepository,
						this.psRepository, this.vmstatRepository, this.topRepository,
						this.netstatRepository, this.dfRepository))
				.setViewResolvers(viewResolver)
				.build();
		// clean the repository before start testing.
		this.instanceRepository.deleteAll();
		this.lsofRepository.deleteAll();

		Instance first = new Instance("10.20.30.40", "ami-xxxxxxxx");
		Instance second = new Instance("10.20.30.41", "ami-xxxxxxxx");
		this.instanceRepository.save(first);
		this.instanceRepository.save(second);

		for (Instance instance : this.instanceRepository.findAll()) {
			logger.info(instance);
		}
	}

	@Test
	public void testHealthChecker() throws Exception {
		this.mvc.perform(get("/healthchecker"))
			.andExpect(status().isOk())
			.andExpect(view().name("healthchecker"));
	}

	@Test
	public void testGetInstance() throws Exception {
		Instance instance = this.instanceRepository.findByIpaddress("10.20.30.40");
		String id = instance.getId();
		this.mvc.perform(get("/healthchecker/" + id)).andExpect(status().isOk())
				.andExpect(jsonPath("$.ipaddress", is("10.20.30.40"))).andExpect(jsonPath("$.ami", is("ami-xxxxxxxx")));
	}

	@Test
	public void testCreateInstance() throws Exception {
		Instance instance = new Instance("10.20.30.42", "ami-xxxxxxxx");
		logger.info("---------- Create Instance ----------");
		logger.info(instance);
		String content = instance.toString();
		this.mvc.perform(post("/healthchecker").content(content).contentType(this.contentType))
				.andExpect(status().isOk());
		String id = instance.getId();
		this.mvc.perform(get("/healthchecker/" + id)).andExpect(status().isOk())
		.andExpect(jsonPath("$.ipaddress", is("10.20.30.42"))).andExpect(jsonPath("$.ami", is("ami-xxxxxxxx")));
	}

	@Test
	public void testDeleteInstance() throws Exception {
		Instance instance = this.instanceRepository.findByIpaddress("10.20.30.40");
		String id = instance.getId();
		this.mvc.perform(delete("/healthchecker/" + id)).andExpect(status().isOk());
	}

	@Test
	public void testCreateLsof() throws Exception {
		// create an save the new instance
		Instance instance = new Instance("10.20.30.44", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		// create a new lsof object.
		Lsof lsof = new Lsof(instance.getId(), "Google", 248, "philipperibeiro", "73u", "REG", "1,4", "14163968", "16315254",
                "/Users/philipperibeiro");
		String content = lsof.toString();
		logger.info(String.format("------------------ Lsof: %s --------------", content));
		String url = String.format("/healthchecker/%s/lsof", instance.getId());
		this.mvc.perform(post(url).content(content).contentType(this.contentType)).andExpect(status().isOk());
	}

	@Test
	public void testGetLsof() throws Exception {
		// create an save the new instance
		Instance instance = new Instance("10.20.30.44", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		// create a new lsof object.
		String[] elements = { "Google", "248", "philipperibeiro", "73u", "REG", "1,4", "14163968", "16315254",
				"/Users/philipperibeiro", };
		Lsof lsof = new Lsof(instance.getId(), elements);
		this.lsofRepository.save(lsof);
		String id = instance.getId();
		this.mvc.perform(get("/healthchecker/" + id + "/lsof")).andExpect(status().isOk())
						.andExpect(jsonPath("$[0].command", is("Google")));
	}
	
	@Test
	public void testDeleteLsof() throws Exception {
		Instance instance = new Instance("10.20.30.45", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		// create a new lsof object.
		String[] elements = { "Google", "248", "philipperibeiro", "73u", "REG", "1,4", "14163968", "16315254",
				"/Users/philipperibeiro", };
		Lsof lsof = new Lsof(instance.getId(), elements);
		this.lsofRepository.save(lsof);
		String id = instance.getId();
		this.mvc.perform(delete("/healthchecker/" + id + "/lsof")).andExpect(status().isOk());
		
	}
	
	@Test
	public void testCreateProcessService() throws Exception {
		Instance instance = new Instance("10.20.30.46", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "philippesouzamoraesribeiro", "2253", "0.0", "0.1", "2496996", "9116", "??", "S",
				"3:42PM", "0:00.08", "/System/Library/" };
		ProcessService ps = new ProcessService(instance.getId(), elements);
		logger.info(String.format("------------ Process Service: %s ---------", ps));
		String url = String.format("/healthchecker/%s/process_service", instance.getId());
		logger.info(String.format("------------ URL: %s ---------------------", url));
		this.mvc.perform(post(url).content(ps.toString()).contentType(this.contentType)).andExpect(status().isOk());
		
	}
	
	@Test
	public void testGetProcessService() throws Exception {
		Instance instance = new Instance("10.20.30.46", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "philippesouzamoraesribeiro", "2253", "0.0", "0.1", "2496996", "9116", "??", "S",
				"3:42PM", "0:00.08", "/System/Library/" };
		ProcessService ps = new ProcessService(instance.getId(), elements);
		this.psRepository.save(ps);
		String id = instance.getId();
		this.mvc.perform(get("/healthchecker/" + id + "/process_service")).andExpect(status().isOk())
		.andExpect(jsonPath("$[0].command", is("/System/Library/")));
	}
	
	@Test
	public void testDeleteProcessService() throws Exception {
		Instance instance = new Instance("10.20.30.46", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "philippesouzamoraesribeiro", "2253", "0.0", "0.1", "2496996", "9116", "??", "S",
				"3:42PM", "0:00.08", "/System/Library/" };
		ProcessService ps = new ProcessService(instance.getId(), elements);
		this.psRepository.save(ps);
		String id = instance.getId();
		this.mvc.perform(delete("/healthchecker/" + id + "/process_service")).andExpect(status().isOk());
	}

	@Test
	public void testCreateTop() throws Exception {
		Instance instance = new Instance("10.20.30.47", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] tokens = { "11595", "top", "7.6", "00:00.54", "1/1", "0", "21", "2428K+", "0B", "0B", "11595", "1582",
				"running", "*0[1]", "0.00000", };
		Top top = new Top(instance.getId(), tokens);
		String url = String.format("/healthchecker/%s/top", instance.getId());
		this.mvc.perform(post(url).content(top.toString()).contentType(this.contentType)).andExpect(status().isOk());
	}
	
	@Test
	public void testGetTop() throws Exception {
		Instance instance = new Instance("10.20.30.47", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] tokens = { "11595", "top", "7.6", "00:00.54", "1/1", "0", "21", "2428K+", "0B", "0B", "11595", "1582",
				"running", "*0[1]", "0.00000", };
		Top top = new Top(instance.getId(), tokens);
		String url = String.format("/healthchecker/%s/top", instance.getId());
		this.topRepository.save(top);
		this.mvc.perform(get(url)).andExpect(status().isOk()).andExpect(jsonPath("$[0].user", is("top")));
	}
	
	@Test
	public void testDeleteTop() throws Exception {
		Instance instance = new Instance("10.20.30.47", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] tokens = { "11595", "top", "7.6", "00:00.54", "1/1", "0", "21", "2428K+", "0B", "0B", "11595", "1582",
				"running", "*0[1]", "0.00000", };
		Top top = new Top(instance.getId(), tokens);
		String url = String.format("/healthchecker/%s/top", instance.getId());
		this.topRepository.save(top);
		this.mvc.perform(delete(url)).andExpect(status().isOk());
	}
	
	@Test
	public void testCreateNetstat() throws Exception {
		Instance instance = new Instance("10.20.30.48", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "tcp4", "0", "0", "10.232.74.243.55783", "10.0.0.65.afs3-fileser", "SYN_SENT", };
		Netstat netstat = new Netstat(instance.getId(), elements);
		String url = String.format("/healthchecker/%s/netstat", instance.getId());
		this.mvc.perform(post(url).content(netstat.toString()).contentType(this.contentType)).andExpect(status().isOk());
	}
	
	@Test
	public void testDeleteNetstat() throws Exception {
		Instance instance = new Instance("10.20.30.48", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "tcp4", "0", "0", "10.232.74.243.55783", "10.0.0.65.afs3-fileser", "SYN_SENT", };
		Netstat netstat = new Netstat(instance.getId(), elements);
		this.netstatRepository.save(netstat);
		String url = String.format("/healthchecker/%s/netstat", instance.getId());
		this.mvc.perform(delete(url)).andExpect(status().isOk());
	}
	
	@Test
	public void testGetNetstat() throws Exception {
		Instance instance = new Instance("10.20.30.48", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "tcp4", "0", "0", "10.232.74.243.55783", "10.0.0.65.afs3-fileser", "SYN_SENT", };
		Netstat netstat = new Netstat(instance.getId(), elements);
		this.netstatRepository.save(netstat);
		String url = String.format("/healthchecker/%s/netstat", instance.getId());
		this.mvc.perform(get(url)).andExpect(status().isOk()).andExpect(jsonPath("$[0].state", is("SYN_SENT")));
	}
	
	@Test
	public void testCreateDiskFile() throws Exception {
		Instance instance = new Instance("10.20.30.49", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "/dev/disk0s2", "1951845952", "773697912", "1177636040", "40%", "96776237", "147204505",
				"40%", "/", };
		DiskFile diskfile = new DiskFile(instance.getId(), elements);
		String url = String.format("/healthchecker/%s/disk_file", instance.getId());
		this.mvc.perform(post(url).content(diskfile.toString()).contentType(this.contentType)).andExpect(status().isOk());
	}
	
	@Test
	public void testGetDiskFile() throws Exception {
		Instance instance = new Instance("10.20.30.49", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "/dev/disk0s2", "1951845952", "773697912", "1177636040", "40%", "96776237", "147204505",
				"40%", "/", };
		DiskFile diskfile = new DiskFile(instance.getId(), elements);
		this.dfRepository.save(diskfile);
		String url = String.format("/healthchecker/%s/disk_file", instance.getId());
		this.mvc.perform(get(url)).andExpect(status().isOk()).andExpect(jsonPath("$[0].mountedOn", is("96776237")));
	}
	
	@Test
	public void testDeleteDiskFile() throws Exception {
		Instance instance = new Instance("10.20.30.49", "ami-xxxxxxxx");
		this.instanceRepository.save(instance);
		String[] elements = { "/dev/disk0s2", "1951845952", "773697912", "1177636040", "40%", "96776237", "147204505",
				"40%", "/", };
		DiskFile diskfile = new DiskFile(instance.getId(), elements);
		this.dfRepository.save(diskfile);
		String url = String.format("/healthchecker/%s/disk_file", instance.getId());
		this.mvc.perform(delete(url)).andExpect(status().isOk());
	}
	
	
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}
}
