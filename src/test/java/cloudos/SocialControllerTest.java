package cloudos;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

public class SocialControllerTest {

	private MockMvc mvc;

	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(SocialControllerTest.class);
	
	@Before
	public void setUp() throws Exception {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");
		this.mvc = MockMvcBuilders
				.standaloneSetup(new SocialController())
				.setViewResolvers(viewResolver)
				.build();
	}

	@Test
	public void testSocialController() throws Exception {
		this.mvc.perform(get("/social"))
			.andExpect(status().isOk())
			.andExpect(view().name("social"));
	}

}
