package cloudos;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Implement the Test Case for the HealthChecker Controller.
 * 
 * @author Philippe Ribeiro
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HealthCheckerIT {

	@LocalServerPort
	private int port;

	private URL base;
	private TestRestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.port = 8080;
		this.base = new URL("http://localhost:" + port + "/healthchecker");
		template = new TestRestTemplate();
	}

	@Test
	public void getHealthChecker() throws Exception {
		ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
		System.out.println("------------- Body response  ---------------------------");
		System.out.println(response.getBody());
		System.out.println("--------------------------------------------------------");
		// assertThat(response.getBody(),
		// equalTo('[{"ipaddress":"10.20.30.40","ami":"ami-xxxxxxxx"},{"ipaddress":"10.20.30.41","ami":"ami-xxxxxxxx"}]"));
	}

}
