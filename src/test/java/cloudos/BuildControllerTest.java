package cloudos;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BuildControllerTest {
	
	private MockMvc mvc;
	
	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(BuildControllerTest.class);
	
	@Before
	public void setUp() throws Exception {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp/view/");
		viewResolver.setSuffix(".jsp");
		this.mvc = MockMvcBuilders.standaloneSetup(new BuildsController()).setViewResolvers(viewResolver).build();
	}
	
	@Test
	public void testBuildsController() throws Exception {
		this.mvc.perform(get("/builds")).andExpect(status().isOk());
	}
	
	@Test
	public void testBuildsPost() throws Exception {
		/*
		 * String configuration = "sudo apt-get install apache2";
		 * Build build = new Build(configuration, 1, "aws", "ansible");
		 * this.mvc.perform(post("/builds").content(asJsonString(build)).
		 * contentType(MediaType.APPLICATION_JSON)
		 * .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		 */
		
	}
	
	private String asJsonString(final Object object) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(object);
			return jsonContent;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
