package amazon;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;

public class OnDemandInstanceTest {
	
	private final String securityGroup = "default";
	private final String keyPairName = "test_key";
	private Key key;
	private OnDemandInstance onDemand;
	protected AmazonEC2 client;
	private final static Logger logger = Logger.getLogger(OnDemandInstanceTest.class);
	
	@Before
	public void setUp() throws Exception {
		this.client = new AmazonEC2Client(AWSCredentialSingleton.getCredentials());
		this.key = new Key();
		assertNotNull(this.key);
		assertNotNull(this.client);
		String privateKey = this.key.getPrivateKey(this.keyPairName, this.client);
		assertNotNull(privateKey);
		this.onDemand = new OnDemandInstance(this.securityGroup);
	}
	
	@After
	public void tearDown() throws Exception {
		assertNotNull(this.onDemand);
		this.onDemand.terminate();
		this.key.deleteKey(this.keyPairName, this.client);
	}
	
	@Test
	public void testAvailabilityZones() {
		List<String> zones = this.onDemand.availabilityZones();
		assertNotNull(zones);
		assertNotEquals(zones.size(), 0);
	}
	
	@Test
	public void testOnDemandInstanceString() {
		assertNotNull(this.onDemand);
		String value = this.onDemand.toString();
		logger.info(value);
		assertNotNull(value);
	}
	
	@Test
	public void testCreateSingleInstance() {
		/*
		assertNotNull(this.onDemand);
		
		this.onDemand.createInstance("ami-59e8964e", "m1.small", new Integer(1), new Integer(1), this.keyPairName,
				this.securityGroup);
		// check if there's a single instance in the instanceId list.
		assertSame(this.onDemand.getInstanceCount(), 1);
		
		List<String> publicIpList = this.onDemand.getInstancesPublicIP();
		logger.info("-----------------------------------------");
		logger.info(publicIpList);
		logger.info("-----------------------------------------");
		assertNotNull(publicIpList);
		assertTrue(publicIpList.size() > 0);
		*/
		
	}
	
	@Test
	public void testMultipleInstances() {
		/**
		 *
		 * // test the creation of 3 instances.
		 * assertNotNull(this.onDemand);
		 * // create the first 3 instances.
		 * this.onDemand.createInstance("ami-59e8964e", "m1.small", new
		 * Integer(1), new Integer(3), this.keyPairName,
		 * this.securityGroup);
		 * assertSame(this.onDemand.getInstanceCount(), 3); // create 10 more
		 * 
		 * List<String> publicIpList = this.onDemand.getInstancesPublicIP();
		 * logger.info("-----------------------------------------");
		 * logger.info(publicIpList);
		 * logger.info("-----------------------------------------");
		 * assertNotNull(publicIpList);
		 * assertTrue(publicIpList.size() > 0);
		 */
	}
}
