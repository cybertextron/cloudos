package amazon;

/**
 * KeyPair test request for AWS.
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeyTest {
	
	protected AmazonEC2 client;
	protected Key key;
	
	@Before
	public void setUp() throws Exception {
		// use an amazon EC2 client
		this.client = new AmazonEC2Client(AWSCredentialSingleton.getCredentials());
		this.key = new Key();
	}
	
	@Test
	public void test1GetPrivateKey() {
		assertNotNull(this.key);
		assertNotNull(this.client);
		String privateKey = this.key.getPrivateKey("test_key", this.client);
		System.out.println("Created KeyPair: " + privateKey);
	}
	
	@Test
	public void test2DeleteKey() {
		assertNotNull(this.key);
		assertNotNull(this.client);
		this.key.deleteKey("test_key", this.client);
	}
	
	@Test
	public void test3SaveKeyPair() {
		assertNotNull(this.key);
		assertNotNull(this.client);
		String newkey = "mykey";
		String keyPair = this.key.saveKeyPair(newkey, this.client);
		assertNotNull(keyPair);
		// delete the key;
		this.key.deleteKey(newkey, this.client);
	}
	
}
