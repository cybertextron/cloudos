package amazon;
/**
 * Test case for the SecurityGroup class
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amazonaws.services.ec2.AmazonEC2;

@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityGroupTest {

	// use an amazon EC2 client
	protected AmazonEC2 instance;
	protected SecurityGroup securityGroup;

	// @Test(expected=Exception.class)
	/**
	 * @Test public void setUp() { this.instance = new AmazonEC2Client(
	 *       AWSCredentialSingleton.getCredentials() );
	 *       assertNotNull(this.instance); this.securityGroup = new
	 *       SecurityGroup("test_group", "Unit test group", this.instance);
	 *       assertNotNull(this.securityGroup); }
	 * 
	 * @Test public void testIngressGroup() { boolean status = false; try {
	 *       this.securityGroup.createGroupIngress("192.0.2.0/24",
	 *       "198.51.100.0/24", "tcp", new Integer(80), new Integer(80)); status
	 *       = true; } catch (AmazonServiceException e) { status = false; }
	 *       finally { assertTrue(status); } }
	 */
}
