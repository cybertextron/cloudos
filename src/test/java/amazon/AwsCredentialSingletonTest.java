package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import amazon.AWSCredentialSingleton;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amazonaws.auth.AWSCredentials;

@RunWith(SpringJUnit4ClassRunner.class)
public class AwsCredentialSingletonTest {

	@Test
	public void testSingleton() {
		AWSCredentials credentials = null;
		assertNull(credentials);
		/* credentials were correctly created */
		credentials = AWSCredentialSingleton.getCredentials();
		assertNotNull(credentials);
	}

}
