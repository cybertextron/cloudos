package amazon;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class ReservedInstanceTest {

	private final String securityGroup = "test_group";
	private final String keyPairName = "test_key";
	private ReservedInstance reservedInstance;

	/**
	 * Initial setup for the ReservedInstance classes.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.reservedInstance = new ReservedInstance(this.securityGroup);
	}

	/**
	 * Create a single instance in AWS. This is pretty much a smoke test for the
	 * ReservedInstance classes.
	 *
	@Test
	public void testCreateInstance() {
		assertNotNull(this.reservedInstance);
		this.reservedInstance.createInstance("ami-4b814f22", "m1.small", new Integer(1), 
											 new Integer(1), this.keyPairName, this.securityGroup);
	}
	*/

}
