package builds;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AnsibleTest {
	
	private Ansible ansible;
	private String dirname;
	
	@Before
	public void setUp() throws Exception {
		this.ansible = new Ansible();
		this.dirname = System.getProperty("user.home") + "/ansible_test";
	}
	
	@Test
	public void test1CreateDirectory() {
		this.ansible.createDirectory(this.dirname);
		// Check if the directory exists
		File file = new File(this.dirname);
		assertTrue(file.exists());
	}
	
	@Test
	public void test2CreatePlaybookFile() {
		String text = "this is a text test file";
		String filepath = this.dirname + "/playbook.yml";
		try {
			this.ansible.createPlaybookFile(text, filepath);
			File file = new File(filepath);
			assertTrue(file.exists());
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test3CreateInventoryFile() {
		List<String> hostnames = new ArrayList<String>();
		hostnames.add("10.20.30.40");
		hostnames.add("10.20.30.41");
		String filepath = this.dirname;
		this.ansible.createInventoryFile(hostnames, filepath);
		File file = new File(filepath);
		assertTrue(file.exists());
	}
	
	@Test
	public void test4Deploy() {
		Build build = new Build();
		// this.ansible.deploy(build);
	}
	
	@Test
	public void test5DeleteDirectory() {
		assertTrue(this.ansible.removeDirectory(this.dirname));
	}
	
}
