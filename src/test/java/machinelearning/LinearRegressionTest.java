package machinelearning;

import java.util.ArrayList;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;

public class LinearRegressionTest {

	public final static Logger logger = Logger.getLogger(LinearRegressionTest.class);
	
	@Test
	public void test1() {
		double xVal[] = { 2104, 1600, 2400, 1416, 3000 };
		ArrayList<MathVector> x = new ArrayList<MathVector>();
		for (int i = 0; i < 5; i++) {
			x.add(new MathVector());
			if (x.size() == 0) {
				continue;
			} else {
				MathVector vector = x.get(i);
				if (vector != null) {
					vector.set(0, xVal[i]);
				}
			}
		}
		double y[] = { 400, 330, 369, 232, 540 };
		LinearRegression lr = new LinearRegression();
		lr.multiLinearRegression(x, y);
		System.out.println(lr.toString());
		// find the correct answer....
	}

	@Test
	public void test2() {
		int n = 100;
		this.test(n);
	}

	@Test
	public void test3() {
		int n = 1500;
		this.test(n);
	}

	@Test
	public void test4() {
		int n = 10000;
		this.test(n);
	}

	private void test(int n) {
		assert n >= 1;
		ArrayList<MathVector> x = new ArrayList<MathVector>();
		double y[] = new double[n];
		Random random = new Random();
		for (int i = 0; i < n; i++) {
			double a = -100.0 + random.nextDouble() * 200.0;
			double b = -100.0 + random.nextDouble() * 200.0;
			ArrayList<Double> xVal = new ArrayList<Double>();
			xVal.add(a);
			if (xVal.size() != 0) {
				x.add(new MathVector(xVal));
				y[i] = b;
			}
		}
		long startTime = System.nanoTime();
		LinearRegression lr = new LinearRegression();
		lr.multiLinearRegression(x, y);

		System.out.println(lr.toString());
		long stopTime = System.nanoTime();
		System.out.println(stopTime - startTime);
	}

}
