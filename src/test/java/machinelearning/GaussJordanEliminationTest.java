package machinelearning;

public class GaussJordanEliminationTest {

	/**
	 * private void test(String name, double[][] A, double[] b) {
	 * System.out.println("----------------------------------------------------"
	 * ); System.out.println(name);
	 * System.out.println("----------------------------------------------------"
	 * ); GaussJordanElimination gaussian = new GaussJordanElimination(A, b); if
	 * (gaussian.isFeasible()) { System.out.printf("Solution to Ax = b");
	 * double[] x = gaussian.primal(); for (int i = 0; i < x.length; i++) {
	 * System.out.printf(String.format("%10.6f\n", x[i])); } } else {
	 * System.out.println("Certificate of infeasibility"); double[] y =
	 * gaussian.dual(); for (int j = 0; j < y.length; j++) {
	 * System.out.printf("%10.6f\n", y[j]); } } System.out.println();
	 * System.out.println(); }
	 * 
	 * // 3-by-3 nonsingular system
	 * 
	 * @Test public void test1() { double[][] A = { { 0, 1, 1 }, { 2, 4, -2 }, {
	 *       0, 3, 15 } }; double[] b = { 4, 2, 36 }; test("test 1", A, b); }
	 * 
	 *       // 3-by-3 nonsingular system
	 * @Test public void test2() { double[][] A = { { 1, -3, 1 }, { 2, -8, 8 },
	 *       { -6, 3, -15 } }; double[] b = { 4, -2, 9 }; test("test 2", A, b);
	 *       }
	 * 
	 *       // 5-by-5 singular: no solutions // y = [ -1, 0, 1, 1, 0 ]
	 * @Test public void test3() { double[][] A = { { 2, -3, -1, 2, 3 }, { 4,
	 *       -4, -1, 4, 11 }, { 2, -5, -2, 2, -1 }, { 0, 2, 1, 0, 4 }, { -4, 6,
	 *       0, 0, 7 }, }; double[] b = { 4, 4, 9, -6, 5 }; test("test 3", A,
	 *       b); }
	 * 
	 *       // 5-by-5 singluar: infinitely many solutions
	 * @Test public void test4() { double[][] A = { { 2, -3, -1, 2, 3 }, { 4,
	 *       -4, -1, 4, 11 }, { 2, -5, -2, 2, -1 }, { 0, 2, 1, 0, 4 }, { -4, 6,
	 *       0, 0, 7 }, }; double[] b = { 4, 4, 9, -5, 5 }; test("test 4", A,
	 *       b); }
	 * 
	 *       // 3-by-3 singular: no solutions // y = [ 1, 0, 1/3 ]
	 * @Test public void test5() { double[][] A = { { 2, -1, 1 }, { 3, 2, -4 },
	 *       { -6, 3, -3 }, }; double[] b = { 1, 4, 2 }; test("test 5", A, b); }
	 * 
	 *       // 3-by-3 singular: infinitely many solutions
	 * @Test public void test6() { double[][] A = { { 1, -1, 2 }, { 4, 4, -2 },
	 *       { -2, 2, -4 }, }; double[] b = { -3, 1, 6 }; test(
	 *       "test 6 (infinitely many solutions)", A, b); }
	 */
}
