package builds;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
public class DeployerFactory {

	/**
	 * Default constructor
	 */
	public DeployerFactory() {
		
	}
	
	/**
	 * Use the getDeployer method to get object of the type Deployer
	 * 
	 * @param type: The type of the deployer: Ansible, Chef, Salt or Puppet.
	 * @param build: The build information.
	 * @return A Deployer type object.
	 */
	public Deployer getDeployer(Build build) {
		if (build.getDeployer() == null) {
			return null;
		}
		if (build.getDeployer().equalsIgnoreCase("ANSIBLE")) {
			return new Ansible();
		} else if (build.getDeployer().equalsIgnoreCase("CHEF")) {
			String[] args = {};
			return new Chef(args);
		} else if (build.getDeployer().equalsIgnoreCase("SALT")) {
			return new Salt();
		} else if (build.getDeployer().equalsIgnoreCase("PUPPET")) {
			return new Puppet();
		} 
		return null;
	}
}
