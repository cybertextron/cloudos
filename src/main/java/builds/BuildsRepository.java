package builds;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildsRepository extends MongoRepository<Build, String> {

}
