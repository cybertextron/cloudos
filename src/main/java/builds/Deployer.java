package builds;

public interface Deployer {

	public void deploy(Build build);

}
