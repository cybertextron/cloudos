package builds;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

@Document(collection = "builds")
public class Build {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	private String id;
	private String date;

	@NotNull
	@Size(min=2)
	private String configuration;
	
	@NotNull
	private String provider;
	
	@NotNull
	private String deployer;
	
	@NotNull
	@Min(0)
	private Integer instances;

	public Build() {
	}

	public Build(String configuration, Integer instances, String provider, String deployer) {
		this.id = new ObjectId().toHexString();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.date = dateFormat.format(new Date());
		this.configuration = configuration;
		this.provider = provider;
		this.instances = instances;
		this.deployer = deployer;
	}

	public Integer getInstances() {
		return this.instances;
	}
	
	public void setInstances(Integer instances) {
		this.instances = instances;
	}
	
	public String getProvider() {
		return this.provider;
	}
	
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public void setDeployer(String deployer) {
		this.deployer = deployer;
	}
	
	public String getDeployer() {
		return this.deployer;
	}
	
	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getConfiguration() {
		return this.configuration;
	}
	
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
