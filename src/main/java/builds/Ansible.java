package builds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.apache.log4j.Logger;

public class Ansible implements Deployer {
	
	final static Logger logger = Logger.getLogger(Ansible.class);
	
	public Ansible() {
	}
	
	/**
	 * 
	 */
	@Override
	public void deploy(Build build) {
		// TODO Auto-generated method stub
		String path = System.getProperty("user.home") + "/ansible/solr/provisioning";
		try {
			String command = "ansible-playbook -i inventory -u ubuntu --key-file=~/.ssh/test_deploy.pem playbook.yml --sudo";
			String[] tokens = command.split("\\s+");
			ProcessBuilder builder = new ProcessBuilder(tokens);
			logger.info("------------------------------------------------");
			logger.info(String.format("Executing %s in directory %s", command, path));
			builder.directory(new File(path));
			Process child = builder.start();
			watch(child);
			logger.info("------------------------------------------------");
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param process
	 */
	private void watch(final Process process) {
		new Thread() {
			@Override
			public void run() {
				BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = null;
				try {
					logger.info("------------------------------------------");
					logger.info("Executing Ansible");
					while ((line = input.readLine()) != null) {
						logger.info(line);
					}
					logger.info("-------------------------------------------");
				}
				catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
		}.start();
	}
	
	/**
	 * 
	 * @param directoryPath
	 * @return
	 */
	public boolean removeDirectory(String directoryPath) {
		/* First, remove all the files in the directory */
		File directory = new File(directoryPath);
		if (!directory.exists()) {
			logger.warn("Directory " + directoryPath + " does not exist.");
			return true;
		}
		String[] entries = directory.list();
		for (String entry : entries) {
			File current = new File(directory.getPath(), entry);
			logger.info("Deleting file " + current.getName());
			current.delete();
		}
		return directory.delete();
	}
	
	/**
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public boolean deleteFile(String filename) throws IOException {
		File file = new File(filename);
		if (file.delete()) {
			logger.info(file.getName() + " is deleted");
			return true;
		}
		logger.warn("Delete operation is failed.");
		return false;
	}
	
	/**
	 * 
	 * @param filename
	 * @return
	 */
	public boolean fileExists(String filename) {
		File f = new File(filename);
		if (f.exists() && !f.isDirectory()) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param hostnames
	 */
	public void createInventoryFile(List<String> hostnames, String inventoryFile) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(inventoryFile));
			out.println("[webservers]");
			for (int i = 0; i < hostnames.size(); i++) {
				String line = String.format("solr%s ansible_ssh_host=%s ansible_ssh_port=22", i, hostnames.get(i));
				out.println(line);
			}
		}
		catch (IOException e) {
			logger.error("Caught IOException: " + e.getMessage());
		}
		finally {
			if (out != null) {
				out.close();
			}
		}
	}
	
	/**
	 * 
	 * @param text
	 * @param targetFilePath
	 * @throws IOException
	 */
	public void createPlaybookFile(String text, String targetFilePath) throws IOException {
		Path targetPath = Paths.get(targetFilePath);
		byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
		Files.write(targetPath, bytes, StandardOpenOption.CREATE);
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public boolean createDirectory(String name) {
		File directory = new File(name);
		boolean result = false;
		if (!directory.exists()) {
			logger.info("Creating directory: " + name);
			try {
				result = directory.mkdirs();
			}
			catch (SecurityException se) {
				logger.error(se.getMessage());
			}
		}
		else {
			logger.info(String.format("Directory %s already exists", name));
		}
		return result;
	}
	
}
