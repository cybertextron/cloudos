package amazon;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.Tag;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class EBS {

	/* Make the AmazonEC2 client static, so it can be used by other objects */
	private static AmazonEC2 client;
	
	public final static Logger logger = Logger.getLogger( EBS.class );
	
	/**
	 * Default constructor
	 */
	public EBS() { }

	/**
	 * 
	 * @param client
	 */
	public EBS(AmazonEC2 client) {
		EBS.client = client;
	}
	
	/**
	 * 
	 * @param instanceId
	 * @param size
	 * @param region
	 * @param device
	 * @param name
	 * @param value
	 * @return
	 */
	public String createVolume(String instanceId, Integer size, String region,
							   String device, String name, String value) {
		try {
			logger.info("Volume creating begins...");
			CreateVolumeRequest request = new CreateVolumeRequest(size, region);
			CreateVolumeResult result = client.createVolume(request);
			
			/* Create the list of tags we want to create */
			logger.info("Settings the tags to the volume...");
			ArrayList<Tag> instanceTags = new ArrayList<>();
			instanceTags.add(new Tag(name, value));
			/* Create a new tag request */
			CreateTagsRequest tagsRequest = new CreateTagsRequest()
												.withTags(instanceTags)
												.withResources(result.getVolume().getVolumeId());
			client.createTags(tagsRequest);
			
			String volumeId = result.getVolume().getVolumeId();
			this.attachVolume(volumeId, instanceId, device);
			logger.info("Creating the volume ends...");
			return volumeId;
		} catch (AmazonServiceException ase) {
			logger.error(ase.getMessage());
			return null;
		}
		
	}
	
	/**
	 * Attaches a volume to an EC2 instance. The volume must has been created
	 * before being used, and not attached to other EC2 instance.
	 * 
	 * @param size
	 * @param availabilityZone
	 * @param device
	 * @param instanceId
	 */
	public void attachVolume(String volumeId, String instanceId,
							 String device) {
		try {
			AttachVolumeRequest attachRequest = new AttachVolumeRequest(volumeId,
																		instanceId, device);
			client.attachVolume(attachRequest);
			logger.info("Volume " + volumeId + " correctly attached.");
		} catch (AmazonServiceException ase) {
			logger.error(ase.getMessage());
		}
	}
	
	/**
	 * 
	 */
	@Override
	public String toString() {
		/**
		 * Define the toString method for the ReservedInstance class.
		 */
		return String.format(
                "EBS[EC2 client=%s]",
                client);
	}
}
