package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document (collection = "aws_instance_os_type")
public class AWSInstanceOSType {

	@Id
	private String id;
	
	@Field
	private InstanceOSType osType;
	
	public AWSInstanceOSType(InstanceOSType osType) {
		this.osType = osType;
	}
	
	@Override
	public String toString() {
		return String.format(
                "User[id=%s, type='%s']",
                this.id, this.osType);
	}
}
