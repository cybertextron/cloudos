package amazon;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EC2InstanceRepository extends MongoRepository<EC2Instance, String> {

    public List<EC2Instance> findByModel(String model);
    public List<EC2Instance> findByVcpu(int vpcu);
    public List<EC2Instance> findByStorage(String storage);
    public List<EC2Instance> findBySsdStorage(double ssdStorage);
    public List<EC2Instance> findByEbsOnly(boolean ebsOnly);
    public List<EC2Instance> findByNetworkingPerformance(AWSNetworkingPerformance networkingPerformance);
    

}
