package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ec2_instances")
public class EC2Instance {

	@Id
	private String id;

	private InstanceType type;
	private String model;
	private int vcpu;
	private int cpuCreditHour;
	private double memory;
	private String storage;
	private boolean ebsOnly;
	private double dedicatedEbsThroughput;
	private double ssdStorage;
	private boolean enhancedNetworking;
	private double cpuClockSpeed;

	private EC2InstanceCost cost;
	private AWSNetworkingPerformance networkingPerformance;
	
	public EC2Instance() { }
	
	public EC2Instance(InstanceType type, String model, int vpcu, int cpuCreditHour,
					   double memory, String storage, boolean ebsOnly,
					   double dedicatedEbsThroughput, double ssdStorage, boolean enhancedNetworking,
					   double cpuClockSpeed, EC2InstanceCost cost, AWSNetworkingPerformance networkingPerformance) {
		
		this.type = type;
		this.model = model;
		this.vcpu = vpcu;
		this.memory = memory;
		this.storage = storage;
		this.cpuClockSpeed = cpuClockSpeed;
		this.cpuCreditHour = cpuCreditHour;
		this.ebsOnly = ebsOnly;
		this.dedicatedEbsThroughput = dedicatedEbsThroughput;
		this.ssdStorage = ssdStorage;
		this.enhancedNetworking = enhancedNetworking;
		this.cpuClockSpeed = cpuClockSpeed;
		this.cost = cost;
		this.networkingPerformance = networkingPerformance;
	}
	
    @Override
    public String toString() {
        return String.format(
                "User[id=%s, type='%s', model='%s']",
                this.id, this.type, this.model);
    }
}
