package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document (collection = "aws_instance_family")
public class AWSInstanceFamily {

	@Id
	private String id;

	@Field
	private InstanceFamily instanceFamily;
	
	public AWSInstanceFamily(InstanceFamily family) {
		this.instanceFamily = family;
	}
	
	@Override
	public String toString() {
		return String.format(
                "User[id=%s, type='%s']",
                this.id, this.instanceFamily);
	}
}
