package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.mongodb.repository.MongoRepository;

import com.amazonaws.regions.Regions;

/**
 * Mongo Query interface for the AWS Regions Collection
 * @author philippesouzamoraesribeiro
 *
 */
public interface AWSRegionsRepository extends MongoRepository<AWSRegions, String>{
	
	/* Only one needed ... search by the Region */
	public Regions findByRegion(Regions region);

}
