package amazon;

import org.apache.log4j.Logger;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "instances")
public class Instance {

	private final static Logger logger = Logger.getLogger(Instance.class);

	@Id
	private String id;
	private String type;
	private String currencies;
	private String price;
	private String instance_type_id;
	private String rate;
	private String size;
	private String name;
	private String vcpu;
	private String storage;
	private String ecu;
	private String memory;

	public Instance() {
	}

	public Instance(String type, String currencies, String price, String instance_type_id, String rate, String size,
			String name, String vcpu, String storage, String ecu, String memory) {
		this.type = type;
		this.currencies = currencies;
		this.price = price;
		this.instance_type_id = instance_type_id;
		this.rate = rate;
		this.size = size;
		this.name = name;
		this.vcpu = vcpu;
		this.storage = storage;
		this.ecu = ecu;
		this.memory = memory;
	}

	@Override
	public String toString() {
		return String.format(
				"Instance[id=%s, type='%s', currencies='%s', price='%s', rate='%s', size='%s',"
						+ "name='%s', vcpu='%s', storage='%s', ecu='%s', memory='%s', instance_type_id='%s']",
				this.id, this.type, this.currencies, this.price, this.rate, this.size, this.name, this.vcpu,
				this.storage, this.ecu, this.memory, this.instance_type_id);
	}
}
