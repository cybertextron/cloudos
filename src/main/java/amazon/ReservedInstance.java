package amazon;

import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;

/**
 * Define an object for the Reserved Instances in AWS. 
 * This object is composed of a GroupName, which must exists before the
 * object is instantiated, and a keyPairName.
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class ReservedInstance extends EC2 {
	
	/**
	 * Default constructor
	 */
	public ReservedInstance() { }

	/**
	 * 
	 * @param securityGroup
	 */
	public ReservedInstance(String securityGroup) {
		super(securityGroup);
	}
	
	/**
	 * 
	 * @param ami
	 * @param instance
	 * @param minCount
	 * @param maxCount
	 * @param keyPairName
	 * @param securityGroup
	 */
	public void createInstance(String ami, String instance,
							   Integer minCount, Integer maxCount,
							   String keyPairName, String securityGroup) 
	{
		assert client != null;
		/* add boundaries, e.g max number of instances allowed to be created */
		assert minCount >= 1 && minCount <= maxCount;
		assert maxCount >= 1;
		
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId(ami)
        	.withInstanceType(instance)
        	.withMinCount(minCount)
        	.withMaxCount(maxCount)
        	.withKeyName(keyPairName)
        	.withSecurityGroups(securityGroup);
		RunInstancesResult runInstancesResult =
			      					client.runInstances(runInstancesRequest);
		logger.info(runInstancesResult);
	}
	
	/**
	 * 
	 */
	@Override
	public String toString() {
		/**
		 * Define the toString method for the ReservedInstance class.
		 */
		return String.format(
                "ReservedInstance[EC2 client=%s]",
                client.getClass());
	}

}
