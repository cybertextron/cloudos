package amazon;
import java.util.Map;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
final class CloudOSEntry<K, V> implements Map.Entry<K, V>  {

	private final K key;
	private  V value;
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public CloudOSEntry(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * 
	 */
	@Override
	public K getKey() {
		return this.key;
	}
	
	/**
	 * 
	 */
	@Override
	public V getValue() {
		return this.value;
	}
	
	/**
	 * 
	 */
	@Override
	public V setValue(V value) {
		V old = this.value;
		this.value = value;
		return old;
	}
}
