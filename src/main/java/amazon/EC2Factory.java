package amazon;

/**
 * Will create instances based on their type
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class EC2Factory {

	private EC2Factory() {} 

	/**
	 * Create a new EC2 Instance type, based on its type
	 * 
	 * @param type the type of the EC2 Instance
	 * @return
	 *
	public static EC2 getInstance(EC2InstanceType type, String groupName) {
		EC2 client = null;
		switch ( type ) {
			case EBS_OPTIMIZED_INSTANCE:
				client = new EBSOptimizedInstance(groupName);
				break;
			case ON_DEMAND_INSTANCE:
				client = new OnDemandInstance(groupName);
				break;
			case RESERVERD_INSTANCE:
				client = new ReservedInstance(groupName);
				break;
			default:
				client = new EC2(groupName);
				break;
		}
		return instance;
	}
	*/
}
