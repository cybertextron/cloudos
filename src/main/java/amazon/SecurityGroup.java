package amazon;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.IpPermission;

/**
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 3/26/2016
 */	
public class SecurityGroup {

	/** 
	 * The Security group name must be unique within the AWS Region
	 * in which you initialize your Amazon EC2 client.
	 * Create a new security group in AWS which acts like a virtual firewall
	 * that controls the network traffic for one or more EC2 instances.
	 */
	// the name of the group
	private String groupName;
	// any particular description
	private String description;
	// group client
	private CreateSecurityGroupRequest csgr;
	// EC2 client.
	private AmazonEC2 client;
	// holds a map between each protocol and the ports being used.
	private Map<String, List<String>> properties;

	/** Logger */
	private final static Logger logger = Logger.getLogger( SecurityGroup.class );
	
	// default constructor
	public SecurityGroup() {}

	/**
	 * 
	 * @param groupName
	 * @param description
	 * @param client
	 */
	public SecurityGroup(String groupName, String description,
						 AmazonEC2 client) {
		try {
			assert client != null;
			
			this.groupName = groupName;
			this.description = description;
			this.client = client;
			this.properties = new HashMap<>();
			this.csgr = new CreateSecurityGroupRequest();
			this.csgr.withGroupName(this.groupName)
			         .withDescription(this.description);
			CreateSecurityGroupResult result = client.createSecurityGroup(this.csgr);
			logger.info (result);
		} catch (AmazonServiceException e) {
            // Output the error to the logger
            logger.error ("Error when calling describeSpotInstances");
            logger.error ("Caught exception: " + e.getMessage());
            logger.error ("Response Status Code: " + e.getStatusCode());
            logger.error ("Error Code: " + e.getErrorCode());
            logger.error ("Request ID: " + e.getRequestId());
		}
	}

	/**
	 * Create an ingress security group in AWS for in-bound connections.
	 * This group contains the IP permission from start to end.
	 * 
	 * @param start	the start IP range
	 * @param end 	the end of the IP range
	 * @param protocol	the type of the protocol being allowed (e.g 'HTTP')
	 * @param fromPort	the start port number
	 * @param toPort	the end port number
	 * 
	 */
	public void createGroupIngress(String start, String end, String protocol,
							 	   Integer fromPort, Integer toPort) {

		try {
			IpPermission permission = new IpPermission();
			permission.withIpRanges(start, end)
					  .withIpProtocol(protocol)
					  .withFromPort(fromPort)
					  .withToPort(toPort);
			// if the protocol is already listed, then add the new ranges to
			// the protocol.
			List<String> ipRanges;
			if (this.properties.containsKey(protocol)) {
				ipRanges = this.properties.get(protocol);
			} else {
				ipRanges = new LinkedList<>();
			}
			// A better algorithm to check ranges may be needed.
			ipRanges.add(start);
			ipRanges.add(end);
			// add the property to the permission.
			permission.setIpRanges(ipRanges);
			AuthorizeSecurityGroupIngressRequest authorize = 
					new AuthorizeSecurityGroupIngressRequest();
			authorize.withGroupName(this.groupName)
					 .withIpPermissions(permission);
			this.client.authorizeSecurityGroupIngress(authorize);
		} catch (AmazonServiceException e) {
            // Output the error to the logger
            logger.error ("Error when calling describeSpotInstances");
            logger.error ("Caught exception: " + e.getMessage());
            logger.error ("Response Status Code: " + e.getStatusCode());
            logger.error ("Error Code: " + e.getErrorCode());
            logger.error ("Request ID: " + e.getRequestId());
		}
	}
}
