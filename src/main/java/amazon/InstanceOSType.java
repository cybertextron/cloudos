package amazon;

/**
 * Define all the Operating Systems defined in AWS
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum InstanceOSType {
	
	LINUX ("Linux"),
	RHEL ("RHEL"),
	SLES ("SLES"),
	WINDOWS ("Windows"),
	WINDOWS_WITH_SQL_STANDARD ("Windows with SQL Standard"),
	WINDOWS_WITH_SQL_WEB ("Windows with SQL Web");
	
	private String os;
	
	private InstanceOSType(String os) {
		this.os = os;
	}
	
	@Override
	public String toString() {
		return String.format(
                "User[OS='%s']", this.os);
	}
}
