package amazon;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeImagesResult;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeTagsRequest;
import com.amazonaws.services.ec2.model.DescribeTagsResult;
import com.amazonaws.services.ec2.model.Image;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.RebootInstancesRequest;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TagDescription;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public abstract class EC2 {

    protected String groupName;
    /*
     * Make the client static, since the connection can be shared across all the
     * objects.
     */
    protected static AmazonEC2 client;
    /* Allow to handle and manipulate Elastic Block Storage */
    protected EBS ebs;
    protected ArrayList<String> ipRanges;
    protected ArrayList<IpPermission> ipPermissions;
    /* Contain a list of all the instances created */
    protected List<String> instanceIds;
    /* Maintain a Map between the instance and its IP address */
    protected Map<String, String> addresses;
    /* Logger */
    public final static Logger logger = Logger.getLogger(EC2.class);

    protected volatile List<String> availabilityZones;

    /**
     * Default constructor
     */
    public EC2() {
    }

    /**
     * Constructor for the EC2 class. Since EC2 is an abstract class, it cannot
     * be instantiated. The children class can take advantage of this
     * constructor for simplification.
     * 
     * @param groupName
     *            The name of the Security Group the instances will belong to.
     */
    public EC2(String groupName) {
	try {
	    /* Create the AmazonEC2Client object so we can call various APIs */
	    client = new AmazonEC2Client(AWSCredentialSingleton.getCredentials());
	    this.groupName = groupName;
	    this.ebs = new EBS(client);
	    /* Store all the instances created */
	    this.instanceIds = new LinkedList<>();
	    this.addresses = new HashMap<>();
	} catch (Exception e) {
	    logger.error(e.getMessage());
	    throw new AmazonClientException(String.format("Cannot load the credentials from the"
		    + "credential profiles file. Please make sure that your credentials file is at"
		    + " the correct location (~/.aws/credentials), and is in valid format."));
	}
    }

    /**
     * 
     * @param request
     * @return
     */
    public List<TagDescription> describeTags(DescribeTagsRequest request) {
	logger.info(String.format("describe tags, request=%s", request));
	DescribeTagsResult result = client.describeTags(request);
	return result.getTags();
    }

    /**
     * 
     * @return
     */
    public synchronized List<String> availabilityZones() {
	if (this.availabilityZones == null) {
	    DescribeAvailabilityZonesResult result = client.describeAvailabilityZones();
	    this.availabilityZones = result.getAvailabilityZones().stream()
		    .filter(zone -> "available".equals(zone.getState())).map(AvailabilityZone::getZoneName)
		    .collect(Collectors.toList());
	    logger.info(String.format("Availability zones => %s", this.availabilityZones));

	}
	return this.availabilityZones;
    }

    /**
     * Create a new Security Group in AWS. If Security Group already exists,
     * then it will throw an exception
     * 
     * @param securityGroupName
     *            The name of the Security Group the instances will belong to.
     */
    public void createSecurityGroup(String securityGroupName) {
	try {
	    CreateSecurityGroupRequest securityGroup = new CreateSecurityGroupRequest(this.groupName,
		    securityGroupName);
	    client.createSecurityGroup(securityGroup);
	} catch (AmazonServiceException ase) {
	    /*
	     * Likely this means that the group is already created, so ignore.
	     */
	    logger.error(ase.getMessage());
	}
    }

    /**
     * 
     * @param securityGroupId
     * @param rules
     */
    public void deleteSGIngressRules(String securityGroupId, List<IpPermission> rules) {
	logger.info(String.format("delete ingress sg rules, sgId=%s, rules=%s", securityGroupId, rules));

	client.revokeSecurityGroupIngress(
		new RevokeSecurityGroupIngressRequest().withGroupId(securityGroupId).withIpPermissions(rules));
    }

    /**
     * 
     * @param securityGroupId
     * @param rules
     */
    public void createSGIngressRules(String securityGroupId, List<IpPermission> rules) {
	logger.info(String.format("create ingress sg rules, sgId=%s, rules=%s", securityGroupId, rules));

	client.authorizeSecurityGroupIngress(
		new AuthorizeSecurityGroupIngressRequest().withGroupId(securityGroupId).withIpPermissions(rules));
    }

    /**
     * 
     * @param securityGroupId
     */
    public void deleteSecurityGroup(String securityGroupId) {
	logger.info(String.format("delete security group, securityGroupId=%s", securityGroupId));
	client.deleteSecurityGroup(new DeleteSecurityGroupRequest().withGroupId(securityGroupId));
    }

    /**
     * Get a list of all the available images in AWS.
     * 
     * @return A list of all the available Images.
     */
    public List<Image> getAvailableImages() {
	try {
	    DescribeImagesResult directory = client.describeImages();
	    List<Image> images = directory.getImages();
	    return images;
	} catch (AmazonServiceException ase) {
	    // just log the error.
	    logger.error(ase.getMessage());
	    return null;
	}
    }

    /**
     * Get the number of instances in the list.
     * 
     * @return the number of instances in the instanceIds list.
     */
    public int getInstanceCount() {
    	return this.instanceIds.size();
    }

    /**
     * Get the IP of the current host, so that we can limit the Security Group
     * by default to the ip range associated with your subnet.
     * 
     * @return String the IP address of the current host.
     */
    public String getLocalHostIpAddress() {
		try {
		    String ipaddress = "0.0.0.0/0";
		    InetAddress addr = InetAddress.getLocalHost();
		    /* Get IP Address */
		    ipaddress = addr.getHostAddress() + "/10";
		    this.ipRanges.add(ipaddress);
		    return ipaddress;
		} catch (UnknownHostException e) {
		    logger.error(e.getMessage());
		    return null;
		}
    }

    /**
     * Open up port 'fromPort' for protocol traffic to the associate IP from
     * above (e.g. SSH traffic )
     * 
     * @param protocol
     * @param fromPort
     * @param toPort
     */
    public void addIpRanges(String protocol, Integer fromPort, Integer toPort) {

		IpPermission permission = new IpPermission();
		permission.setIpProtocol(protocol);
		permission.setFromPort(fromPort);
		permission.setToPort(toPort);
		permission.setIpRanges(this.ipRanges);
		this.ipPermissions.add(permission);
	
		try {
		    /* Authorize the ports to be used. */
		    AuthorizeSecurityGroupIngressRequest ingressRequest = new AuthorizeSecurityGroupIngressRequest(
			    this.groupName, this.ipPermissions);
		    client.authorizeSecurityGroupIngress(ingressRequest);
		} catch (AmazonServiceException ase) {
		    /*
		     * Ignore because this likely means the zone has already been
		     * authorized
		     */
		    logger.warn(ase.getErrorMessage());
		}

    }

    /**
     * Get a list of all the current reservations in AWS.
     * 
     * @return List<Reservation>
     */
    public List<Reservation> getReservations() {
		DescribeInstancesResult describeInstancesRequest = client.describeInstances();
		return describeInstancesRequest.getReservations();
    }

    /**
     * Get a set of all the current instances in use.
     * 
     * @return Set<Instance> the set of instances.
     */
    public Set<Instance> getInstances() {
		Set<Instance> instances = new HashSet<>();
		List<Reservation> reservations = this.getReservations();
		for (Reservation reservation : reservations) {
		    instances.addAll(reservation.getInstances());
		}
	
		return instances;
    }

    /**
     * Get the status (running, stopped, terminated) of a single EC2 instance,
     * given its instanceId.
     * 
     * @param instanceId
     *            the instance ID.
     * @return The instance status.
     */
    public String getInstanceStatus(String instanceId) {
	try {
	    DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest().withInstanceIds(instanceId);
	    DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
	    List<InstanceStatus> state = result.getInstanceStatuses();
	    while (state.size() < 1) {
		// Do Nothing, just wait, have thread sleep if needed.
		result = client.describeInstanceStatus(request);
		state = result.getInstanceStatuses();
	    }
	    String status = state.get(0).getInstanceState().getName();
	    return status;

	} catch (AmazonServiceException ase) {
	    // log the error and return null
	    logger.error(ase.getErrorMessage());
	    return null;
	}
    }

    /**
     * Return all the current instances Id.
     * 
     * @return A list of instance id.
     */
    public List<String> getInstancesId() {
	return this.instanceIds;
    }

    /**
     * Get the status (running, stopped, terminated) of all EC2 instance
     * 
     * @return A list of instance status.
     */
    public List<InstanceStatus> getInstanceStatus() {
	try {
	    DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest();
	    DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
	    List<InstanceStatus> state = result.getInstanceStatuses();
	    while (state.size() < 1) {
		// Do Nothing, just wait, have thread sleep if needed.
		result = client.describeInstanceStatus(request);
		state = result.getInstanceStatuses();
	    }
	    return state;

	} catch (AmazonServiceException ase) {
	    // log the error and return null
	    logger.error(ase.getErrorMessage());
	    return null;
	}
    }

    /**
     * Start all the instances in the instanceIds list.
     */
    public void start() {
	try {
	    logger.info("Attempting to start all instances");
	    StartInstancesRequest request = new StartInstancesRequest(this.instanceIds);
	    client.startInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Start a particular set of EC2 instance, given its ID
     * 
     * @param instanceId
     *            A list of instance IDs
     */
    public void start(List<String> instances) {
	try {
	    logger.info(String.format("Attempting to start '%s' instances", instances.size()));
	    StartInstancesRequest request = new StartInstancesRequest(instances);
	    client.startInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Stop all the instances in the instanceIds list.
     */
    public void stop() {
	try {
	    logger.info("Attempting to stop all the instances");
	    StopInstancesRequest request = new StopInstancesRequest(this.instanceIds);
	    client.stopInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Stop all the instances in a particular list of IDs.
     * 
     * @param instances
     *            A list of instance IDs
     */
    public void stop(List<String> instances) {
	try {
	    logger.info(String.format("Attempting to stop '%s' instances", instances.size()));
	    StopInstancesRequest request = new StopInstancesRequest(instances);
	    client.stopInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Reboot all the instances
     */
    public void reboot() {
	try {
	    logger.info("Reboot");
	    RebootInstancesRequest request = new RebootInstancesRequest(this.instanceIds);
	    client.rebootInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Reboot all the instances in a particular list of IDs.
     * 
     * @param instances
     *            A list of instance IDs
     */
    public void reboot(List<String> instances) {
	try {
	    logger.info(String.format("Attempting rebooting '%s' instances", instances.size()));
	    RebootInstancesRequest request = new RebootInstancesRequest(instances);
	    client.rebootInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Terminate all the instances in the instanceIds list.
     */
    public void terminate() {
	try {
	    logger.info("Attempting to terminate all instances");
	    TerminateInstancesRequest request = new TerminateInstancesRequest(this.instanceIds);
	    client.terminateInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Terminate all the instances in a particular list of IDs.
     * 
     * @param instances
     *            A list of instance IDs
     */
    public void terminate(List<String> instances) {
	try {
	    logger.info(String.format("Attempting to terminate '%s' instances", instances.size()));
	    TerminateInstancesRequest request = new TerminateInstancesRequest(instances);
	    client.terminateInstances(request);
	} catch (AmazonServiceException ase) {
	    logger.error(ase.getMessage());
	}
    }

    /**
     * Get all the currently running instances based on their common tag name.
     * 
     * @param tagname
     *            the tag name of the instance.
     * @param value
     *            the value name of the instance.
     * @return a List of instance IDs.
     */
    public List<String> getRunningInstancesByTags(String tagname, String value) {
	List<String> instances = new ArrayList<>();
	for (Reservation reservation : this.getReservations()) {
	    for (Instance instance : reservation.getInstances()) {
		// if the instance is not running, skip
		if (!instance.getState().getName().equals(InstanceStateName.Running.toString())) {
		    continue;
		}
		for (Tag tag : instance.getTags()) {
		    if (tag.getKey().equals(tagname) && tag.getValue().equals(value)) {
			instances.add(instance.getInstanceId());
		    }
		}
	    }
	}
	return instances;
    }

    /**
     * User defined toString method for EC2
     * 
     * @return The string representation of the EC2 object.
     */
    @Override
    public String toString() {
	return String.format("EC2[groupName='%s']", this.groupName);
    }

    /**
     * Shutdown the EC2 client connection.
     */
    @Override
    public void finalize() {
	if (client != null) {
	    logger.info("Shutting down the EC2 client");
	    client.shutdown();
	} else {
	    logger.warn("EC2 client is null... nothing to do.");
	}
    }

}
