package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.amazonaws.regions.Regions;

@Document(collection = "aws_regions")
public class AWSRegions {

	@Id
	private String id;
	private Regions region;
	
	public AWSRegions(Regions region) {
		this.region = region;
	}
	
	@Override
	public String toString() {
		return String.format(
                "Region[id=%s, region='%s']",
                this.id, this.region);
	}
}
