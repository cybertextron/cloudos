package amazon;

/**
 * Define the different families of AWS Instances
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum InstanceFamily {

	GENERAL_PURPOSE ("General Purpose"),
	COMPUTE_OPTIMIZED ("Compute optimized"),
	GPU_INSTANCES ("GPU instances"),
	MEMORY_OPTIMIZED ("Memory optimized"),
	STORAGE_OPTIMIZED ("Storage optimized"),
	MICRO ("micro");

	private String instanceFamily;

	private InstanceFamily(String family) {
		this.instanceFamily = family;
	}
	
	@Override
	public String toString() {
		return String.format(
                "User[type='%s']", this.instanceFamily);
	}
}
