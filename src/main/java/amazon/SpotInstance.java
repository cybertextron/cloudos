package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.model.CancelSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.LaunchSpecification;
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest;
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

public class SpotInstance extends EC2 {

	// Add the security group to the request.
	private ArrayList<String> securityGroups;
	private Set<String> spotInstances;
		
	public SpotInstance() {}
	
	/**
	 * 
	 * @param groupName
	 */
	public SpotInstance(String groupName) {
		super(groupName);
		this.securityGroups = new ArrayList<>();
		this.securityGroups.add(groupName);
		this.spotInstances = new HashSet<>();
	}
	
	/**
	 * Launch a bid for a number of instances. These instances will be qualified
	 * as "Spot Instances" in AWS. There needs to be a bid, the AMI id, type of
	 * the instance and the number of instances to be created.
	 * 
	 * @param bidPrice	The instance bid price
	 * @param imageId	The AMI id
	 * @param instanceType	The type of the instance (e.g medium, small).
	 * @param instanceCount	The number of instances to be created with the request.
	 */
	public void launchSpotInstance(String bidPrice, String imageId, String instanceType,
								   Integer instanceCount) {
		/* Create the AmazonEC2Client object so we can call various APIs */
		/* Initialize a Spot Instance Request */
		RequestSpotInstancesRequest request = new RequestSpotInstancesRequest();
		/* Request 'instanceCount' x 'instanceType' with a bid price of $0.03 */
		request.setSpotPrice(bidPrice);
		request.setInstanceCount(instanceCount);
		
		/* Setup the specifications of the launch. This includes the instance
		 * type (e.g t1.micro) and the latest Amazon Linux AMI id available.
		 * Now, you should always use the latest Amazon Linux AMI id or another
		 * of your choosing
		 */
		LaunchSpecification launch = new LaunchSpecification();
		launch.setImageId(imageId);
		launch.setInstanceType(instanceType);
		
		/* Add the security group to the Request */
		launch.setSecurityGroups(this.securityGroups);
		
		/* Add the launch specifications to the request */
		request.setLaunchSpecification(launch);
		
		/* Call the RequestSpotInstance API */
		ArrayList<String> instances = this.getRequestSpotInstancesResult(request);
		// add all the new instances to the instanceIds list.
		this.instanceIds.addAll(instances);
	}
	
	/**
	 * Get all the successful requests and wait until their instantiation is
	 * completed, or until the bid is finished.
	 * 
	 * @param request	the Spot Instance request.
	 * @return	A list of the new Instance Ids.
	 */
	public ArrayList<String> getRequestSpotInstancesResult(RequestSpotInstancesRequest request) {
		/* Call the RequestSpotInstance API */
		RequestSpotInstancesResult result = client.requestSpotInstances(request);
		List<SpotInstanceRequest> requestResponses = result.getSpotInstanceRequests();
		/* Add all of the request ids to the hashet, so we can determine when they hit the
		 * active state
		 */
		ArrayList<String> currentspotInstanceRequestIds = new ArrayList<>();
		for (SpotInstanceRequest response : requestResponses) {
			logger.info("Created Spot Request: " + response.getSpotInstanceRequestId());
			String current = response.getSpotInstanceRequestId();
			this.spotInstances.add(current);
			currentspotInstanceRequestIds.add(current);
		}
		
		/* Wait for the current spotInstances to be created */
		this.waitInstances(currentspotInstanceRequestIds);
		return currentspotInstanceRequestIds;
	}

	/**
	 * Return how many spot instances have been launched.
	 * 
	 * @return the number of instances in spotInstances.
	 */
	public int getNumberSpotInstances() {
		return this.spotInstances.size();
	}
	
	/**
	 * Performs a polling for current spot instance requests. It will wait
	 * until the state of the response is not open.
	 * 
	 * @param spotInstanceRequestIds	A list of spot instance requests.
	 */
	public void waitInstances(List<String> spotInstanceRequestIds) {
		boolean anyOpen = false;
		do {
			/* Create the describe object wit hall the request ids to monitor
			 * (e.g that we started)
			 */
			DescribeSpotInstanceRequestsRequest describeRequest = new DescribeSpotInstanceRequestsRequest();
			describeRequest.setSpotInstanceRequestIds(spotInstanceRequestIds);
			/* Initialize the anyOpen variable to false - which assumes there are
			 * no requests open unless we find one that is still open.
			 */
			try {
				/* Retrieve all of the requests we want to monitor */
				DescribeSpotInstanceRequestsResult describeResult = 
						client.describeSpotInstanceRequests(describeRequest);
				List<SpotInstanceRequest> describeResponses = describeResult.getSpotInstanceRequests();
				/* Look through each request and determine if they are all in the active state */
				for (SpotInstanceRequest describeResponse : describeResponses) {
					/* If the state is open, it hasn't changed since we attempted
					 * to request it. There is the potential for it to transition
					 * almost immediately to closed or cancelled so we compare
					 * against open instead of active.
					 */
					if (describeResponse.getState().equals("open")) {
						anyOpen = true;
						break;
					}
				}
			} catch (@SuppressWarnings("unused") AmazonServiceException e) {
				anyOpen = true;
			}
			
			try {
				/* Sleep for 60 seconds */
				Thread.sleep(60 * 1000);
			} catch (@SuppressWarnings("unused") Exception e) {
				/* Do nothing because it woke up early */
			}
		} while (anyOpen);
	}
	
	/**
	 * Cancel a request for spotInstances. If the request hasn't been processed
	 * yet, it will cancel the request. Already created instances must be
	 * terminated with the terminateInstanceRequest method.
	 * 
	 * @param spotInstanceRequestIds	A list of spotInstance requests.
	 */
	public void cancelSpotInstanceRequest(List<String> spotInstanceRequestIds) {
		try {
			CancelSpotInstanceRequestsRequest cancelRequest = 
					new CancelSpotInstanceRequestsRequest(spotInstanceRequestIds);
			client.cancelSpotInstanceRequests(cancelRequest);
		} catch (AmazonServiceException e) {
			/* Write out any exception that may have occurred */
			logger.error("Error cancelling instances");
			logger.error("Caught Exception: " + e.getMessage());
			logger.error("Response Status Code: " + e.getStatusCode());
			logger.error("Error code: " + e.getErrorCode());
			logger.error("Request ID: " + e.getRequestId());
		}
	}
	
	/**
	 * Terminate all the instances listed in the instanceIds list.
	 *   
	 * @param instanceIds	A list of instance ids.
	 */
	public void terminateInstanceRequest(List<String> instances) {
		try {
			/* Terminate Instances */
			TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(instances);
			client.terminateInstances(terminateRequest);
			logger.info("Instances successfully terminated");
			logger.info(terminateRequest);
		} catch (AmazonServiceException e) {
			/* Write out any exception that may have occurred */
			logger.error("Error cancelling instances");
			logger.error("Caught Exception: " + e.getMessage());
			logger.error("Response Status Code: " + e.getStatusCode());
			logger.error("Error code: " + e.getErrorCode());
			logger.error("Request ID: " + e.getRequestId());
		}
	}
}
