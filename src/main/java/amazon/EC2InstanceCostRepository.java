package amazon;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EC2InstanceCostRepository extends MongoRepository<EC2InstanceCost, String> {

    public List<EC2InstanceCost> findByType(InstanceOSType type);
    public List<EC2InstanceCost> findByPrice(double price);

}
