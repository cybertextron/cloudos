package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum InstanceType {

	/* General Purpose */
	T2_NANO ("t2.nano", true),
	T2_MICRO ("t2.micro", true),
	T2_SMALL ("t2.small", true),
	T2_MEDIUM ("t2.medium", true),
	T2_LARGE ("t2.large", true),
	M4_LARGE ("m4.large", true),
	M4_XLARGE ("m4.xlarge", true),
	M4_2XLARGE ("m4.2xlarge", true),
	M4_4XLARGE ("m4.4xlarge", true),
	M4_10XLARGE ("m4.10xlarge", true),
	M3_MEDIUM ("m3.medium", true),
	M3_LARGE ("m3.large", true),
	M3_XLARGE ("m3.xlarge", true),
	M3_2XLARGE ("m3.2xlarge", true),

	/* Computer Optimized */
	C4_LARGE ("c4.large", true),
	C4_XLARGE ("c4.xlarge", true),
	C4_2XLARGE ("c4.2xlarge", true),
	C4_4XLARGE ("c4.4xlarge", true),
	C4_8XLARGE ("c4.8xlarge", true),
	C3_LARGE ("c3.large", true),
	C3_XLARGE ("c3.xlarge", true),
	C3_2XLARGE ("c3.2xlarge", true),
	C3_4XLARGE ("c3.4xlarge", true),
	C3_8XLARGE ("c3.8xlarge", true),

	/* Memory Optimized */
	R3_LARGE ("r3.large", true),
	R3_XLARGE ("r3.xlarge", true),
	R3_2XLARGE ("r3.2xlarge", true),
	R3_4XLARGE ("r3.4xlarge", true),
	R3_8XLARGE ("r3.8xlarge", true),
	/* GPU */
	G2_2XLARGE ("g2.2xlarge", true),
	G2_8XLARGE ("g2.8xlarge", true),
	/* Storage Optimized */
	I2_XLARGE ("i2.xlarge", true),
	I2_2XLARGE ("i2.2xlarge", true),
	I2_4XLARGE ("i2.4xlarge", true),
	I2_8XLARGE ("i2.8xlarge", true),
	
	/* Dense Storage */
	D2_XLARGE ("d2.xlarge", true),
	D2_2XLARGE ("d2.2xlarge", true),
	D2_4XLARGE ("d2.4xlarge", true),
	D2_8XLARGE ("d2.8xlarge", true),
	
	/* General Purpose, previous generation */
	M1_SMALL ("m1.small", false),
	M1_MEDIUM ("m1.medium", false),
	M1_LARGE ("m1.large", false),
	M1_XLARGE ("m1.xlarge", false),
	C1_MEDIUM ("c1.medium", false),
	C1_XLARGE ("c1.xlarge", false),
	CC2_8XLARGE ("cc2.8xlarge", false),
	CG1_4XLARGE ("cg1.4xlarge", false),
	M2_XLARGE ("m2.xlarge", false),
	M2_2XLARGE ("m2.2xlarge", false),
	M2_4XLARGE ("m2.4xlarge", false),
	CR1_8XLARGE ("cr1.8xlarge", false),
	HI1_4XLARGE ("hi1.4xlarge", false),
	HS1_8XLARGE ("hs1.8xlarge", false),
	T1_MICRO ("t1.micro", false);

	private final String type;
	private final boolean currentGeneration;
	
	private InstanceType(String type, boolean current) {
		this.type = type;
		this.currentGeneration = current;
	}
	
	@Override
	public String toString() {
		return String.format(
                "User[type='%s', Current Generation=%b]",
                this.type, this.currentGeneration);
	}

}
