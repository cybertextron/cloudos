package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum AWSNetworkingPerformance {
	LOW,
	LOW_TO_MODERATE,
	MODERATE,
	HIGH,
	GIGABIT_10,
}
