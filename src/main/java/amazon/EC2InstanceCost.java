package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ec2_instances_cost")
public class EC2InstanceCost {

	@Id
	String id;
	private InstanceOSType type;
	private double price;
	private TimeUnit unit;
	
	public EC2InstanceCost() { }
	
	public EC2InstanceCost(InstanceOSType type, double price, TimeUnit unit) {
		this.type = type;
		this.price = price;
		this.unit = unit;
	}
	
    @Override
    public String toString() {
        return String.format(
                "User[id=%s, type='%s', price='%s', unit='%s']",
                this.id, this.type, this.price, this.unit);
    }
	
}
