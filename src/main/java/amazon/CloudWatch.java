package amazon;

import org.apache.log4j.Logger;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class CloudWatch {

	public final static Logger logger = Logger.getLogger( CloudWatch.class );
}
