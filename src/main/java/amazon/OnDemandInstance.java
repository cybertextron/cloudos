package amazon;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;

/**
 * Define an object for the Reserved Instances in AWS. This object is
 * composed of a GroupName, which must exists before the object is
 * instantiated, and a keyPairName.
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class OnDemandInstance extends EC2 {

	/**
	 * Default constructor
	 */
	public OnDemandInstance() {
	}

	/**
	 * Constructor for the OnDemandInstance class.
	 * 
	 * @param securityGroup
	 *            The Security Group this On-Demand instance will belong to.
	 */
	public OnDemandInstance(String securityGroup) {
		super(securityGroup);
		this.instanceIds = new LinkedList<>();
	}

	/**
	 * Create a new On-Demand instance request. The user has the option to
	 * specify how many instances must be created. The maxCount bound is 
	 * 10,000 instances.
	 * 
	 * @param ami
	 * 			The AMI ID.
	 * @param instance
	 * 			The type of instance. (e.g 'm1.small')
	 * @param minCount
	 * 			The minimum number of instances to be created.
	 * @param maxCount
	 * 			The maximum number of instances to be created. Currently bounded
	 * 			at 10,000.
	 * @param keyPairName
	 * 			The name of the key pair for AWS.
	 * @param securityGroup
	 * 			The name of the security group.
	 */
	public void createInstance(String ami, String instanceType, Integer minCount, Integer maxCount, String keyPairName,
			String securityGroup) {

		assert client != null;
		/* add boundaries, e.g max number of instances allowed to be created */
		assert minCount >= 1 && minCount <= maxCount;
		assert maxCount <= 10000;
		
		// Create a new request to create AWS instances.
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId(ami).withInstanceType(instanceType).withMinCount(minCount).withMaxCount(maxCount)
				.withKeyName(keyPairName).withSecurityGroups(securityGroup);
		RunInstancesResult runInstancesResult = client.runInstances(runInstancesRequest);
		logger.info(runInstancesResult);
		List<Instance> instances = runInstancesResult.getReservation().getInstances();
		// Store the new instance IDs to the list.
		for (Instance instance : instances) {
			String current = instance.getInstanceId();
			logger.info(String.format("Added instance '%s'", current));
			this.instanceIds.add(current);
			if (instance.getPublicIpAddress() != null) {
				this.addresses.put(current, instance.getPublicIpAddress());
			}
		}
		try {
			this.waitUntilRunning();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public List<String> getInstancesPublicIP() {
		List<String> publicIpList = new ArrayList<>();
		DescribeInstancesRequest request = new DescribeInstancesRequest().withInstanceIds(this.instanceIds);
		DescribeInstancesResult result= client.describeInstances(request);
		List <Reservation> list  = result.getReservations();
		
		for (Reservation res : list) {
			List <Instance> instanceList= res.getInstances();
			for (Instance instance : instanceList) {
				// Select only the instances marked as running.
				if ("running".equalsIgnoreCase(instance.getState().getName())) {
					publicIpList.add(instance.getPublicIpAddress());
				}
			}     
		}
		return publicIpList;
	}
	
	/**
	 * 	
	 * @throws InterruptedException
	 */
	public void waitUntilRunning() throws InterruptedException { 
        int attempts = 0; 
        while (true) { 
            attempts++; 
            Thread.sleep(1000*30);
            List<InstanceStatus> statuses = client.describeInstanceStatus(new DescribeInstanceStatusRequest() 
                .withInstanceIds(this.instanceIds)).getInstanceStatuses(); 
 
            if (statuses.size() < this.instanceIds.size()) { 
                logger.info("status is not synced, continue to wait"); 
                continue; 
            } 
 
            for (InstanceStatus status : statuses) { 
                logger.info(String.format("instance status %s => %s, checks => %s, %s", 
			                    status.getInstanceId(), 
			                    status.getInstanceState().getName(), 
			                    status.getSystemStatus().getStatus(), 
			                    status.getInstanceStatus().getStatus())); 
            } 
 
            boolean allOK = statuses.stream().allMatch(status -> 
                "running".equalsIgnoreCase(status.getInstanceState().getName()) 
                    && "ok".equalsIgnoreCase(status.getSystemStatus().getStatus()) 
                    && "ok".equalsIgnoreCase(status.getInstanceStatus().getStatus())); 
 
            if (allOK) { 
                break; 
            } else if (attempts > 20) { // roughly after 10 mins 
                throw new Error("waited too long to get instance status, something is wrong, please check aws console"); 
            } 
        } 
    } 

	/**
	 * Return a string representation of the class.
	 * 
	 * @return
	 * 		The String representation of a On Demand instance.
	 */
	@Override
	public String toString() {
		/**
		 * Define the toString method for the ReservedInstance class.
		 */
		return String.format("ReservedInstance[EC2 client=%s]", client.getClass());
	}

	/**
	 * 
	 * @param runInstances
	 * @param tagname
	 */
	public void tagEC2Instances(RunInstancesResult runInstances, String tagname) {
		List<Instance> instances = runInstances.getReservation().getInstances();
		int index = 1;
		for (Instance instance : instances) {
			CreateTagsRequest createTagsRequest = new CreateTagsRequest();
			createTagsRequest.withResources(instance.getInstanceId()) //
					.withTags(new Tag("Name", tagname + index));
			client.createTags(createTagsRequest);
			index++;
		}
	}
}
