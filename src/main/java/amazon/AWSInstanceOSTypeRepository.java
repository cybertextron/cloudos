package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AWSInstanceOSTypeRepository extends MongoRepository<AWSInstanceOSType, String> {
	
	public InstanceOSType findByosType(InstanceOSType osType);

}
