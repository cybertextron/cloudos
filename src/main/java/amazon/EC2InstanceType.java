package amazon;

/**
 * Define all the types of AWS EC2 Instances that can be created
 * Currently there are 4
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum EC2InstanceType {

	EBS_OPTIMIZED_INSTANCE,
	ON_DEMAND_INSTANCE,
	RESERVERD_INSTANCE,
	SPOT_INSTANCE,
	
}
