package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;

import cloudos.Config;

public class AWSCredentialSingleton {

    private static AWSCredentials credentials;

    protected AWSCredentialSingleton() {
        /* Exists only to defeat instantiation */
    }

    public static AWSCredentials getCredentials() {
        if ( credentials == null ) {
        	credentials = new BasicAWSCredentials(Config.accessKeyId,
        										  Config.secretAccessKey);
        }
        return credentials;
    }
}
