package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
/**
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.CreateDBInstanceRequest;
*/
public class RDS {
/**
	public final static Logger logger = Logger.getLogger( RDS.class );
	
	private AmazonRDS client;
	private SecurityGroup securityGroup;
	private Regions region;

	public RDS(SecurityGroup securityGroup, Regions region) { 
		try {
			this.client = new AmazonRDSClient( AWSCredentialSingleton.getCredentials() );
			this.securityGroup = securityGroup;
			this.region = region;
		} catch (Exception ex) {
            logger.error( ex.getMessage() );
        }
	}
	
	public void createDBInstance(String databaseName, Integer storage, String dbInstance,
			                     String engine, String username, String password,
			                     String instanceIdentifier, boolean multiAz) {
		try {
			List<String> ids = new ArrayList<String>();
            ids.add(this.securityGroup.getGroupId());
			CreateDBInstanceRequest createDBInstanceRequest = new CreateDBInstanceRequest();
			createDBInstanceRequest
				.withDBName(databaseName)
                .withAllocatedStorage(storage)
                .withDBInstanceClass(dbInstance)
                .withEngine(engine)
                .withMasterUsername(username)
                .withMasterUserPassword(password)
                .withDBInstanceIdentifier(instanceIdentifier)
                .withMultiAZ(multiAz)
                .withVpcSecurityGroupIds(ids);
		} catch (Exception ex) {
			logger.error( ex.getMessage() );
		}
	}
	*/
}
