package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.StringUtils;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;

import org.apache.log4j.Logger;

public class S3 {

    private AmazonS3 client;
    private Region region;
    private final String SUFFIX;
    public final static Logger logger = Logger.getLogger(S3.class);

    public S3() {
    	ClientConfiguration clientConfig = new ClientConfiguration();
    	clientConfig.setProtocol(Protocol.HTTP);
    	
    	this.client = new AmazonS3Client( AWSCredentialSingleton.getCredentials(),
    									  clientConfig );
        this.region = Region.getRegion(Regions.US_WEST_2);
        this.client.setRegion(this.region);
        this.SUFFIX = "/";
    }

    /**
     * Create a new bucket in S3, given the bucket name
     * 
     * @param bucketname the name of the bucket
     */
    public void createBucket(String bucketname) {
        try {
            assert bucketname != null;
            logger.info("Creating bucket" + bucketname + "\n");
            this.client.createBucket(bucketname);
        }catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.error("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
        }
    }

    /**
     * Print a list of all the buckets in this account.
     */
    public void listBuckets() {
    	try { 
	        List<Bucket> buckets = this.client.listBuckets();
	        for (Bucket bucket : buckets) {
	            logger.info( bucket.getName() + "\t" +
	                         StringUtils.fromDate(bucket.getCreationDate()) );
	        }
        } catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Make an existing object in S3 public.
     * 
     * @param bucketname
     * @param filename
     */
    public void makeObjectPublic( String bucketname, String filename ) {
    	try {
    		this.client.setObjectAcl( bucketname, filename,
                                  	  CannedAccessControlList.PublicRead );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Make an existing object in S3 private.
     * 
     * @param bucketname
     * @param filename
     */
    public void makeObjectPrivate( String bucketname, String filename ) {
    	try {
	        this.client.setObjectAcl( bucketname, filename,
	                                  CannedAccessControlList.Private );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Download a file in S3 to the local path.
     * 
     * @param bucketname
     * @param filepath
     */
    public void downloadObject( String bucketname, String filepath ) {
    	try { 
	        Path path = Paths.get( filepath );
	        String filename = path.getFileName().toString();
	        this.client.getObject(
	                       new GetObjectRequest( bucketname, filename ),
	                       new File( filepath ) );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Upload a file to S3.
     * 
     * @param object
     * @param bucketname
     * @param filename
     */
    public void createObject( String object, String bucketname, String filename ) {
        try { 
        	ByteArrayInputStream input = new ByteArrayInputStream( object.getBytes() );
        	this.client.putObject( bucketname, filename, input, new ObjectMetadata() );
        } catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * List all the elements in a bucket
     * 
     * @param bucketname
     */
	public void listBucketContent( String bucketname ) {
		try {
	        ObjectListing objects = this.client.listObjects( bucketname );
	        do {
	            for (S3ObjectSummary objectSummary : objects.getObjectSummaries() ) {
	                logger.info( objectSummary.getKey() + "\t" +
	                			 objectSummary.getSize() + "\t" +
	                             StringUtils.fromDate(objectSummary.getLastModified()) );
	            }
	        } while ( objects.isTruncated() );
		} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

	/**
	 * Return a list of all the existing buckets in this account
	 * 
	 * @return
	 */
    public List<Bucket> getBucketList() {
    	try {
	        logger.info("Listing buckets");
	        return this.client.listBuckets();
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    	return null;
    }

    /**
     * Create a folder inside a bucket in S3.
     * 
     * @param bucketname
     * @param foldername
     */
    public void createFolder( String bucketname, String foldername ) {
    	try { 
	        /* create metadata for the folder and set content-length to 0.*/
	        ObjectMetadata metadata = new ObjectMetadata();
	        metadata.setContentLength( 0 );
	        /* create empty content */
	        InputStream emptyContent = new ByteArrayInputStream( new byte[0] );
	        /* Create a PutObjectRequest passing the folder name suffixed by / */
	        PutObjectRequest putObjectRequest = new PutObjectRequest( bucketname,
	                                                 foldername + this.SUFFIX,
	                                                 emptyContent, metadata );
	        /* send request to S3 to create folder */
	        this.client.putObject( putObjectRequest );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Upload a file to a bucket in S3.
     * 
     * @param bucketname
     * @param foldername
     * @param filename
     * @param path
     */
    public void uploadFile( String bucketname, String keyname, String filepath ) {
    	try { 
    		logger.info("Uploading a new object to S3 from a file: " + keyname);
	        this.client.putObject( new PutObjectRequest( bucketname, keyname,
	                                                     new File( filepath ) ));
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Delete a bucket, if it exists.
     * 
     * @param bucketname
     */
    public void deleteBucket( String bucketname ) {
    	try { 
    		this.client.deleteBucket( bucketname );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Delete all the files in a bucket.
     * 
     * @param bucketname
     * @param filename
     */
    public void deleteFiles( String bucketname, String filename ) {
    	try { 
    		this.client.deleteObject( bucketname, filename );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Delete all a folder in a bucket. In order to delete the folder,
     * all the files in that folder have to be deleted first.
     * 
     * @param bucketname
     * @param foldername
     */
    public void deleteFolder( String bucketname, String foldername ) {
    	try { 
	        List<S3ObjectSummary> fileList = this.client.listObjects( bucketname, foldername )
	                                              					.getObjectSummaries();
	        for (S3ObjectSummary file : fileList ) {
	            this.client.deleteObject( bucketname, file.getKey() );
	        }
	        this.client.deleteObject( bucketname, foldername );
    	} catch (AmazonClientException ace) {
        	logger.error(ace.getMessage());
        }
    }

    /**
     * Produce a URL object, which can be fetched for download.
     * 
     * @param bucketname
     * @param filename
     * @return
     */
    public URL generateDownloadURL( String bucketname, String filename ) {
        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(
                                                          bucketname, filename );
        return this.client.generatePresignedUrl( request );
    }
}
