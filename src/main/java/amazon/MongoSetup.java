package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amazonaws.regions.Regions;

@SpringBootApplication
public class MongoSetup implements CommandLineRunner {

	@Autowired
	private AWSRegionsRepository awsRegions;
	@Autowired
	private AWSInstanceTypeRepository typeRepository;
	@Autowired
	private AWSInstanceOSTypeRepository osTypeRepository;
	@Autowired
	private AWSInstanceFamilyRepository familyRepository;

	/* Logger */
	private final static Logger logger = Logger.getLogger( MongoSetup.class );
	
	/**
	public static void main(String[] args) {
		// Create and populate the Amazon AWS Regions
		SpringApplication.run(MongoSetup.class, args);
	}
	*/
	
	@Override
	public void run(String...args) throws Exception {
		
		/* Save all the AWSRegions */
		this.awsRegions.deleteAll();
		for (Regions r : Regions.values()) {
			logger.info(r);
		    AWSRegions region = new AWSRegions(r);
		    this.awsRegions.save(region);
		}
		
		/* Save all the AWS Instance Types */
		this.typeRepository.deleteAll();
		for (InstanceType type : InstanceType.values()) {
			logger.info(type);
			AWSInstanceType instanceType = new AWSInstanceType(type);
			this.typeRepository.save(instanceType);
		}
		
		/* Save all the AWS Instance OS types */
		this.osTypeRepository.deleteAll();
		for (InstanceOSType osType : InstanceOSType.values()) {
			logger.info(osType);
			AWSInstanceOSType instanceOS = new AWSInstanceOSType(osType);
			this.osTypeRepository.save(instanceOS);
		}
		
		/* Save all the AWS Instance Family */
		this.familyRepository.deleteAll();
		for (InstanceFamily instanceFamily : InstanceFamily.values()) {
			logger.info(instanceFamily);
			AWSInstanceFamily awsInstanceFamily = new AWSInstanceFamily(instanceFamily);
			this.familyRepository.save(awsInstanceFamily);
		}
	}
	
}
