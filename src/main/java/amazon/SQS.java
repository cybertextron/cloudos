package amazon;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public class SQS {

	/* Make the AmazonEC2 client static, so it can be used by other objects */
	private AmazonSQS sqs;
	private Region region;
	
	/* Logger */
	public final static Logger logger = Logger.getLogger( SQS.class );
	
	/**
	 * Default constructor
	 */
	public SQS() {
	}
	
	/**
	 * 
	 * @param client
	 * @param regions
	 */
	public SQS(Regions regions) {
		this.sqs = new AmazonSQSClient(AWSCredentialSingleton.getCredentials());
		this.region = Region.getRegion(regions);
		this.sqs.setRegion(this.region);
		logger.info("===========================================");
		logger.info("Getting Started with Amazon SQS");
		logger.info("===========================================");
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public String createQueue(String name) {
		try {
			logger.info(String.format("Creating a new SQS queue called '%s'.", name));
			CreateQueueRequest request = new CreateQueueRequest(name);
			String url = this.sqs.createQueue(request).getQueueUrl();
			return url;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return null;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return null;
        }
	}
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	public List<Message> readMessages(String url) {
		try {
			logger.info(String.format("Receiving messages from '%s'", url));
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(url);
            List<Message> messages = this.sqs.receiveMessage(receiveMessageRequest).getMessages();
            return messages;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return null;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return null;
        }
	}
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	public boolean deleteQueue(String url) {
		try {
			logger.info(String.format("Deleting the queue '%s'", url));
			 this.sqs.deleteQueue(new DeleteQueueRequest(url));
			return true;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return false;
        }
	}

	/**
	 * 
	 * @param message
	 * @param url
	 * @return
	 */
	public boolean deleteMessage(Message message, String url) {
		try {
			logger.info("Deleting a message");
			 String messageReceiptHandle = message.getReceiptHandle();
			 this.sqs.deleteMessage(new DeleteMessageRequest(url, messageReceiptHandle));
			return true;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return false;
        }
	}

	/**
	 * 
	 * @param messages
	 */
	public void readContent(List<Message> messages) {
		for (Message message : messages) {
			logger.info("Message");
			logger.info(String.format("Message ID: '%s'", message.getMessageId()));
			logger.info(String.format("Receipt Handler: '%s'", message.getReceiptHandle()));
			logger.info(String.format("MD5 of body: '%s'", message.getMD5OfBody()));
			logger.info(String.format("Body: '%s'", message.getBody()));
			for (Entry<String, String> entry : message.getAttributes().entrySet()) {
				logger.info("Attribute");
				logger.info(String.format("Name: ", entry.getKey()));
				logger.info(String.format("Value: ", entry.getValue()));
			}
		}
	}

	/**
	 * 
	 * @param url
	 * @param message
	 * @return
	 */
	public boolean sendMessage(String url, String message) {
		try {
			logger.info(String.format("Sending a message to '%s'", url));
			this.sqs.sendMessage(new SendMessageRequest(url, message));
			return true;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return false;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return false;
        }
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getQueueList() {
		try {
			logger.info("Listing all queues in your account.");
			List<String> queues = new LinkedList<>();
			for (String queueUrl : this.sqs.listQueues().getQueueUrls()) {
                queues.add(queueUrl);
            }
			return queues;
		} catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            return null;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not "
        			+ "being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage());
        	return null;
        }
	}
}
