package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Define a Mongo model for all the Instances current supported by AWS.
 * 
 * @author philippesouzamoraesribeiro
 *
 */
@Document(collection = "aws_instance_type")
public class AWSInstanceType {

	@Id
	private String id;

	@Field(value = "instance_type")
	private InstanceType instanceType;
	
	public AWSInstanceType(InstanceType type) {
		this.instanceType = type;
	}

	@Override
	public String toString() {
		return String.format(
                "User[id=%s, type='%s']",
                this.id, this.instanceType);
	}
}
