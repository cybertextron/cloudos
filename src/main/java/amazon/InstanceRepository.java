/**
 * 
 */
package amazon;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Create an interface in other to search for the objects of the type Instance,
 * in the MongoDB collections.
 * 
 * @author philipperibeiro
 *
 */
public interface InstanceRepository extends MongoRepository<Instance, String> {

	// Search the 'price' field
	public List<Instance> findByPrice(String price);

	// Search for the 'vpcu' field
	public List<Instance> findByVcpu(String vcpu);

	// Search by the 'storage' field
	public List<Instance> findByStorage(String storage);

	// Search by the 'size' field
	public List<Instance> findBySize(String size);

	// Search by the 'memory' field
	public List<Instance> findByMemory(String memory);

	// Search by the 'ecu' field
	public List<Instance> findByEcu(String ecu);

	// Search by the 'name' field.
	public List<Instance> findByName(String name);

}
