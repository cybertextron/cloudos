package amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AWSInstanceFamilyRepository extends MongoRepository<AWSInstanceFamily, String> {
	
	/* Only one needed ... search by the Instance Family */
	public InstanceFamily findByInstanceFamily(InstanceFamily instanceFamily);

}
