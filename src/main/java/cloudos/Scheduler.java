package cloudos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.primitives.Doubles;
import com.mongodb.client.MongoDatabase;

import amazon.OnDemandInstance;
import healthchecker.MongoDb;
import healthchecker.Utils;
import machinelearning.LinearRegression;
import machinelearning.MathVector;

/**
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/10/2016
 */

public class Scheduler {

	public final static Logger logger = Logger.getLogger(Scheduler.class);
	private String securityGroup;
	private String keyPair;
	private OnDemandInstance onDemand;
	private static MongoDatabase database;

	private LinearRegression linearRegression;
	/**
	 * 
	 * @param securityGroup
	 * @param keyPair
	 * @param databaseName
	 */
	public Scheduler(String securityGroup, String keyPair, String databaseName) {
		this.securityGroup = securityGroup;
		this.keyPair = keyPair;
		this.onDemand = new OnDemandInstance(this.securityGroup);
		database = MongoDb.getDatabase(databaseName);
		this.linearRegression = new LinearRegression();
	}

	public Scheduler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param collectionName
	 */
	@Autowired
	public void solve(String collectionName) {
		List<Document> documents = MongoDb.getDocuments(database, collectionName);
		List<Double> prices = new ArrayList<>();
		List<MathVector> values = new ArrayList<>();
		for (Document document : documents) {
			try {
				MathVector vector = new MathVector();
				if (document.containsKey("vcpu")) {
					double vcpu = Double.parseDouble((String) document.get("vcpu"));
					vector.add(vcpu);
				}
				if (document.containsKey("ecu")) {
					double ecu = Double.parseDouble((String) document.get("ecu"));
					vector.add(ecu);
				}
				if (document.containsKey("memory")) {
					double memory = Double.parseDouble((String) document.get("memory"));
					vector.add(memory);
				}
				/**
				if (document.containsKey("storage")) {
					String storage = (String) document.get("storage");
					double value = this.getStorage(storage);
					vector.add(value);
				}
				*/
				if (vector.length() == 3) {
					values.add(vector);
					if (document.containsKey("price")) {
						Double price = Double.parseDouble((String) document.get("price"));
						prices.add(price);
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		double[] y = Utils.toDoubleArray(prices);
		this.linearRegression.multiLinearRegression(values, y);
		logger.info(this.linearRegression.intercept());
		logger.info(this.linearRegression.toString());
		this.onDemand.createInstance("ami-4b814f22", "m1.small", new Integer(1), new Integer(10),
									 this.keyPair, this.securityGroup);
	}

	/**
	 * 
	 * @param ami
	 * @param instanceSize
	 * @param quantity
	 */
	public synchronized void launchInstances(String ami, String instanceSize, Integer quantity) {
		assert quantity >= 1;
		this.onDemand.createInstance(ami, instanceSize, new Integer(1), quantity, this.keyPair, this.securityGroup);
	}

	/**
	 * 
	 */
	public void close() {
		if (this.onDemand != null) {
			this.onDemand.terminate();
		}
	}

}
