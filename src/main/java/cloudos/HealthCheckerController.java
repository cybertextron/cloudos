package cloudos;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import healthchecker.DiskFile;
import healthchecker.DiskFileRepository;
import healthchecker.IOTop;
import healthchecker.IOTopRepository;
import healthchecker.Instance;
import healthchecker.InstanceRepository;
import healthchecker.Iostat;
import healthchecker.IostatRepository;
import healthchecker.Lsof;
import healthchecker.LsofRepository;
import healthchecker.Netstat;
import healthchecker.NetstatRepository;
import healthchecker.ProcessService;
import healthchecker.ProcessServiceRepository;
import healthchecker.Top;
import healthchecker.TopRepository;
import healthchecker.Vmstat;
import healthchecker.VmstatRepository;

/* Import the healthchecker package */

/**
 * Implement the HealthChecker controller, so instances can register themselves
 * and start sending information from the instances in the cloud provider to the
 * CloudOS ML modules.
 * 
 * @author Philippe Ribeiro
 *
 */
@Controller
@RestController
@ComponentScan("healthchecker")
public class HealthCheckerController {

	@Autowired
	private TopRepository topRepository;

	@Autowired
	private InstanceRepository instanceRepository;

	@Autowired
	private VmstatRepository vmstatRepository;

	@Autowired
	private ProcessServiceRepository psRepository;

	@Autowired
	private NetstatRepository netstatRepository;

	@Autowired
	private DiskFileRepository dfRepository;

	@Autowired
	private IostatRepository iostatRepository;

	@Autowired
	private IOTopRepository iotopRepository;

	@Autowired
	private LsofRepository lsofRepository;

	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(HealthCheckerController.class);

	/**
	 * Constructor for the HealthCheckerController. It takes an
	 * InstanceRepository object so the controller can be unit tested.
	 * 
	 * @param instanceRepository:
	 *            A InstanceRepository object.
	 */
	public HealthCheckerController(InstanceRepository instanceRepository, LsofRepository lsofRepository,
								   ProcessServiceRepository psRepository, VmstatRepository vmstatRepository,
								   TopRepository topRepository, NetstatRepository netstatRepository,
								   DiskFileRepository dfRepository) {
		this.instanceRepository = instanceRepository;
		this.lsofRepository = lsofRepository;
		this.psRepository = psRepository;
		this.vmstatRepository = vmstatRepository;
		this.topRepository = topRepository;
		this.netstatRepository = netstatRepository;
		this.dfRepository = dfRepository;
	}

	/**
	 * Get a particular instance, based on its id.
	 * 
	 * @param instance:
	 *            An instance id
	 * @return: the instance, if one exists.
	 */
	@RequestMapping(value = "/healthchecker/{instance}", method = RequestMethod.GET)
	@ResponseBody
	public Instance getInstance(@PathVariable String instance) {
		return this.instanceRepository.findOne(instance);
	}

	/**
	 * Return all the instances stored into MongoDb.
	 * 
	 * @return: A list of all the Instances currently in Mongo.
	 */
	@RequestMapping("/healthchecker")
	@ResponseBody
	public ModelAndView index() {
		List<Instance> instances = this.instanceRepository.findAll();
		ModelAndView model = new ModelAndView("healthchecker");
		model.addObject("instances", instances);
		return model;
	}

	/**
	 * Implement the create API for a new instance to register itself with the
	 * health checker controller.
	 * 
	 * @param instance:
	 *            the new Instance object to be inserted into the MongoDB.
	 * @return: the newly created Instance object.
	 */
	@RequestMapping(value = "/healthchecker", method = RequestMethod.POST)
	@ResponseBody
	public Instance create(@RequestBody Instance instance) {
		return this.instanceRepository.save(instance);
	}

	/**
	 * Delete an existing instance, based on its ID.
	 * 
	 * @param instance:
	 *            The instance's ID.
	 */
	@RequestMapping(value = "/healthchecker/{instance}", method = RequestMethod.DELETE)
	public void delete(@PathVariable String instance) {
		this.instanceRepository.delete(instance);
	}

	/**
	 * 
	 * @param instance
	 * @param iotop
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iotop", method = RequestMethod.POST)
	@ResponseBody
	public IOTop createIOTop(@RequestBody IOTop iotop) {
		return this.iotopRepository.save(iotop);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iotop", method = RequestMethod.GET)
	@ResponseBody
	public List<IOTop> getIOTop(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.iotopRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iotop", method = RequestMethod.DELETE)
	public void deleteIOTop(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<IOTop> iotop = this.iotopRepository.findByInstance(current.getId());
			this.iotopRepository.delete(iotop);
		}
	}

	/**
	 * 
	 * @param instance
	 * @param lsof
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/lsof", method = RequestMethod.POST)
	@ResponseBody
	public Lsof createLsof(@RequestBody Lsof lsof) {
		return this.lsofRepository.save(lsof);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/lsof", method = RequestMethod.GET)
	@ResponseBody
	public List<Lsof> getLsof(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.lsofRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/lsof", method = RequestMethod.DELETE)
	public void deleteLsof(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<Lsof> lsof = this.lsofRepository.findByInstance(current.getId());
			this.lsofRepository.delete(lsof);
		}
	}

	/**
	 * Return the information produced by the `top` command in this particular
	 * instance.
	 * 
	 * @param instance:
	 *            The instance ID
	 * @return: A list of Top objects, representing the result contained within
	 *          `top`.
	 */
	@RequestMapping(value = "/healthchecker/{instance}/top", method = RequestMethod.GET)
	@ResponseBody
	public List<Top> getTopAll(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.topRepository.findByInstance(current.getId());
	}

	/**
	 * Delete all the top instances in Mongo associated with the instance ID.
	 * 
	 * @param instance:
	 *            The Instance ID
	 */
	@RequestMapping(value = "/healthchecker/{instance}/top", method = RequestMethod.DELETE)
	public void deleteTop(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<Top> top = this.topRepository.findByInstance(current.getId());
			this.topRepository.delete(top);
		}
	}

	/**
	 * Insert new values obtained by the `top` command from the given instance
	 * into MongoDB.
	 * 
	 * @param instance:
	 *            The instance ID.
	 * @param top:
	 *            A list of Top objects, representing each row of the `top`
	 *            command.
	 * @return: A list of Top objects, newly inserted into the database.
	 */
	@RequestMapping(value = "/healthchecker/{instance}/top", method = RequestMethod.POST)
	@ResponseBody
	public Top createTop(@RequestBody Top top) {
		return this.topRepository.save(top);
	}

	/**
	 * 
	 * @param instance
	 * @param iostat
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iostat", method = RequestMethod.POST)
	public Iostat createIostat(@RequestBody Iostat iostat) {
		return this.iostatRepository.save(iostat);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iostat", method = RequestMethod.GET)
	public List<Iostat> getIostast(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.iostatRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/iostat", method = RequestMethod.DELETE)
	public void deleteIostat(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<Iostat> iostat = this.iostatRepository.findByInstance(current.getId());
			this.iostatRepository.delete(iostat);
		}
	}

	/**
	 * 
	 * @param instance
	 * @param df
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/disk_file", method = RequestMethod.POST)
	public DiskFile createDiskFile(@RequestBody DiskFile df) {
		return this.dfRepository.save(df);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/disk_file", method = RequestMethod.GET)
	public List<DiskFile> getDiskFile(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.dfRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/disk_file", method = RequestMethod.DELETE)
	public void deleteDiskFile(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<DiskFile> df = this.dfRepository.findByInstance(current.getId());
			this.dfRepository.delete(df);
		}
	}

	/**
	 * 
	 * @param instance
	 * @param netstat
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/netstat", method = RequestMethod.POST)
	public Netstat createNetstat(@RequestBody Netstat netstat) {
		return this.netstatRepository.save(netstat);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/netstat", method = RequestMethod.GET)
	public List<Netstat> getNetstat(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.netstatRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/netstat", method = RequestMethod.DELETE)
	public void deleteNetstat(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<Netstat> netstat = this.netstatRepository.findByInstance(current.getId());
			this.netstatRepository.delete(netstat);
		}
	}

	/**
	 * 
	 * @param instance
	 * @param vmstat
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/vmstat", method = RequestMethod.POST)
	public Vmstat createVmstat(@RequestBody Vmstat vmstat) {
		return this.vmstatRepository.save(vmstat);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/vmstat", method = RequestMethod.GET)
	public List<Vmstat> getVmstat(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.vmstatRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/vmstat", method = RequestMethod.DELETE)
	public void deleteVmstat(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<Vmstat> vmstat = this.vmstatRepository.findByInstance(current.getId());
			this.vmstatRepository.delete(vmstat);
		}
	}

	/**
	 * 
	 * @param instance
	 * @param ps
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/process_service", method = RequestMethod.POST)
	public ProcessService createProcessService(@RequestBody ProcessService ps) {
		return this.psRepository.save(ps);
	}

	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/healthchecker/{instance}/process_service", method = RequestMethod.GET)
	public List<ProcessService> getProcessService(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current == null) {
			return new ArrayList<>();
		}
		return this.psRepository.findByInstance(current.getId());
	}

	/**
	 * 
	 * @param instance
	 */
	@RequestMapping(value = "/healthchecker/{instance}/process_service", method = RequestMethod.DELETE)
	public void deleteProcessService(@PathVariable String instance) {
		Instance current = this.instanceRepository.findOne(instance);
		if (current != null) {
			List<ProcessService> ps = this.psRepository.findByInstance(current.getId());
			this.psRepository.delete(ps);
		}
	}
}
