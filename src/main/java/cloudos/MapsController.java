package cloudos;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MapsController {

	@RequestMapping("/google_map")
	public String googleMap(Model model) {
		return "google_map";
	}
	
	@RequestMapping("/vector_map")
	public String vectorMap(Model model) {
		return "vector_map";
	}

}
