package cloudos;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;

import amazon.AWSCredentialSingleton;
import amazon.Key;
import amazon.OnDemandInstance;
import builds.Ansible;
import builds.Build;

@Controller
@ComponentScan("builds")
public class BuildsController {
	
	// @Autowired
	// private BuildsRepository buildsRepository;
	private final static Logger logger = Logger.getLogger(BuildsController.class);
	
	private final String securityGroup = "default";
	private final String keyPairName = "test_deploy";
	private Key key;
	private String keyPair;
	private OnDemandInstance onDemand;
	protected AmazonEC2 client;
	
	public BuildsController() {
		this.client = new AmazonEC2Client(AWSCredentialSingleton.getCredentials());
		this.key = new Key();
		if (this.key.keyPairExists(keyPairName, client)) {
			this.key.deleteKey(keyPairName, client);
		}
		this.keyPair = this.key.saveKeyPair(keyPairName, client);
		this.onDemand = new OnDemandInstance(this.securityGroup);
	}
	
	/*
	 * Add the builds url page
	 */
	@GetMapping("/builds")
	public String index(Model model) {
		model.addAttribute("build", new Build());
		return "builds";
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 */
	@PostMapping("/builds")
	public Build deploy(@ModelAttribute Build build) {
		logger.info(String.format("--------- Deploying build: %s --------", build));
		Ansible ansible = new Ansible();
		// Deploy AWS instances
		logger.info("----------------------------------------------");
		logger.info("Deploying AWS EC2 instances");
		this.onDemand.createInstance("ami-59e8964e", "m1.small", new Integer(1), build.getInstances(), this.keyPairName,
				this.securityGroup);
		List<String> publicIpList = this.onDemand.getInstancesPublicIP();
		String ansiblePath = System.getProperty("user.home") + "/ansible/solr/provisioning/inventory";
		ansible.createInventoryFile(publicIpList, ansiblePath);
		logger.info("----------------------------------------------");
		logger.info("Deploying Ansible");
		ansible.deploy(build);
		logger.info("----------------------------------------------");
		
		return build;
	}
	
	/**
	 * 
	 */
	@Override
	public void finalize() {
		if (this.onDemand != null) {
			this.onDemand.terminate();
			this.key.deleteKey(this.keyPairName, client);
		}
	}
}
