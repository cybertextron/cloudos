package cloudos;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmailController {

	@RequestMapping("/email")
	@ResponseBody
	public ModelAndView index() {
		ModelAndView model = new ModelAndView("email");
		return model;
	}
	
	@RequestMapping("/email_compose")
	@ResponseBody
	public ModelAndView emailCompose() {
		ModelAndView model = new ModelAndView("email_compose");
		return model;
	}

}
