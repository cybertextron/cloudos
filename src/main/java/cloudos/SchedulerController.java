package cloudos;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SchedulerController {

	/* Import the logger for the Scheduler Controller */
	private static final Logger logger = Logger.getLogger(SchedulerController.class);

	@RequestMapping(value = "/builderScheduler", method = RequestMethod.GET)
	public String builderForm(Model model) {
		model.addAttribute("builder", new Scheduler());
		return "builder";
	}

	@RequestMapping(value = "/builderScheduler", method = RequestMethod.POST)
	public void deployScheduler(@ModelAttribute Scheduler scheduler, Model model) {
		model.addAttribute("builder", scheduler);
	}
}
