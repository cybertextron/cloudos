package cloudos;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/index").setViewName("index");
		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/signup").setViewName("signup");
		registry.addViewController("/hello").setViewName("hello");
		registry.addViewController("/healthchecker").setViewName("healthchecker");
		registry.addViewController("/builds").setViewName("builds");
		registry.addViewController("/dashboard").setViewName("dashboard");
		registry.addViewController("/social").setViewName("social");
		registry.addViewController("/email").setViewName("email");
		registry.addViewController("/calendar").setViewName("calendar");
		registry.addViewController("/google_maps").setViewName("google_maps");
		registry.addViewController("/vector_maps").setViewName("vector_maps");
		registry.addViewController("/register").setViewName("register");
		registry.addViewController("/scheduler").setViewName("scheduler");
		registry.addViewController("/configuration").setViewName("configuration");
	}

}
