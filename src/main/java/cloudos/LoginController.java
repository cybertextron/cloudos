package cloudos;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	/*
	 * Add the login redirect url
	 */
	@RequestMapping("/login")
	public String index() {
		return "login";
	}

	/*
	 * Add the login redirect url
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void login() {
	}

	@RequestMapping("/logout")
	public void logout() {

	}
}
