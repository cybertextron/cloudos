package cloudos;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

	/* Use the User collections */
	@Autowired
	private UserRepository repository;

	public final static Logger logger = Logger.getLogger(Application.class);

	public static void main(String[] args) throws Throwable {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		this.repository.deleteAll();

		// save a couple of users
		this.repository.save(new User("Alice", "Smith", "asmith", "alice123", "asmith@cloudos.com", null, null));
		this.repository.save(new User("Bob", "Smith", "bsmith", "bob123", "bsmith@cloudos.com", null, null));
		this.repository.save(new User("user", "", "user_cloudos", "password", "user_cloudos@cloudos.com", null, null));

		// fetch all users
		logger.info("Users found with findAll():");
		logger.info("-------------------------------");
		for (User user : this.repository.findAll()) {
			System.out.println(user);
		}

		// fetch an individual user
		logger.info("User found with findByFirstName('Alice'):");
		logger.info("--------------------------------");
		logger.info(this.repository.findByFirstName("Alice"));

		logger.info("Users found with findByLastName('Smith'):");
		logger.info("--------------------------------");
		for (User user : this.repository.findByLastName("Smith")) {
			logger.info(user);
		}

	}
}
