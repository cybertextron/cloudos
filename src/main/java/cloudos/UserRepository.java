package cloudos;

/**
 * Created by philippesouzamoraesribeiro on 12/12/15.
 */
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    public User findByFirstName(String firstname);
    public List<User> findByLastName(String lastname);
    public User findByEmail(String email);
    public User findByUsername(String username);

}
