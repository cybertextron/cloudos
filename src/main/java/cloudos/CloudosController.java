package cloudos;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@EnableAutoConfiguration
public class CloudosController {

	private static final Logger logger = Logger.getLogger(CloudosController.class);

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "Hello World!";
	}
}
