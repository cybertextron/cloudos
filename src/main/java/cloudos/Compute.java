package cloudos;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.aws.ec2.reference.AWSEC2Constants.PROPERTY_EC2_AMI_QUERY;
import static org.jclouds.aws.ec2.reference.AWSEC2Constants.PROPERTY_EC2_CC_AMI_QUERY;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.options.TemplateOptions.Builder.runScript;
import static org.jclouds.compute.predicates.NodePredicates.TERMINATED;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;
import static org.jclouds.scriptbuilder.domain.Statements.exec;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jclouds.ContextBuilder;
import org.jclouds.apis.ApiMetadata;
import org.jclouds.apis.Apis;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.RunScriptOnNodesException;
import org.jclouds.compute.domain.ComputeMetadata;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.Image;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.OsFamily;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.compute.options.TemplateOptions;
import org.jclouds.domain.Credentials;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.ec2.compute.options.EC2TemplateOptions;
import org.jclouds.enterprise.config.EnterpriseConfigurationModule;
import org.jclouds.googlecloud.GoogleCredentialsFromJson;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.providers.ProviderMetadata;
import org.jclouds.providers.Providers;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.AdminAccess;
import org.jclouds.sshj.config.SshjSshClientModule;

import com.google.common.base.Charsets;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.inject.Module;

/**
 * Demonstrates the use of {@link ComputeService}.
 * <p/>
 * Usage is:
 * {@code java MainApp provider identity credential groupName (add|exec|run|destroy)}
 * if {@code exec} is used, the following parameter is a command, which should
 * be passed in quotes
 * if {@code run} is used, the following parameter is a file to execute.
 */
public class Compute {

	public static final String PROPERTY_OAUTH_ENDPOINT = "oauth.endpoint";
	private static final Logger logger = Logger.getLogger(Compute.class);
	public static final Map<String, ApiMetadata> allApis = Maps.uniqueIndex(Apis.viewableAs(ComputeServiceContext.class),
		        Apis.idFunction());

	public static final Map<String, ProviderMetadata> appProviders = Maps.uniqueIndex(Providers.viewableAs(ComputeServiceContext.class),
        Providers.idFunction());

	public static final Set<String> allKeys = ImmutableSet.copyOf(Iterables.concat(appProviders.keySet(), allApis.keySet()));
		   
	private String provider;
	private String identity;
	private String credential;
	private String groupname;
	
	private ComputeService compute;
	private LoginCredentials login;
	/**
	 * 
	 * @param provider
	 * @param identity
	 * @param credential
	 * @param groupname
	 */
	public Compute(String provider, String identity, String credential, String groupname) {
		/* Set the initial variables */
		if (provider.equalsIgnoreCase("google-compute-engine")) {
			this.credential = this.getCredentialFromJsonKeyFile(credential);
		} else {
			this.setCredential(credential);
		}
		this.setGroupname(groupname);
		this.setIdentity(identity);
		this.setProvider(provider);
		
		this.login = this.getLoginForCommandExecution();
		this.compute = this.initComputeService();
	}
	
	public void add(Integer instances) {
		try {
			logger.info("------------------------------------------------------");
			logger.info(String.format(">> adding node to group %s%n", this.groupname));
			// Default template chooses the smallest size on an operating system
            // that tested to work with java, which tends to be Ubuntu or CentOS
            TemplateBuilder templateBuilder = this.compute.templateBuilder();
            // note this will create a user with the same name as you on the
            // node. ex. you can connect via ssh publicip
            Statement bootInstructions = AdminAccess.standard();

            // to run commands as root, we use the runScript option in the template.
            templateBuilder.options(runScript(bootInstructions));

            Template template = templateBuilder.build();
            // add a custom security group
            
            NodeMetadata node = getOnlyElement(this.compute.createNodesInGroup(this.groupname, instances, template));
            logger.info(String.format("<< node %s: %s%n", node.getId(),
            			concat(node.getPrivateAddresses(), node.getPublicAddresses())));
            logger.info("------------------------------------------------------");
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.info("------------------------------------------------------");
		}
	}
	/**
	 * 
	 * @return
	 */
	private ComputeService initComputeService() {

	  // example of specific properties, in this case optimizing image list to
	  // only amazon supplied
	  Properties properties = new Properties();
	  properties.setProperty(PROPERTY_EC2_AMI_QUERY, "owner-id=137112412989;state=available;image-type=machine");
	  properties.setProperty(PROPERTY_EC2_CC_AMI_QUERY, "");
	  long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
	  properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");
	
	  // set oauth endpoint property if set in system property
	  String oAuthEndpoint = System.getProperty(PROPERTY_OAUTH_ENDPOINT);
	  if (oAuthEndpoint != null) {
	     properties.setProperty(PROPERTY_OAUTH_ENDPOINT, oAuthEndpoint);
	  }
	
	  // example of injecting a ssh implementation
	  Iterable<Module> modules = ImmutableSet.<Module> of(
	        new SshjSshClientModule(),
	        new SLF4JLoggingModule(),
	        new EnterpriseConfigurationModule());
	
	  ContextBuilder builder = ContextBuilder.newBuilder(this.provider)
	                                         .credentials(this.identity, this.credential)
	                                         .modules(modules)
	                                         .overrides(properties);
	
	  logger.info("------------------------------------------------------");
	  logger.info(String.format(">> initializing %s%n", builder.getApiMetadata()));
	
	  return builder.buildView(ComputeServiceContext.class).getComputeService();
	}

	private LoginCredentials getLoginForCommandExecution() {
	  try {
	    String user = System.getProperty("user.name");
	    String privateKey = Files.toString(new File(System.getProperty("user.home") + "/.ssh/cloudos.pem"), UTF_8);
	    return LoginCredentials.builder().user(user).privateKey(privateKey).build();
	  } catch (Exception e) {
		 logger.info("------------------------------------------------------");
		 logger.error("error reading ssh key " + e.getMessage());
	     System.exit(1);
	     logger.info("------------------------------------------------------");
	     return null;
	  }
   }
	
	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getCredential() {
		return this.credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getGroupname() {
		return this.groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	
	private String getPrivateKey() {
		String privateKey = null;
		try {
			privateKey = Files.toString(new File(System.getProperty("user.home") + "/.ssh/cloudos.pem"), UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return privateKey;
	}
	
	/**
	 * 
	 * @param filename
	 * @return
	 */
	private String getCredentialFromJsonKeyFile(String filename) {
      try {
         String fileContents = Files.toString(new File(filename), UTF_8);
         Supplier<Credentials> credentialSupplier = new GoogleCredentialsFromJson(fileContents);
         return credentialSupplier.get().credential;
      } catch (IOException e) {
    	 logger.info("------------------------------------------------------");
         logger.error("Exception reading private key from '%s': " + filename);
         logger.error(e.getMessage());
         logger.info("------------------------------------------------------");
         System.exit(1);
         return null;
      }
   }

}
