package cloudos;

import org.apache.log4j.Logger;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by philippesouzamoraesribeiro on 12/12/15.
 */

@Document(collection = "users")
public class User {

	public final static Logger logger = Logger.getLogger( User.class );
	
    @Id
    private String id;

    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String address;
    private String telephone;

    public User () {}

    public User(String firstName, String lastName, String username,
    			String password, String email, String address, String telephone) {
    	
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%s, firstName='%s', lastName='%s']",
                this.id, this.firstName, this.lastName);
    }
    
    public byte[] getHash(String password1) throws NoSuchAlgorithmException {
    	logger.info(password1);
    	MessageDigest digest = MessageDigest.getInstance("SHA-1");
    	digest.reset();
    	return digest.digest();
    }
    
    public boolean validatePhoneNumber(String phoneNumber) {
    	/*
    	 * Check if a phone number is valid.
    	 * US Phone numbers are of the form XXX-XXX-XXXX
    	 * 
    	 * @param phoneNumber the phone number to be verified
    	 * @return true or false if the phone number is correctly formatted. 
    	 */
    	Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
    	Matcher matcher = pattern.matcher(phoneNumber);
    	return matcher.matches();
    }
}
