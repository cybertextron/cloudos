package machinelearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Instance;

import amazon.AWSCredentialSingleton;

public class MLChecker {

	private AmazonEC2 client;
	private final static Logger logger = Logger.getLogger(MLChecker.class);
	
	public MLChecker() {
		this.client = new AmazonEC2Client(AWSCredentialSingleton.getCredentials());
	}
	
	/**
	 * Finds all the instances allocated that are not running, to mark for the 
	 * MLCore to destroy them.
	 * 
	 * @param Set<String> The list of InstanceIds
	 * @return List<String> a list of instances marked for deletion.
	 */
	public List<String> getInstancesNotRunning(Set<String> instanceIds) {
		List<String> notRunning = new ArrayList<>();
		try {
			DescribeInstancesRequest describeRequest = new DescribeInstancesRequest();
			describeRequest.setInstanceIds(instanceIds);
			DescribeInstancesResult describeResult = this.client.describeInstances(describeRequest);
			List<Reservation> reservations = describeResult.getReservations();
			logger.info("-------------- status of instances -------------");
			for (Reservation reservation : reservations) {
				List<Instance> instances = reservation.getInstances();
				for (Instance instance : instances) {
					// if the instance is not running, make it for deletion
					if (!instance.getState().getName().equals("running")) {
						logger.info(String.format("Instance %s is not running", instance));
						notRunning.add(instance.getInstanceId());
					}
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return notRunning;
	}
}
