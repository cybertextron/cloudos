package machinelearning;

import java.util.Arrays;

import org.apache.log4j.Logger;

public class GaussJordanElimination {
	
	private static final double EPSILON = 1e-8;
	
	private final int N;	// N-by_n system
	private double[][] a;	// N-by-N+1 augmented matrix
	
	public final static Logger logger = Logger.getLogger(GaussJordanElimination.class);
	
	// Gauss-Jordan elimination with partial pivoting.
	public GaussJordanElimination(double[][] A, double[] b) {
		this.N = b.length;
		
		// build augmented matrix
		this.a = new double[this.N][2*this.N+1];
		for (int i = 0; i < this.N; i++) {
			for (int j = 0; j < this.N; j++)
				this.a[i][j] = A[i][j];
		}
		
		// Only needed if you want to find certificate of infeasibility (or compute inverse).
		for (int i = 0; i < this.N; i++)
			this.a[i][this.N + i] = 1.0;
		
		for (int i = 0; i < this.N; i++)
			this.a[i][2*this.N] = b[i];
		this.solve();
		assert this.certifySolution(A, b);
	}
	
	/**
	 * 
	 */
	private void solve() {
		// Gauss-Jordan elimination
		for (int p = 0; p < this.N; p++) {
			// find pivot row using partial pivoting
			int max = p;
			for (int i = p+1; i < this.N; i++) {
				if (Math.abs(this.a[i][p]) > Math.abs(this.a[max][p])) {
					max = i;
				}
			}
			// exchange row p with row max
			this.swap(p, max);
			
			// singular or nearly singular
			if (Math.abs(this.a[p][p]) <= EPSILON) {
				continue;
			}
			
			// pivot
			this.pivot(p, p);
		}
	}
	
	/**
	 * Swap row1 and row2
	 * 
	 * @param row1
	 * @param row2
	 */
	private void swap(int row1, int row2) {
		double[] temp = this.a[row1];
		this.a[row1] = this.a[row2];
		this.a[row2] = temp;
	}
	
	/**
	 * pivot on entry (p, q) using Gauss-Jordan elimination
	 * 
	 * @param p
	 * @param q
	 */
	private void pivot(int p, int q) {
		// everything but row p and column q
		for (int i = 0; i < this.N; i++) {
			double alpha = this.a[i][q] / this.a[p][q];
			for (int j = 0; j <= 2*this.N; j++) {
				if (i != p && j != q)
					this.a[i][j] -= alpha * this.a[p][j];
			}
		}
		
		// zero out column q
		for (int i = 0; i < this.N; i++)
			if (i != p)
				this.a[i][q] = 0.0;
		
		// scale row p 
		for (int j = 0; j <= 2*this.N; j++)
			if (j != q)
				this.a[p][j] /= this.a[p][q];
		this.a[p][q] = 1.0;
	}
	
	/**
	 * 
	 * @return
	 */
	public double[] primal() {
		double[] x = new double[this.N];
		for (int i = 0; i < this.N; i++) {
			if (Math.abs(this.a[i][i]) > EPSILON)
				x[i] = this.a[i][2*this.N] / this.a[i][i];
			else if (Math.abs(this.a[i][2*this.N]) > EPSILON)
				return null;
		}
		return x;
	}
	
	/**
	 * 
	 * @return
	 */
	public double[] dual() {
		double[] y = new double[this.N];
		for (int i = 0; i < this.N; i++) {
			if ((Math.abs(this.a[i][i]) <= EPSILON) && 
					(Math.abs(this.a[i][2*this.N]) > EPSILON)) {
				for (int j = 0; j < this.N; j++)
                    y[j] = this.a[i][this.N+j];
                return y;

			}
		}
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	 public boolean isFeasible() {
		 return this.primal() != null;
	 }
	 /*
	  * print the tableaux
	  */
	 public void show() {
	    for (int i = 0; i < this.N; i++) {
	        for (int j = 0; j < this.N; j++) {
	        	logger.info(String.format("%8.3f ", this.a[i][j]));
	        }
	        logger.info("| ");
	        for (int j = this.N; j < 2*this.N; j++) {
	        	logger.info(String.format("%8.3f ", this.a[i][j]));
	        }
	        logger.info(String.format("| %8.3f\n", this.a[i][2*this.N]));
	    }
	 }


	// check that Ax = b or yA = 0, yb != 0
	private boolean certifySolution(double[][] A, double[] b) {
	    // check that Ax = b
	    if (isFeasible()) {
	        double[] x = primal();
	        logger.info("x: " + Arrays.toString(x));
	        for (int i = 0; i < this.N; i++) {
	            double sum = 0.0;
	            for (int j = 0; j < this.N; j++) {
	                sum += A[i][j] * x[j];
	            }
	            if (Math.abs(sum - b[i]) > EPSILON) {
	            	logger.info("not feasible");
	            	logger.info(String.format("b[%d] = %8.3f, sum = %8.3f\n", i, b[i], sum));
	                return false;
	            }
	        }
	        return true;
	    }

        double[] y = dual();
        for (int j = 0; j < this.N; j++) {
            double sum = 0.0;
            for (int i = 0; i < this.N; i++) {
                sum += A[i][j] * y[i];
            }
            if (Math.abs(sum) > EPSILON) {
            	logger.info("invalid certificate of infeasibility");
            	logger.info(String.format("sum = %8.3f\n", sum));
                return false;
            }
        }
        double sum = 0.0;
        for (int i = 0; i < this.N; i++) {
            sum += y[i] * b[i];
        }
        if (Math.abs(sum) < EPSILON) {
        	logger.info("invalid certificate of infeasibility");
        	logger.info(String.format("yb  = %8.3f\n", sum));
            return false;
        }
        return true;
	}
}
