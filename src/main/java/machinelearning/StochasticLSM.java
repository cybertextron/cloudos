package machinelearning;
// Stochastic Gradient Descent for implementing Least Squares (LSM)
// Author: Andy Chen
//INCOMPLETE

import java.util.*;
import java.lang.Math; // for computing weights in future

public class StochasticLSM
{
	private DataMatrix params;
	private DataMatrix dataInputs;
	private DataMatrix dataOutputs;
	private double threshold;
	// private DataMatrix weights;

	// version without weights
	public StochasticLSM(DataMatrix newInputs, DataMatrix newOutputs, double newThreshold) throws IndexOutOfBoundsException
	{
		
		if (newInputs.numRows() != newOutputs.numRows())
		{
			throw new IndexOutOfBoundsException();
		}
		
		if (newOutputs.numColumns() != 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		dataInputs = new DataMatrix(newInputs);
		dataOutputs = new DataMatrix(newOutputs);
		params = new DataMatrix(1, newInputs.numColumns());
		threshold = newThreshold;
		
		
		/*
		weights = new DataMatrix(dataInputs.numRows(), 1);
		for (int i = 0; i < dataInputs.numRows(); i++)
		{
			// weights.set(i, 0, );
		}
		*/
	}
	/*
	// version with weights...
	public StochasticLSM(DataMatrix newWeights)
	{

		// multiply by weights
		// check if dims of weights and matrix are consistent
		weights = new DataMatrix(newWeights.numRows(), 1);
		for (int i = 0; i < newWeights.numRows(); i++)
		{
			weights.set(i, 0, newWeights.get(i, 0));
		}

	}
	*/

	private double multiplyByParam(FeatureVector input) throws IndexOutOfBoundsException
	{
		if (input.getNumFeatures() != params.numColumns())
		{
			throw new IndexOutOfBoundsException();
		}
		double total = 0.0;
		int columns = params.numColumns();
		for (int i = 0; i < columns; i++) // assuming params only has one row
			total += params.get(0, i) * input.get(i);
		return total;
	}

	private double computeLearningRate(long timeElapsed)
	{
		// return constant for now, eventually should be a decreasing function
		return 0.1;
	}

	private boolean hasConverged(long timeElapsed, double derivCoeff)
	{
		// DEFINITELY CHANGE THIS SOON
		// some kind of timeout device would be good
		return (timeElapsed >= 100);

	}

	public void computeParameters() throws IndexOutOfBoundsException
	{
		// stochastic gradient descent
		// could use a different random library
		Random randomGen = new Random();
		int randNum = 0;
		double derivCoeff = Double.MAX_VALUE;
		double learnStep = 0;
		long timeElapsed = 0;
		double paramVal = 0;
		int columns = params.numColumns();
		
		// HOW DO THE WEIGHTS WORK????
		while (!hasConverged(timeElapsed, derivCoeff))
		{
			randNum = randomGen.nextInt(dataInputs.numRows());
			derivCoeff = multiplyByParam(dataInputs.getRow(randNum)) - dataOutputs.get(randNum, 0);
			for (int i = 0; i < columns; i++) // assuming params only has one row
			{
				learnStep = computeLearningRate(timeElapsed);
				paramVal = params.get(0, i);
				params.set(0, i, paramVal - learnStep * derivCoeff * dataInputs.get(randNum, i));
			}
			timeElapsed++;
		}

	}

	public double predictOutput(FeatureVector newInput) throws IndexOutOfBoundsException
	{
		return multiplyByParam(newInput);
	}

	/*
	public static void main(String[] args) 
	{
		// feature vector tests
		FeatureVector f1 = new FeatureVector(2);
		f1.print();
		f1.set(0, 4.0);
		f1.set(0, 4.5);
		f1.set(1, 45.0);
		f1.print();
		System.out.println(f1.get(0));
		// start stress testing this
		
		// make some data matrices
		
		
		
		FeatureVector f2 = new FeatureVector(1);
		FeatureVector f3 = new FeatureVector(1);
		f2.set(0, 0.0);
		f3.set(0, 2.0);
		DataMatrix inputs = new DataMatrix(2, 1);
		inputs.setRow(0, f2);
		inputs.setRow(1, f3);
		inputs.print();
		DataMatrix outputs = new DataMatrix(inputs);
		outputs.print();
		//test this
		
		// should print close to 2
		StochasticLSM test = new StochasticLSM(inputs, outputs, 0.001);
		test.computeParameters();
		System.out.println(test.predictOutput(f3));
		
		// should print close to 100
		FeatureVector f4 = new FeatureVector(1);
		f4.set(0, 100.0);
		System.out.println(test.predictOutput(f4));
	}
	*/
}


