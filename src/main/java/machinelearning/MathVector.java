package machinelearning;

import java.util.ArrayList;

import org.apache.log4j.Logger;

// recall this throws index-out-of-bounds-exceptions

public class MathVector {

	public final static Logger logger = Logger.getLogger(MathVector.class);

	private ArrayList<Double> data;

	public MathVector() {
		data = new ArrayList<Double>();
	}

	public MathVector(int n) {
		data = new ArrayList<Double>();
		for (int i = 0; i < n; i++) {
			data.add(0.0);
		}
	}

	public MathVector(ArrayList<Double> newData) {
		data = newData;
	}

	public int length() {
		return data.size();
	}

	public Double get(int idx) {
		if (this.data.isEmpty()) {
			return null;
		}
		return data.get(idx);
	}

	public void set(int idx, double value) {
		if (idx < data.size()) {
			this.data.set(idx, value);
		} else if (idx == data.size()) {
			this.data.add(value);
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public void add(double value) {
		this.data.add(value);
	}

	public void add(MathVector addend) {
		// check dimensions
		if (data.size() != addend.length()) {
			throw new IllegalArgumentException("array length is not compatible");
		}
		for (int i = 0; i < data.size(); i++)
			data.set(i, data.get(i) + addend.get(i));
	}

	public double dot(MathVector factor) {
		// check dimensions
		if (data.size() != factor.length()) {
			throw new IllegalArgumentException("array length is not compatible");
		}
		double total = 0.0;
		for (int i = 0; i < data.size(); i++)
			total += data.get(i) * factor.get(i);
		return total;
	}

	public void divide(double factor) {
		if (factor == 0)
			return;
		for (int i = 0; i < data.size(); i++)
			data.set(i, data.get(i) / factor);
	}

	public void padWithOne() {
		data.add(0, 1.0);
	}

	@Override
	public String toString() {
		String s = new String();
		for (int i = 0; i < data.size(); i++) {
			s += data.get(i) + ", ";
		}
		return s;
	}

}
