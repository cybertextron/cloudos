package machinelearning;
// Feature Vector Data Structure
// Author: Andy Chen
//INCOMPLETE


public class FeatureVector
{
	public static int maxNumFeatures = 20;

	private int numFeatures;
	private double features[];

	public FeatureVector(int newNumFeatures) throws IndexOutOfBoundsException
	{
		if (newNumFeatures > maxNumFeatures)
		{
			throw new IndexOutOfBoundsException();
		}
		numFeatures = newNumFeatures;
		features = new double[maxNumFeatures];
		for (int i = 0; i < maxNumFeatures; i++)
			features[i] = 0.0;
	}
	
	public FeatureVector(FeatureVector oldCopy) throws IndexOutOfBoundsException
	{
		numFeatures = oldCopy.getNumFeatures();
		features = new double[maxNumFeatures];
		for (int i = 0; i < oldCopy.getNumFeatures(); i++)
			features[i] = oldCopy.get(i);
	}
	
	public int getNumFeatures()
	{
		return numFeatures;
	}

	public void set(int idx, double value) throws IndexOutOfBoundsException
	{
		if (idx >= numFeatures)
		{
			throw new IndexOutOfBoundsException();
		}
		features[idx] = value;
	}

	public double get(int idx) throws IndexOutOfBoundsException
	{
		if (idx >= numFeatures)
		{
			throw new IndexOutOfBoundsException();
		}
		return features[idx];
		
	}
	
	public void print()
	{
		for (int i = 0; i < maxNumFeatures; i++)
		{
			System.out.print(features[i]+ ", ");	
		}
		System.out.println("Features: " + numFeatures);
	}
}