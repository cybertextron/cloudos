package machinelearning;

import java.util.List;

import org.apache.log4j.Logger;

public class LinearRegression {

	private int N;
	private int M;
	// private final double intercept, slope;
	private MathVector params;
	private double R2;
	private double svar, svarx, svary;

	public final static Logger logger = Logger.getLogger(LinearRegression.class);

	public LinearRegression() {
	}

	/**
	 * Performs a linear regression on the data points (y[i], x[i]).
	 * 
	 * @param x
	 *            the values of the predictor variable.
	 * @param y
	 *            the corresponding values of the response variable
	 * @throws IllegalArgumentException
	 *             if the lengths of the two arrays are not equal.
	 */
	public void multiLinearRegression(List<MathVector> matrix, double[] y) {

		//logger.info(String.format("Matrix size: '%s'", matrix.size()));
		//logger.info(String.format("Matrix row size: '%s'", matrix.get(0).length()));
		//logger.info(String.format("Array y: '%s'", y.length));
		if (matrix.size() != y.length) {
			throw new IllegalArgumentException("array lengths are not equal");
		}
		this.N = y.length;
		assert this.N > 0;

		// to account for intercepts
		for (int i = 0; i < this.N; i++) {
			matrix.get(i).padWithOne();
		}
		this.M = matrix.get(0).length();
		
		// first pass
		MathVector sumx = new MathVector(this.M); // make sure this initializes
													// to have all zeroes
		logger.info(String.format("sumx size: " + sumx.length()));
		double sumy = 0.0;

		for (int i = 0; i < this.N; i++) {
			sumx.add(matrix.get(i));
			// sumx2 += x[i]*x[i];
			sumy += y[i];
		}
		sumx.divide(this.N);
		MathVector xbar = sumx; // could be fairly suspect -- perhaps should
								// make deep copy?
		double ybar = sumy / this.N;
		

		// construct X'Y
		double[] XtY = new double[this.M];
		for (int i = 0; i < this.M; i++) {
			XtY[i] = 0.0;
			for (int j = 0; j < this.N; j++) {
				assert this.M == matrix.get(j).length();
				XtY[i] += matrix.get(j).get(i) * y[j];
			}
		}
		double[][] XtX = new double[this.M][this.M];
		for (int i = 0; i < this.M; i++) {
			for (int j = 0; j < this.M; j++) {
				XtX[i][j] = 0.0;
				for (int k = 0; k < this.N; k++)
					XtX[i][j] += matrix.get(k).get(i) * matrix.get(k).get(j);
			}
		}
		logger.info(String.format("XtY: " + XtY[0] + " " + XtY[1]));
		logger.info(String.format("XtX: " + XtX[0][0] + " " + XtX[0][1]+ ", " + XtX[1][0] + " " + XtX[1][1]));
		
		// what happens if no soln?
		//logger.info(String.format("XtX: " + XtX));
		//logger.info(String.format("XtY: " + XtY));
		GaussJordanElimination solver = new GaussJordanElimination(XtX, XtY);
		double[] paramSoln = solver.primal();
		this.params = new MathVector(this.M);
		for (int i = 0; i < this.M; i++) {
			this.params.set(i, paramSoln[i]);
		}

	}

	/**
	 * public double weightedLinearRegression(ArrayList<ArrayList<Double>>
	 * matrix, double[] y, double[] weights)
	 */
	/**
	 * Returns the y-intercept of the best-fit line y.
	 * 
	 * @return the y-intercept that best fit line y.
	 */
	public double intercept() {
		return this.params.get(0);
	}

	/**
	 * Returns the slope of the best-fit line y.
	 * 
	 * @return The slope value.
	 */
	/*
	 * public double slope() { return this.slope; }
	 */

	/**
	 * Returns the coefficient of determination R^2.
	 * 
	 * @return the coefficient of determination which is a real number between 0
	 *         and 1.
	 */
	public double R2() {
		return this.R2;
	}

	/**
	 * Returns the standard error of the estimate for the slope
	 * 
	 * @return the standard error of the estimate for the slope
	 */
	public double slopeStdErr() {
		return Math.sqrt(this.svary);
	}

	/**
	 * Returns the standard error of the estimate of the intercept.
	 * 
	 * @return the standard error of the estimate of the intercept.
	 */
	public double interceptStdErr() {
		return Math.sqrt(this.svarx);
	}

	/**
	 * Returns the expected response y given the value of the predictor variable
	 * x.
	 * 
	 * @param x
	 *            the value of the predictor variable.
	 * @return The expected response y given the value of the predictor variable
	 *         x.
	 */
	public double predict(MathVector x) {
		x.padWithOne();
		return x.dot(this.params);
	}

	/**
	 * Returns a string representation of the simple linear regression model.
	 * 
	 * @return a string representation of the simple linear regression model,
	 *         including the best-fit line and the coefficient of determination.
	 */
	@Override
	public String toString() {
		String s = "";
		// s += String.format("%.2f N + %.2f", this.slope(), this.intercept());
		for (int i = 0; i < this.M; i++) {
			if (i != 0) {
				s += String.format(" + %.2f X_%d", this.params.get(i), i);
			} else {
				s += String.format("%.2f", this.params.get(i));
			}

		}
		return s;

		// return s + " (R^2 = " + String.format("%.3f", this.R2()) + ")";
	}
}
