package machinelearning;
// Matrix Data Structure
// Author: Andy Chen
// INCOMPLETE

import java.util.*;

public class DataMatrix
{
	private ArrayList<FeatureVector> matrix;
	private int rows;
	private int columns;

	public DataMatrix()
	{
		matrix = new ArrayList<FeatureVector>();
		rows = 0;
		columns = 0;
	}

	public DataMatrix(int newRows, int newColumns) throws IndexOutOfBoundsException
	{
		rows = newRows;
		columns = newColumns;
		matrix = new ArrayList<FeatureVector>(); 
		FeatureVector parent = new FeatureVector(columns);
		for (int i = 0; i < newRows; i++)
		{
			matrix.add(new FeatureVector(parent));
		}
	}
	
	// performs a deep copy of oldCopy
	public DataMatrix(DataMatrix oldCopy) throws IndexOutOfBoundsException
	{
		rows = oldCopy.numRows();
		columns = oldCopy.numColumns();
		matrix = new ArrayList<FeatureVector>();
		for (int i = 0; i < rows; i++)
		{
			matrix.add(new FeatureVector(oldCopy.getRow(i)));
			if (matrix.get(i).getNumFeatures() != matrix.get(0).getNumFeatures())
			{
				throw new IndexOutOfBoundsException();
			}
		}
	}

	public int numRows()
	{
		return rows;
	}

	public int numColumns()
	{
		return columns;
	}

	public void add() throws IndexOutOfBoundsException
	{
		matrix.add(new FeatureVector(columns));
	}

	public void set(int idx1, int idx2, double value) throws IndexOutOfBoundsException
	{
		// can throw IndexOutOfBoundsException
		(matrix.get(idx1)).set(idx2, value);
	}

	public double get(int idx1, int idx2) throws IndexOutOfBoundsException
	{
		// can throw IndexOutOfBoundsException
		return (matrix.get(idx1)).get(idx2);
	}

	public void setRow(int idx, FeatureVector features) throws IndexOutOfBoundsException
	{
		// can throw IndexOutOfBoundsException 
		if (columns != features.getNumFeatures())
		{
			throw new IndexOutOfBoundsException();
		}
		for (int i = 0; i < columns; i++)
		{
			set(idx, i, features.get(i));
		}
	}
	
	public FeatureVector getRow(int idx) throws IndexOutOfBoundsException
	{
		// can throw IndexOutOfBoundsException 
		return matrix.get(idx);
	}
	
	public void print()
	{
		for (int i = 0; i < rows; i++)
			matrix.get(i).print();
		System.out.println("Rows: " + rows +  ", Columns: " + columns);
	}
}

