package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "process_service", path = "/healthchecker/{instance}/process_service")
public interface ProcessServiceRepository extends MongoRepository<ProcessService, String> {

	List<ProcessService> findByCommand(String command);

	List<ProcessService> findByUser(String user);

	List<ProcessService> findByInstance(@Param("instance") String instance);
}
