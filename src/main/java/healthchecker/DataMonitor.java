package healthchecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Implement the Data Monitor class, which is responsible for collecting data
 * and store it in the Mongo Database.
 * 
 * @author Philippe Ribeiro
 *
 */
@SpringBootApplication
public class DataMonitor implements CommandLineRunner {

	@Autowired
	private DiskFileRepository diskRepository;

	/**
	 * 
	 */
	@Override
	public void run(String... arg0) throws Exception {
		diskRepository.deleteAll();
	}

}
