package healthchecker;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
public final class Utils {
	
	public final static Logger logger = Logger.getLogger(Utils.class);

	/**
	 * 
	 * @param doubles
	 * @return
	 */
	public static double[] toDoubleArray(List<Double> doubles) {
		if ((doubles == null) || (doubles.size() == 0)) {
			return null;
		}
		double[] target = new double[doubles.size()];
		for (int i = 0; i < target.length; i++) {
			target[i] = doubles.get(i);
		}
		return target;
	}

	/**
	 * Split a string representing the EC2 storage, and converts it to a double value.
	 * 
	 * @param storage: A string representing the EC2 storage.
	 * @return: the double value of the storage
	 */
	public static double getStorage(String storage) {
		try {
			// @TODO this regex is not correct
			String[] tokens = storage.split("((\\d+) x (\\d+))");
			double total = 1;
			logger.info(Arrays.toString(tokens));
			for (String token : tokens) {
				logger.info(token);
			}
			return total;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return 0;
		}
	}
	
	/**
	 * Given the root path, and the filename, join both as a single string path.
	 * 
	 * @param path: the name of the root path.
	 * @param filename: the name of the file
	 * @return: The joined full path for filename.
	 */
	public static String joinFilePath(String path, String filename) {
		File root = new File(path);
		File name = new File(root, filename);
		return name.getPath();
	}

}
