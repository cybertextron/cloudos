package healthchecker;

/**
 * Load data into the MongoDb database.This data is associated with the cluster
 * information. There will be a cloud provider associated with each of the
 * databases. Load data uses a parser to scan each of the directories, parse
 * each of one of the files and store them into their own MongoDB database.
 * 
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 3/26/2016
 *
 */
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import org.apache.log4j.Logger;

public class LoadData {

	// MongoClient object.
	private MongoClient mongoClient;
	// load the logger for the LoadData
	public final static Logger logger = Logger.getLogger( LoadData.class );
	// Specify the list of databases. Each cloud provider is a database.
	private final String databases[] = {
			"amazon",
			"google",
			"ibm",
			"microsoft",
			"oracle",
	};
	// InstanceParser object
	private InstanceParser parser;
	
	/**
	 * When the default constructor is used, the connection to the MongoDB
	 * client is localhost, and port 27017. 
	 */
	public LoadData() {
		this.mongoClient = new MongoClient();
		this.parser = new InstanceParser();
	}
	
	/**
	 * User specified host name and port number for MongoDB.
	 * 
	 * @param hostname: The host name where the MongoDB is located.
	 * @param portNumber: The port number MongoDB is listening.
	 */
	public LoadData(String hostname, Integer portNumber, String username,
					String password, String database) {
		MongoCredential credential = MongoCredential.createCredential(username,
											database, password.toCharArray());
		this.mongoClient = new MongoClient(new ServerAddress(), Arrays.asList(credential));
		this.parser = new InstanceParser();
	}
	
	/**
	 * Load the data obtained from the file parsing into each one of the Mongo
	 * collections.
	 */
	public void loadData() {
		try {
			for (String database : this.databases) {
				MongoDatabase db = this.getDatabase(database);
				if (db != null) {
					logger.info("Using database: " + db.getName());
					Path currentRelativePath = Paths.get("");
					String root = currentRelativePath.toAbsolutePath().toString();
					String dir = root + File.separatorChar + "data" + File.separatorChar + database;
					logger.info("Parsing files in directory: " + dir);
					List<String> files = new ArrayList<String>();
					File folder = new File(dir);
					if (!folder.exists()) {
						logger.warn("Folder " + folder + " does not exist. Skip");
						continue;
					}
					this.readFiles(folder, files);
					// parse each one of the files in read in the directory
					for (String filename : files) {
						String filepath = dir + File.separatorChar + filename;
						logger.info("Parsing file '" + filepath + "'");
						// loop over all the keys and values from the Map<String, Document>
						// in insert them into the 
						Map<String, Document> map = parser.parseFile(filepath);
						for (Map.Entry<String, Document> entry : map.entrySet()) {
							if (this.insertDocuments(db, entry.getKey(), entry.getValue())) {
								logger.info("Document '" + filepath + "' added successfully");
							}
						}
					}
				}
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Given each row in the data file, create a new Document object and store
	 * the information in the MongoDB.
	 * 
	 * @param db: The MongoDatabase connection
	 * @param collection The collection where the new document will be stored.
	 * @param row: the list representing each file 
	 * @return
	 */
	public boolean insertDocuments(MongoDatabase db, String collection, Document document) {
		boolean status = false;
		try {
			db.getCollection(collection).insertOne(document);
			status = true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			status = false;
		}
		return status;
	}
	/**
	 * Fetch or create a Mongo database, if one does not exists.
	 * 
	 * @param database: The name of the database.
	 * @return MongoDatabase object: The database connection.
	 */
	private MongoDatabase getDatabase(String database) {
		try {
			return this.mongoClient.getDatabase(database);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * Read each one of the root folders containing data, and search for all
	 * files that will be parsed.
	 * 
	 * @param folder: The root folder
	 * @param files: A list of files contained in the directory and
	 * sub-directories.
	 */
	public void readFiles(final File folder, List<String> files) {
		for (final File entry: folder.listFiles()) {
			// if entry is a directory, we iterate deeper.
			if (!entry.exists()) {
				logger.warn("File entry '" + entry.getName() + "' does not exists");
				continue;
			}
			else if (entry.isDirectory()) {
				readFiles(entry, files);
			} else {
				// if it's a file, we add to the files list.
				files.add(entry.getName());
			}
		}
	}
}
