package healthchecker;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

@Document(collection = "instances")
public class Instance {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;

	public String ipaddress;
	public String ami;

	public Instance() {
	}

	public Instance(String ipaddress, String ami) {
		this.id = new ObjectId().toHexString();
		this.ipaddress = ipaddress;
		this.ami = ami;
	}

	public String getIpaddress() {
		return this.ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public void setAmi(String ami) {
		this.ami = ami;
	}

	public String getAmi() {
		return this.ami;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
