package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the `ps` utility, which displays information about a selection of
 * the active processes.
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "process_service")
public class ProcessService {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;
	public String user;
	public Integer pid;
	public Double cpuPercentage;
	public Double memoryPercentage;
	public Double vsz;
	public Double rss;
	public String tt;
	public String stat;
	public String started;
	public String time;
	public String command;

	public ProcessService() {
	}

	/**
	 * 
	 * @param user:
	 *            the user that owns the process.
	 * @param pid:
	 *            the process id
	 * @param cpuPercentage:
	 *            cpu utilization of the process in "##.#" format.
	 * @param memoryPercentage:
	 *            ratio of the process's resident set size to the physical
	 *            memory on the machine, expressed as a percentage.
	 * @param vsz:
	 * @param rss:
	 *            resident set size, the non-swapped physical memory that a task
	 *            has used.
	 * @param tt:
	 *            controlling tty (terminal).
	 * @param stat:
	 *            multi-character process state.
	 * @param started:
	 *            time the command started.
	 * @param time:
	 *            cumulative CPU time.
	 * @param command:
	 *            the command being executed
	 */
	public ProcessService(String instance, String user, Integer pid, Double cpuPercentage, Double memoryPercentage,
			Double vsz, Double rss, String tt, String stat, String started, String time, String command) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		this.user = user;
		this.pid = pid;
		this.cpuPercentage = cpuPercentage;
		this.memoryPercentage = memoryPercentage;
		this.vsz = vsz;
		this.rss = rss;
		this.tt = tt;
		this.stat = stat;
		this.started = started;
		this.time = time;
		this.command = command;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
	}

	/**
	 * Another constructor for PSEntry
	 * 
	 * @param tokens:
	 *            an array string containing the attributes.
	 */
	public ProcessService(String instance, String[] tokens) {
		if (tokens.length < 11) {
			String[] copy = new String[11];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		this.user = tokens[0];
		this.pid = Integer.parseInt(tokens[1]);
		this.cpuPercentage = Double.parseDouble(tokens[2]);
		this.memoryPercentage = Double.parseDouble(tokens[3]);
		this.vsz = Double.parseDouble(tokens[4]);
		this.rss = Double.parseDouble(tokens[5]);
		this.tt = tokens[6];
		this.stat = tokens[7];
		this.started = tokens[8];
		this.time = tokens[9];
		this.command = tokens[10];

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
	}

	/**
	 * Implement the string representation of the PSEntry object.
	 * 
	 * @return: the string representation of the PSEntry object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
