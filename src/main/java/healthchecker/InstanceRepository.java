package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "instances", path = "instances")
public interface InstanceRepository extends MongoRepository<Instance, String> {

	Instance findByIpaddress(@Param("ipaddress") String ipaddress);

	List<Instance> findByAmi(String ami);
}
