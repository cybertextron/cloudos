package healthchecker;

/**
 * Parse the instance data information obtained from a cloud provider.
 * As of now the parser only works for AWS (Amazon Web Services), however
 * this will be expanded to include all the other supported providers.
 * This least include Google App Engine, Microsoft Azure, Oracle and IBM.
 * 
 * @author philipperibeiro
 * @Copyright CyberTextron Inc. 2016
 * @license: private
 * @author Philippe Ribeiro
 * @date: 3/26/2016
 */

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.bson.Document;

public class InstanceParser {

	public final static Logger logger = Logger.getLogger( InstanceParser.class );
	/* regex to parse AWS instances. Later each cloud provider will contain
	 * their own parser. */
	private final String pattern = "(?<=\\d)\\s+(?=[^x])|(?<=[^x])\\s+(?=\\d)";
	
	/**
	 * Given the data contained in a file, parseFile splits the values contained
	 * in each row, and return a ArrayList associated with that row. This is then
	 * stored in a HashMap which will be returned to the user.
	 * 
	 * @param filename: the name of the file to be parsed.
	 * @return Map: a map between the type of the instance and the row values.
	 */
	public Map<String, Document> parseFile(String filename) {
		Scanner input = null;
		Map<String, Document> table = new HashMap<String, Document>();
		try {
			File file = new File(filename);
			/* check if the file exists or if it's a directory */
			if ( !file.exists() ) {
				logger.warn("Filename: " + filename + " does not exist.");
				return null;
			}
			if ( !file.isFile() ) {
				logger.warn("Filename: " + filename + " is not a file.");
				return null;
			}
			input = new Scanner(file);
			String[] regions;
			String region;
			String[] values;
			String purpose = null;
			while(input.hasNextLine()) {
				String line = input.nextLine();
				if (line.trim().isEmpty())
					continue;
				else if (line.startsWith("Region")) {
					regions = line.trim().split("\\s+");
					if ( !(regions.length == 2) ) {
						logger.warn("Invalid region length");
						continue;
					} else {
						region = regions[1];
						logger.info(region);
					}
				} else if (line.startsWith("General Purpose")) {
					purpose = "general_purpose";
				} else if (line.startsWith("Compute Optimized")) {
					purpose = "compute_optimized";
				} else if (line.startsWith("GPU Instances")) {
					purpose = "gpu_instances";
				} else if (line.startsWith("Memory Optimized")) {
					purpose = "memory_optimized";
				} else if (line.startsWith("Storage Optimized")) {
					purpose = "storage_optimized";
				} else if (line.startsWith("Micro Instances")) {
					purpose = "micro_instances";
				} else {
					if (line.startsWith("vCPU")) {
						continue;
					}
					else {
						if (purpose == null) {
							purpose = "vcpu";
						}
						values = line.split(this.pattern);
						Document document = new Document();
						for (String v : values) {
							document.append(purpose, v);
						}
						table.put(purpose, document);
					}
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if ( !(input == null) ) {
				input.close();
			}
		}
		return table;
	}
}
