package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Implement the DiskFileRepository interface that allows for querying the
 * DiskFile collection
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "disk_file", path = "/healthchecker/{instance}/disk_file")
public interface DiskFileRepository extends MongoRepository<DiskFile, String> {

	List<DiskFile> findByFileSystem(String filesystem);

	List<DiskFile> findByMountedOn(String mountedOn);

	List<DiskFile> findByInstance(@Param("instance")String instance);
}
