package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Implement the VmstatRepository interface that allows for querying the Vmstat
 * collection
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "vmstat", path = "/healthchecker/{instance}/vmstat")
public interface VmstatRepository extends MongoRepository<Vmstat, String> {

	List<Vmstat> findByInstance(@Param("instance") String instance);
}
