package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "iostat", path = "/healthchecker/{instance}/iostat")
public interface IostatRepository extends MongoRepository<Iostat, String> {

	List<Iostat> findByDevice(String device);

	List<Iostat> findByInstance(@Param("instance")String instance);
}
