package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Implement the IOTopRepository interface that allows for querying the IOTop
 * collection
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "iotop", path = "/healthchecker/{instance}/iotop")
public interface IOTopRepository extends MongoRepository<IOTop, String> {

	List<IOTop> findByInstance(@Param("instance") String instance);

}
