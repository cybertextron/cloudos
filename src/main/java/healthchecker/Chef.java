package healthchecker;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.collect.Iterables.concat;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jclouds.ContextBuilder;
import org.jclouds.apis.ApiMetadata;
import org.jclouds.apis.Apis;
import org.jclouds.chef.ChefApiMetadata;
import org.jclouds.chef.ChefContext;
import org.jclouds.chef.ChefService;
import org.jclouds.chef.config.ChefProperties;
import org.jclouds.chef.domain.BootstrapConfig;
import org.jclouds.chef.util.RunListBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunScriptOnNodesException;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.providers.ProviderMetadata;
import org.jclouds.providers.Providers;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.domain.StatementList;
import org.jclouds.scriptbuilder.domain.chef.RunList;
import org.jclouds.scriptbuilder.statements.chef.ChefSolo;
import org.jclouds.scriptbuilder.statements.chef.InstallChefUsingOmnibus;
import org.jclouds.scriptbuilder.statements.git.CloneGitRepo;
import org.jclouds.scriptbuilder.statements.git.InstallGit;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.inject.Module;

public class Chef {

	private final static Logger logger = Logger.getLogger(Chef.class);

	public static final Map<String, ApiMetadata> allApis = Maps
			.uniqueIndex(Apis.viewableAs(ComputeServiceContext.class), Apis.idFunction());

	public static final Map<String, ProviderMetadata> appProviders = Maps
			.uniqueIndex(Providers.viewableAs(ComputeServiceContext.class), Providers.idFunction());

	public static final Set<String> allKeys = ImmutableSet
			.copyOf(Iterables.concat(appProviders.keySet(), allApis.keySet()));

	private ChefService chef;
	private List<String> runlist;
	private CloudProvider provider;

	/**
	 * 
	 * @param provider
	 * @param recipes
	 * @param client
	 * @param validator
	 */
	@SuppressWarnings("deprecation")
	public Chef(CloudProvider provider, String recipes, final String client, final String validator) {
		try {
			this.provider = provider;

			Properties chefConfig = new Properties();
			chefConfig.put(ChefProperties.CHEF_VALIDATOR_NAME, validator);
			chefConfig.put(ChefProperties.CHEF_VALIDATOR_CREDENTIAL, credentialForClient(validator));

			ContextBuilder builder = ContextBuilder.newBuilder(new ChefApiMetadata()) //
					.credentials(client, credentialForClient(client)) //
					.modules(ImmutableSet.<Module> of(new SLF4JLoggingModule())) //
					.overrides(chefConfig); //

			System.out.printf(">> initializing %s%n", builder.getApiMetadata());

			ChefContext context = builder.buildView(ChefContext.class);
			this.chef = context.getChefService();

			logger.info("Configuring node runlist in the Chef server...");
			this.runlist = new RunListBuilder().addRecipes(recipes.split(",")).build();
			BootstrapConfig config = BootstrapConfig.builder().runList(runlist).build();
			chef.updateBootstrapConfigForGroup(provider.getGroupName(), config);
			Statement chefServerBootstrap = chef.createBootstrapScriptForGroup(provider.getGroupName());

			// Run the script in the nodes of the group
			logger.info(String.format(">> installing [%s] on group %s as %s%n", recipes, provider.getGroupName(),
					provider.getLoginCredentials().identity));
			this.runScriptOnGroup(provider.getComputeService(), provider.getLoginCredentials(), provider.getGroupName(),
					chefServerBootstrap);
		} catch (Exception e) {
			logger.error("error reading private key " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param recipes
	 */
	public void solo(String recipes) {
		logger.info(String.format(">> installing [%s] on group %s as %s%n", recipes, this.provider.getGroupName(),
				this.provider.getLoginCredentials().identity));

		Iterable<String> recipeList = Splitter.on(',').split(recipes);
		ImmutableList.Builder<Statement> bootstrapBuilder = ImmutableList.builder();
		bootstrapBuilder.add(new InstallGit());

		// Clone community cookbooks into the node
		for (String recipe : recipeList) {
			bootstrapBuilder
					.add(CloneGitRepo.builder().repository("git://github.com/opscode-cookbooks/" + recipe + ".git")
							.directory("/var/chef/cookbooks/" + recipe) //
							.build());
		}

		// Configure Chef Solo to bootstrap the selected recipes
		bootstrapBuilder.add(new InstallChefUsingOmnibus());
		bootstrapBuilder.add(ChefSolo.builder() //
				.cookbookPath("/var/chef/cookbooks") //
				.runlist(RunList.builder().recipes(recipeList).build()) //
				.build());

		// Build the statement that will perform all the operations above
		StatementList bootstrap = new StatementList(bootstrapBuilder.build());

		// Run the script in the nodes of the group
		try {
			this.runScriptOnGroup(this.provider.getComputeService(), this.provider.getLoginCredentials(),
					this.provider.getGroupName(), bootstrap);
		} catch (RunScriptOnNodesException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}

	/**
	 * 
	 * @param client
	 * @return
	 * @throws Exception
	 */
	private String credentialForClient(final String client) throws Exception {
		String pemFile = System.getProperty("user.home") + "/.chef/" + client + ".pem";
		return Files.toString(new File(pemFile), UTF_8);
	}

	/**
	 * 
	 * @param compute
	 * @param login
	 * @param groupname
	 * @param command
	 * @throws RunScriptOnNodesException
	 */
	private void runScriptOnGroup(final ComputeService compute, final LoginCredentials login, final String groupname,
			final Statement command) throws RunScriptOnNodesException {
		// when you run commands, you can pass options to decide whether
		// to run it as root, supply or own credentials vs from cache,
		// and wrap in an init script vs directly invoke
		Map<? extends NodeMetadata, ExecResponse> execResponses = compute.runScriptOnNodesMatching(//
				inGroup(groupname), // predicate used to select nodes
				command, // what you actually intend to run
				overrideLoginCredentials(login)); // use the local user & ssh
													// key

		for (Entry<? extends NodeMetadata, ExecResponse> response : execResponses.entrySet()) {
			logger.info(String.format("<< node %s: %s%n", response.getKey().getId(),
					concat(response.getKey().getPrivateAddresses(), response.getKey().getPublicAddresses())));
			logger.info(String.format("<<     %s%n", response.getValue()));
		}
	}
}
