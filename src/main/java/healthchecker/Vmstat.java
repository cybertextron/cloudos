package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the modules for the 'vmstat' command line. 'vmstat' reports
 * information about processes, memory, paging, block IO, traps, and CPU
 * activity.
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "vmstat")
public class Vmstat {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;

	public Integer read;
	public Integer bit;
	public Integer swap;
	public Integer free;
	public Integer buffer;
	public Integer cache;
	public Integer swapIn;
	public Integer swapOut;
	public Integer bytesIn;
	public Integer bytesOut;
	public Integer in;
	public Integer cs;
	public Integer us;
	public Integer sy;
	public Integer cpuId;
	public Integer wa;
	public Integer st;

	public Vmstat() {
	}

	/**
	 * 
	 * @param read:
	 *            the number of processes waiting for run time.
	 * @param bit:
	 *            the number of processes in uninterruptible sleep.
	 * @param swap:
	 *            the amount of virtual memory used.
	 * @param free:
	 *            the mount of idle memory.
	 * @param buffer:
	 *            the mount of memory used as buffers.
	 * @param cache:
	 *            the amount of memory used as cache.
	 * @param swapIn:
	 *            the amount of memory swapped in from disk.
	 * @param swapOut:
	 *            Amount of memory swapped to disk.
	 * @param bytesIn:
	 *            blocks received swapped in from disk
	 * @param bytesOut:
	 *            blocks received swapped in to disk
	 * @param in:
	 *            The number of interrupts per second, including the clock.
	 * @param cs:
	 *            The number of context switches per second.
	 * @param us:
	 *            Time spent running non-kernel code.
	 * @param sy:
	 *            Time spent running kernel code.
	 * @param id:
	 *            Time spent idle.
	 * @param wa:
	 *            Time spent waiting for IO.
	 * @param st:
	 *            time stolen from a virtual machine.
	 */
	public Vmstat(String instance, Integer read, Integer bit, Integer swap, Integer free, Integer buffer,
			Integer cache, Integer swapIn, Integer swapOut, Integer bytesIn, Integer bytesOut, Integer in, Integer cs,
			Integer us, Integer sy, Integer id, Integer wa, Integer st) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.read = read;
		this.bit = bit;
		this.swap = swap;
		this.free = free;
		this.buffer = buffer;
		this.cache = cache;
		this.swapIn = swapIn;
		this.swapOut = swapOut;
		this.bytesIn = bytesIn;
		this.bytesOut = bytesOut;
		this.in = in;
		this.cs = cs;
		this.us = us;
		this.sy = sy;
		this.cpuId = id;
		this.wa = wa;
		this.st = st;
	}

	/**
	 * Another constructor for the VmstatEntry object.
	 * 
	 * @param tokens:
	 *            A String array containing all the information.
	 */
	public Vmstat(String instance, String[] tokens) {
		if (tokens.length < 17) {
			String[] copy = new String[17];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.read = Integer.parseInt(tokens[0]);
		this.bit = Integer.parseInt(tokens[1]);
		this.swap = Integer.parseInt(tokens[2]);
		this.free = Integer.parseInt(tokens[3]);
		this.buffer = Integer.parseInt(tokens[4]);
		this.cache = Integer.parseInt(tokens[5]);
		this.swapIn = Integer.parseInt(tokens[6]);
		this.swapOut = Integer.parseInt(tokens[7]);
		this.bytesIn = Integer.parseInt(tokens[8]);
		this.bytesOut = Integer.parseInt(tokens[9]);
		this.in = Integer.parseInt(tokens[10]);
		this.cs = Integer.parseInt(tokens[11]);
		this.us = Integer.parseInt(tokens[12]);
		this.sy = Integer.parseInt(tokens[13]);
		this.cpuId = Integer.parseInt(tokens[14]);
		this.wa = Integer.parseInt(tokens[15]);
		this.st = Integer.parseInt(tokens[16]);
	}

	/**
	 * Implement the string representation of the Vmstat object.
	 * 
	 * @return: The string representation of the Vmstart object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
