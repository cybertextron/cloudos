package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "lsof", path = "/healthchecker/{instance}/lsof")
public interface LsofRepository extends MongoRepository<Lsof, String> {

	List<Lsof> findByCommand(String command);

	List<Lsof> findByUser(String user);

	List<Lsof> findByName(String name);

	List<Lsof> findByInstance(@Param("instance") String instance);
}
