package healthchecker;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

/*
 * Make the MongoDb class final to avoid unnecessary inheritance
 */
public final class MongoDb {

	public final static Logger logger = Logger.getLogger(MongoDb.class);
	/*
	 * Keep a table between all the current connections and the databases, so to
	 * avoid creating many database connections
	 */
	public static Map<String, MongoDatabase> databaseTable = new HashMap<>();;
	private static MongoClient mongoClient = new MongoClient();

	/**
	 * Private constructor for the MongoDb class
	 */
	private MongoDb() {
	}

	/**
	 * Connect to a MongoDb instance running on the localhost on default port
	 * 27017
	 * 
	 * @param databaseName:
	 *            The name of the Mongo database
	 * @return MongoDatabase: the database connection object
	 */
	public static MongoDatabase getDatabase(String databaseName) {
		try {
			if (databaseTable.containsKey(databaseName)) {
				logger.warn(String.format("There is already a connection to database '%s'", databaseName));
				return databaseTable.get(databaseName);
			} else {
				MongoDatabase db = mongoClient.getDatabase(databaseName);
				databaseTable.put(databaseName, db);
				logger.info(String.format("Stablished a new connection to database '%s'", databaseName));
				return db;
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}
	}

	/**
	 * Returns a list of all the documents in a collection
	 * 
	 * @param database:
	 *            A MongoDatabase object
	 * @param collectionName:
	 *            the name of the collection
	 * @return List<Document> a list of all documents in the collection
	 */
	public static List<Document> getDocuments(MongoDatabase database, String collectionName) {
		if ((database == null) || (collectionName == null)) {
			return null;
		}
		try {
			FindIterable<Document> iterable = database.getCollection(collectionName).find();
			List<Document> documents = new LinkedList<>();
			iterable.forEach(new Block<Document>() {
				@Override
				public void apply(final Document document) {
					// TODO Auto-generated method stub
					documents.add(document);
				}

			});
			return documents;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * Returns a list of all the documents in a collection
	 * 
	 * @param database:
	 *            A MongoDatabase object
	 * @param collectionName:
	 *            the name of the collection
	 * @param map:
	 *            a map between fields and values
	 * @return List<Document> a list of all documents in the collection
	 */
	public static List<Document> getDocuments(MongoDatabase database, String collectionName, Map<String, String> map) {
		try {
			if (map.size() == 0) {
				return null;
			}
			FindIterable<Document> iterable;
			if (map.size() == 1) {
				String key = new String();
				String value = new String();
				for (Map.Entry<String, String> entry : map.entrySet()) {
					key = entry.getKey();
					value = entry.getValue();
				}
				return getDocuments(database, collectionName, key, value);
			} else {
				List<Bson> fields = new ArrayList<>();
				for (Map.Entry<String, String> entry : map.entrySet()) {
					fields.add(eq(entry.getKey(), entry.getValue()));
				}
				iterable = database.getCollection(collectionName).find(and(fields));
				return getDocuments(iterable);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param database
	 * @param collectionName
	 * @param value
	 * @param key
	 * @return
	 */
	public static List<Document> getDocuments(MongoDatabase database, String collectionName, String key, String value) {
		try {
			FindIterable<Document> iterable = database.getCollection(collectionName).find(eq(key, value));
			return getDocuments(iterable);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param iterable
	 * @return
	 */
	private static List<Document> getDocuments(FindIterable<Document> iterable) {
		List<Document> documents = new LinkedList<>();
		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				// TODO Auto-generated method stub
				documents.add(document);
			}

		});
		return documents;
	}

	/**
	 * Open real connection to MongoDB.
	 *
	 * @return target mongo client
	 * @throws UnknownHostException
	 *             exeption when creating server address
	 */
	public static MongoClient getMongoClient(String hostname, Integer portNumber, String username, String database,
			String password) throws UnknownHostException {
		// Using real connection
		MongoCredential credential = MongoCredential.createMongoCRCredential(username, database,
				password.toCharArray());
		ServerAddress adr = new ServerAddress(hostname, portNumber);
		return new MongoClient(adr, Arrays.asList(credential));
	}

}
