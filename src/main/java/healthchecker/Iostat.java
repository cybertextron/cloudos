package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "iostat")
public class Iostat {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;

	public String device;
	public Double tps;
	public Double kbReadps;
	public Double kbWriteps;
	public Double kbRead;
	public Double kbWrite;

	public Iostat() {
	}

	/**
	 * 
	 * @param device
	 * @param tps
	 * @param kbReadps
	 * @param kbWriteps
	 * @param kbRead
	 * @param kbWrite
	 */
	public Iostat(String instance, String device, Double tps, Double kbReadps, Double kbWriteps, Double kbRead,
			Double kbWrite) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		this.device = device;
		this.tps = tps;
		this.kbReadps = kbReadps;
		this.kbWriteps = kbWriteps;
		this.kbRead = kbRead;
		this.kbWrite = kbWrite;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
	}

	/**
	 * 
	 * @param tokens
	 */
	public Iostat(String instance, String[] tokens) {
		if (tokens.length < 6) {
			String[] copy = new String[6];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		this.device = tokens[0];
		this.tps = Double.parseDouble(tokens[1]);
		this.kbReadps = Double.parseDouble(tokens[2]);
		this.kbWriteps = Double.parseDouble(tokens[3]);
		this.kbRead = Double.parseDouble(tokens[4]);
		this.kbWrite = Double.parseDouble(tokens[5]);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
