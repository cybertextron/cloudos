package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the DiskFileEntry utility. It is used to gather information the
 * Disk File utility using the data returned by the 'df' command.
 * 
 * @author Philippe Ribeiro
 */
@Document(collection = "disk_file")
public class DiskFile {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;
	public String fileSystem;
	public Integer blockSize;
	public Integer used;
	public Integer available;
	public Double usedPercentage;
	public String mountedOn;

	public DiskFile() {
	}

	/**
	 * Constructor for the DiskFileEntry object.
	 * 
	 * @param fileSystem:
	 *            the name of the file system
	 * @param blockSize:
	 *            the amount of space available as 1k blocks.
	 * @param used:
	 *            amount of of space used.
	 * @param available:
	 *            amount of the space available in the file system.
	 * @param usedPercentage:
	 *            the percentage of space used.
	 * @param mountedOn:
	 *            where the file system is mounted on.
	 */
	public DiskFile(String instance, String fileSystem, Integer blockSize, Integer used, Integer available,
			Double usedPercentage, String mountedOn) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		// assert only valid values are added.
		assert blockSize >= 0;
		assert used >= 0;
		assert available >= 0;
		assert usedPercentage >= 0;
		// just assign default values
		this.fileSystem = fileSystem != null ? fileSystem : "";
		this.blockSize = blockSize;
		this.used = used;
		this.available = available;
		this.usedPercentage = usedPercentage;
		this.mountedOn = mountedOn != null ? mountedOn : "";
	}

	/**
	 * DiskFileEntry constructor, takes an array of tokens to initialize the
	 * arguments.
	 * 
	 * @param tokens:
	 *            Array of String values from the file parsed.
	 */
	public DiskFile(String instance, String[] tokens) {
		if (tokens.length < 6) {
			String[] copy = new String[6];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.fileSystem = tokens[0];
		this.blockSize = Integer.parseInt(tokens[1]);
		this.used = Integer.parseInt(tokens[2]);
		this.available = Integer.parseInt(tokens[3]);
		if (tokens[4].contains("%")) {
			StringBuilder build = new StringBuilder(tokens[4]);
			build.deleteCharAt(tokens[4].length() - 1);
			this.usedPercentage = Double.parseDouble(build.toString());
		} else {
			this.usedPercentage = Double.parseDouble(tokens[4]);
		}
		this.mountedOn = tokens[5];
	}

	/**
	 * Implement the string representation of the DiskFile object
	 * 
	 * @return: The string representation of the DiskFile object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
