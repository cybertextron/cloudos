package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the `lsof` command in Linux, which is used to report a list of all
 * open files and the processes that opened them.
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "lsof")
public class Lsof {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;
	public String command;
	public Integer pid;
	public String user;
	public String fd;
	public String type;
	public String device;
	public String size;
	public String node;
	public String name;

	public Lsof() {
	}

	/**
	 * Constructor for the Lsof object.
	 * 
	 * @param command:
	 *            the command the process is executing
	 * @param pid:
	 *            the pid of the process that opened the file.
	 * @param tid:
	 *            the process group identification number.
	 * @param user:
	 *            the owner of the process
	 * @param fd:
	 *            the file descriptor number of the file.
	 * @param type:
	 *            the file type of the device
	 * @param device:
	 *            the file's device number
	 * @param size:
	 *            the file'size or offset
	 * @param node:
	 *            the file's inode number
	 * @param name:
	 *            the name of file system containing the file
	 */
	public Lsof(String instance, String command, Integer pid, String user, String fd, String type, String device,
			String size, String node, String name) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.command = command;
		this.pid = pid;
		this.user = user;
		this.fd = fd;
		this.type = type;
		this.device = device;
		this.size = size;
		this.node = node;
		this.name = name;
	}

	/**
	 * Another constructor for the LsofEntry object. Instead, takes an array of
	 * tokens representing the variables as it was parsed.
	 * 
	 * @param tokens
	 */
	public Lsof(String instance, String[] tokens) {
		// COMMAND PID USER FD TYPE DEVICE SIZE/OFF NODE NAME
		if (tokens.length < 9) {
			String[] copy = new String[9];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());

		this.command = tokens[0];
		this.pid = Integer.parseInt(tokens[1]);
		this.user = tokens[2];
		this.fd = tokens[3];
		this.type = tokens[4];
		this.device = tokens[5];
		this.size = tokens[6];
		this.node = tokens[7];
		this.name = tokens[8];
	}

	/**
	 * Describe the string representation of the Lsof object.
	 * 
	 * @return: The string representation of the Lsof object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
