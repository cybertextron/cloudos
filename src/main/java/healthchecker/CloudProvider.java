package healthchecker;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.options.TemplateOptions.Builder.runScript;
import static org.jclouds.compute.predicates.NodePredicates.TERMINATED;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;
import static org.jclouds.scriptbuilder.domain.Statements.exec;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jclouds.ContextBuilder;
import org.jclouds.apis.ApiMetadata;
import org.jclouds.apis.Apis;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.RunScriptOnNodesException;
import org.jclouds.compute.domain.ComputeMetadata;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.Image;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.OsFamily;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.domain.Credentials;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.ec2.domain.InstanceType;
import org.jclouds.enterprise.config.EnterpriseConfigurationModule;
import org.jclouds.googlecloud.GoogleCredentialsFromJson;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.providers.ProviderMetadata;
import org.jclouds.providers.Providers;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.AdminAccess;
import org.jclouds.sshj.config.SshjSshClientModule;

import com.google.common.base.Charsets;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.inject.Module;

public class CloudProvider {

	private final static Logger logger = Logger.getLogger(CloudProvider.class);

	public static final Map<String, ApiMetadata> allApis = Maps
			.uniqueIndex(Apis.viewableAs(ComputeServiceContext.class), Apis.idFunction());

	public static final Map<String, ProviderMetadata> appProviders = Maps
			.uniqueIndex(Providers.viewableAs(ComputeServiceContext.class), Providers.idFunction());

	public static final Set<String> allKeys = ImmutableSet
			.copyOf(Iterables.concat(appProviders.keySet(), allApis.keySet()));

	public static final String PROPERTY_OAUTH_ENDPOINT = "oauth.endpoint";

	private String groupname;
	private String credentials;
	private ComputeService compute;
	private LoginCredentials login;

	/**
	 * 
	 * @param provider
	 * @param identity
	 * @param credential
	 * @param groupname
	 */
	public CloudProvider(String provider, String identity, String credential, String groupname) {
		this.groupname = groupname;
		if (provider.equalsIgnoreCase("google-compute-engine")) {
			// something will come here
			this.credentials = this.getCredentialFromJsonKeyFile(credential);
		} else {
			this.credentials = credential;
		}
		Properties properties = new Properties();
		// properties.setProperty(PROPERTY_EC2_AMI_QUERY,
		// "owner-id=137112412989;state=available;image-type=machine");
		// properties.setProperty(PROPERTY_EC2_CC_AMI_QUERY, "");
		long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
		properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");

		// set OAuth end-point property if set in system property
		String oAuthEndpoint = System.getProperty(PROPERTY_OAUTH_ENDPOINT);
		if (oAuthEndpoint != null) {
			properties.setProperty(PROPERTY_OAUTH_ENDPOINT, oAuthEndpoint);
		}

		// example of injecting a SSH implementation
		Iterable<Module> modules = ImmutableSet.<Module> of(new SshjSshClientModule(), new SLF4JLoggingModule(),
				new EnterpriseConfigurationModule());
		ContextBuilder builder = ContextBuilder.newBuilder(provider).credentials(identity, credential).modules(modules)
				.overrides(properties);
		logger.info(String.format(">> initializing %s%n", builder.getApiMetadata()));
		/* Set the login entry */
		this.login = this.getLoginForCommandExecution();
		/* Set the ComputeService */
		this.compute = builder.buildView(ComputeServiceContext.class).getComputeService();
	}

	/**
	 * 
	 * @param action
	 * @return
	 */
	private LoginCredentials getLoginForCommandExecution() {
		try {
			String user = System.getProperty("user.name");
			String privateKey = Files.toString(new File(System.getProperty("user.home") + "/.ssh/id_rsa"), UTF_8);
			return LoginCredentials.builder().user(user).privateKey(privateKey).build();
		} catch (Exception e) {
			logger.error("error reading ssh key " + e.getMessage());
			return null;
		}
	}

	public void createSecurityGroupAndAuthorizePorts(String name) {
		logger.info(String.format("%d: creating security group: %s%n", System.currentTimeMillis(), name));
	}
	/**
	 * 
	 * @return
	 */
	public String getGroupName() {
		return this.groupname;
	}

	/*
	 * 
	 */
	public ComputeService getComputeService() {
		return this.compute;
	}

	/**
	 * 
	 * @return
	 */
	public LoginCredentials getLoginCredentials() {
		return this.login;
	}

	/**
	 * 
	 * @param filename
	 * @return
	 */
	private String getCredentialFromJsonKeyFile(String filename) {
		// TODO Auto-generated method stub
		try {
			String fileContents = Files.toString(new File(filename), UTF_8);
			Supplier<Credentials> credentialSupplier = new GoogleCredentialsFromJson(fileContents);
			String credential = credentialSupplier.get().credential;
			return credential;
		} catch (IOException e) {
			logger.error("Exception reading private key from '%s': " + filename);
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param number
	 */
	public void add(int number) {
		assert number >= 1;
		try {
			logger.info(String.format(">> adding node to group %s%n", this.groupname));
			/**
			 * Default template chooses the smallest size on an operating system
			 * that tested to work with java, which tends to be Ubuntu or CentOS
			 */
			TemplateBuilder templateBuilder = this.compute.templateBuilder();
			templateBuilder.osVersionMatches("14.04");
			templateBuilder.osFamily(OsFamily.UBUNTU);
			/**
			 * This will create a user with the same name as you on the node.
			 * ex. you can connect via SSH public-IP.
			 */
			Statement bootInstructions = AdminAccess.standard();
			// to run commands as root, we use the runScript option in the
			// template.
			templateBuilder.options(runScript(bootInstructions));

			Template template = templateBuilder.hardwareId(InstanceType.M1_SMALL).build();
			/* Modify here to create more elements */
			NodeMetadata node = getOnlyElement(this.compute.createNodesInGroup(this.groupname, number, template));
			logger.info(String.format("<< node %s: %s%n", node.getId(),
					concat(node.getPrivateAddresses(), node.getPublicAddresses())));
		} catch (RunNodesException e) {
			logger.error("error adding node to group " + this.groupname + ": " + e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void run(String param) {
		try {
			File file = new File(param);
			logger.info(
					String.format(">> running [%s] on group %s as %s%n", file, this.groupname, this.login.identity));
			/**
			 * Default template chooses the smallest size on an operating system
			 * that tested to work with java, which tends to be Ubuntu or CentOS
			 */
			// when running a sequence of commands, you probably want to have
			// jclouds use the default behavior,
			// which is to fork a background process.
			Map<? extends NodeMetadata, ExecResponse> responses = this.compute.runScriptOnNodesMatching(//
					inGroup(this.groupname), Files.toString(file, Charsets.UTF_8), // passing
																					// in
																					// a
																					// string
																					// with
																					// the
																					// contents
																					// of
																					// the
																					// file
					overrideLoginCredentials(this.login).runAsRoot(false)
							.nameTask("_" + file.getName().replaceAll("\\..*", ""))); // ensuring
																						// task
																						// name
																						// isn't
			// the same as the file so status checking works properly

			for (Entry<? extends NodeMetadata, ExecResponse> response : responses.entrySet()) {
				logger.info(String.format("<< node %s: %s%n", response.getKey().getId(),
						concat(response.getKey().getPrivateAddresses(), response.getKey().getPublicAddresses())));
				logger.info(String.format("<<     %s%n", response.getValue()));
			}
		} catch (Exception e) {
			logger.error("error adding node to group " + this.groupname + ": " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param command
	 * @return
	 */
	public void execute(String command) {
		try {
			logger.info(
					String.format(">> running [%s] on group %s as %s%n", command, this.groupname, this.login.identity));
			// when you run commands, you can pass options to decide whether to
			// run it as root, supply or own credentials vs from cache, and wrap
			// in an init script vs directly invoke
			Map<? extends NodeMetadata, ExecResponse> responses = this.compute.runScriptOnNodesMatching(
					inGroup(this.groupname), // predicate used to select nodes
					exec(command),
					overrideLoginCredentials(this.login) // use my local user &
							// ssh key
							.runAsRoot(true) // don't attempt to run as root
												// (sudo)
							.wrapInInitScript(false));// run command directly
			for (Entry<? extends NodeMetadata, ExecResponse> response : responses.entrySet()) {
				logger.info(String.format("<< node %s: %s%n", response.getKey().getId(),
						concat(response.getKey().getPrivateAddresses(), response.getKey().getPublicAddresses())));
				logger.info(String.format("<<     %s%n", response.getValue()));
			}
		} catch (RunScriptOnNodesException e) {
			logger.error("error executing " + command + " on group " + this.groupname + ": " + e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void destroy() {
		try {
			logger.info(String.format(">> destroying nodes in group %s%n", this.groupname));
			/**
			 * Default template chooses the smallest size on an operating system
			 * that tested to work with java, which tends to be Ubuntu or CentOS
			 */
			Set<? extends NodeMetadata> destroyed = this.compute.destroyNodesMatching(//
					Predicates.<NodeMetadata> and(not(TERMINATED), inGroup(this.groupname)));
			logger.info(String.format("<< destroyed nodes %s%n", destroyed));
		} catch (Exception e) {
			logger.error("error adding node to group " + this.groupname + ": " + e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void listImages() {
		try {
			Set<? extends Image> images = this.compute.listImages();
			logger.info(">> No of images " + images.size());
			for (Image img : images) {
				logger.info(">>>>  " + img);
			}
		} catch (Exception e) {
			logger.error("error adding node to group " + this.groupname + ": " + e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void listNodes() {
		try {
			Set<? extends ComputeMetadata> nodes = this.compute.listNodes();
			System.out.printf(">> No of nodes/instances %d%n", nodes.size());
			for (ComputeMetadata nodeData : nodes) {
				logger.info(">>>>  " + nodeData);
			}
		} catch (Exception e) {
			logger.error("error adding node to group " + this.groupname + ": " + e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void close() {
		this.destroy();
		this.compute.getContext().close();
	}

}
