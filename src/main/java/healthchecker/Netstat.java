package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the Netstat object, which is used to print network connections,
 * routing tables, interface statistics, masquerade connections, and multicast
 * membership.
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "netstat")
public class Netstat {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;

	public String protocol;
	public Integer received;
	public Integer sent;
	public String localAddress;
	public String foreignAddress;
	public String state;

	public Netstat() {
	}

	/**
	 * 
	 * @param protocol:
	 *            the protocol used in the connection.
	 * @param received:
	 *            amount of bytes received
	 * @param sent:
	 *            amount of bytes sent.
	 * @param localAddress:
	 *            the local machine address.
	 * @param foreignAddress:
	 *            the other end-point machine connection.
	 * @param state:
	 *            the current state of the connection.
	 */
	public Netstat(String instance, String protocol, Integer received, Integer sent, String localAddress,
			String foreignAddress, String state) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.protocol = protocol;
		this.received = received;
		this.sent = sent;
		this.localAddress = localAddress;
		this.foreignAddress = foreignAddress;
		this.state = state;

	}

	/**
	 * Another constructor for the NetstatEntry class
	 * 
	 * @param tokens:
	 *            An array of tokens representing each attribute of the
	 *            NetstatEntry.
	 */
	public Netstat(String instance, String[] tokens) {
		if (tokens.length < 6) {
			String[] copy = new String[6];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.protocol = tokens[0];
		this.received = Integer.parseInt(tokens[1]);
		this.sent = Integer.parseInt(tokens[2]);
		this.localAddress = tokens[3];
		this.foreignAddress = tokens[4];
		this.state = tokens[5];
	}

	/**
	 * Implement the string representation for the netstat class.
	 * 
	 * @return: The string representation for the netstat class.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
