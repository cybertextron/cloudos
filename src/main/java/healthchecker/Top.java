package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "top")
public class Top {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;

	public Integer pid;
	public String user;
	public String processReal;
	public String niceValue;
	public String virtualImage;
	public Integer residentSize;
	public Integer sharedMemory;
	public String status;
	public String cpuPercentage;
	public String memoryPercentage;
	public String time;
	public String command;

	public Top() {
	}

	/**
	 * 
	 * @param pid
	 * @param user
	 * @param processReal
	 * @param niceValue
	 * @param virtualImage
	 * @param residentSize
	 * @param sharedMemory
	 * @param status
	 * @param cpuPercentage
	 * @param memoryPercentage
	 * @param time
	 * @param command
	 */
	public Top(String instance, Integer pid, String user, String processReal, String niceValue, String virtualImage,
			Integer residentSize, Integer sharedMemory, String status, String cpuPercentage, String memoryPercentage,
			String time, String command) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.pid = pid;
		this.user = user;
		this.processReal = processReal;
		this.niceValue = niceValue;
		this.virtualImage = virtualImage;
		this.residentSize = residentSize;
		this.sharedMemory = sharedMemory;
		this.status = status;
		this.cpuPercentage = cpuPercentage;
		this.memoryPercentage = memoryPercentage;
		this.time = time;
		this.command = command;
	}

	/**
	 * Another constructor for the Top object.
	 * 
	 * @param tokens:
	 *            an array of elements containing the data for the Top object.
	 */
	public Top(String instance, String[] tokens) {
		if (tokens.length < 12) {
			String[] copy = new String[12];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.pid = Integer.parseInt(tokens[0]);
		this.user = tokens[1];
		this.processReal = tokens[2];
		this.niceValue = tokens[3];
		this.virtualImage = tokens[4];
		this.residentSize = Integer.parseInt(tokens[5]);
		this.sharedMemory = Integer.parseInt(tokens[6]);
		this.status = tokens[7];
		this.cpuPercentage = tokens[8];
		this.memoryPercentage = tokens[9];
		this.time = tokens[10];
		this.command = tokens[11];
	}

	/**
	 * Return a json representation of the Top object
	 * 
	 * @return: the json object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
