package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Implement the NetstatRepositort interface that allows for querying the
 * Netstat collection
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "netstat", path = "/healthchecker/{instance}/netstat")
public interface NetstatRepository extends MongoRepository<Netstat, String> {

	List<Netstat> findByProtocol(String protocol);

	List<Netstat> findByState(String state);

	List<Netstat> findByLocalAddress(String localaddress);

	List<Netstat> findByforeignAddress(String foreignAddress);

	List<Netstat> findByInstance(@Param("instance") String instance);
}
