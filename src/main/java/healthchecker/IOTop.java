package healthchecker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.gson.Gson;

/**
 * Implement the IOTop utility. IOTop watches I/O usage information output by
 * the Linux kernel and displays a table of current I/O usage by processes or
 * threads on the system.
 * 
 * @author Philippe Ribeiro
 *
 */
@Document(collection = "iotop")
public class IOTop {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	public String id;
	public String instance;
	public String now;

	public Integer tid;
	public String prio;
	public String user;
	public String diskRead;
	public String diskWrite;
	public Double swapping;
	public Double ioPercentage;
	public String command;

	public IOTop() {
	}

	/**
	 * Constructor for the IOTopEntry command.
	 * 
	 * @param tid:
	 *            the process id
	 * @param prio:
	 *            the process io.
	 * @param user:
	 *            the user name
	 * @param diskRead:
	 *            the disk read value
	 * @param diskWrite:
	 *            the disk write value.
	 * @param swapping:
	 *            the swapping space usage.
	 * @param ioPercentage:
	 *            the io percentage.
	 * @param command:
	 *            the command being executed.
	 */
	public IOTop(String instance, Integer tid, String prio, String user, String diskRead, String diskWrite,
			Double swapping, Double ioPercentage, String command) {
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.instance = instance;
		this.tid = tid;
		this.prio = prio;
		this.user = user;
		this.diskRead = diskRead;
		this.diskWrite = diskWrite;
		this.swapping = swapping;
		this.ioPercentage = ioPercentage;
		this.command = command;
	}

	/**
	 * Another constructor for the IOTopEntry
	 * 
	 * @param tokens:
	 *            A string array containing the parsing results.
	 */
	public IOTop(String instance, String[] tokens) {
		if (tokens.length < 8) {
			String[] copy = new String[8];
			for (int i = 0; i < tokens.length; i++) {
				copy[i] = tokens[i];
			}
			tokens = copy;
		}
		this.id = new ObjectId().toHexString();
		this.instance = instance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.now = dateFormat.format(new Date());
		this.tid = Integer.parseInt(tokens[0]);
		this.prio = tokens[1];
		this.user = tokens[2];
		this.diskRead = tokens[3];
		this.diskWrite = tokens[4];
		if (tokens[5].contains("%")) {
			StringBuilder build = new StringBuilder(tokens[4]);
			build.deleteCharAt(tokens[4].length() - 1);
			this.swapping = Double.parseDouble(build.toString());
		} else {
			this.swapping = Double.parseDouble(tokens[4]);
		}
		if (tokens[6].contains("%")) {
			StringBuilder build = new StringBuilder(tokens[5]);
			build.deleteCharAt(tokens[4].length() - 1);
			this.ioPercentage = Double.parseDouble(build.toString());
		} else {
			this.ioPercentage = Double.parseDouble(tokens[5]);
		}
		this.command = tokens[7];
	}

	/**
	 * Returns the string representation of the IOTop object.
	 * 
	 * @return the string representation of the IOTop object.
	 */
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
