package healthchecker;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 
 * @author Philippe Ribeiro
 *
 */
@RepositoryRestResource(collectionResourceRel = "top", path = "/healthchecker/{instance}/top")
public interface TopRepository extends MongoRepository<Top, String> {

	List<Top> findByCommand(String command);

	List<Top> findByUser(String user);

	List<Top> findByInstance(@Param("instance") String instance);
}